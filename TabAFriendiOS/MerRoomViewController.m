//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "MerRoomViewController.h"
#import "UIImageView+WebCache.h"
#import "TapCell1.h"
#import <QuartzCore/QuartzCore.h>
@interface MerRoomViewController ()

@end

@implementation MerRoomViewController
@synthesize collectionView;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;



NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;

NSMutableArray *senderFNameArr;
NSMutableArray *senderImageArr;
NSMutableArray *ReceiverFNameArr;
NSMutableArray *ReceiverImageArr;
NSMutableArray *tapStatusArr;
NSMutableArray *tapexpireArr;
NSMutableArray *senderLNameArr;

NSMutableArray *ReceiverLNameArr;


NSString *contactIDName;

NSMutableArray *tapIDArray;
NSMutableArray *tapAmountArray;

-(IBAction)addBack
{
    
    if (IS_IPHONE_5)
    {
        MerViewController *signupView=[[MerViewController alloc]initWithNibName:@"MerViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MerViewController *signupView=[[MerViewController alloc]initWithNibName:@"MerViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}


int editTag;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:_addContactView];
    _addContactView.hidden=YES;
    [self.view addSubview:_contactEditView];
    _contactEditView.hidden=YES;
    
    NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
    
    [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
    
    
    [_editButton setTitle:@"Edit" forState:UIControlStateNormal];
    editTag=0;
  
    
    
    
    [self getMerchantDetails];
    [self getTaps];
    
	// Do any additional setup after loading the view, typically from a nib.
}


-(void)getMerchantDetails
{
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    NSString *merID=[check objectForKey:@"MERCHANT_USER_ID"];
    NSString *neme=[check objectForKey:@"NAME"];
    NSString *merImage=[check objectForKey:@"IMAGE"];
    
    NSString *addrStr=[NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@",[check valueForKey:@"AD1"],[check valueForKey:@"AD2"],[check valueForKey:@"CITY"],[check valueForKey:@"CONTCAT_STATE"],[check valueForKey:@"STATE"],[check valueForKey:@"ZIP"]];
    
    
    _toNameLabel.text=neme;
    _toAddressLabel.text=addrStr;
    
    @try {
        
        [_toImageView setImageWithURL:[NSURL URLWithString:merImage]
                     placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
        
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }
    
    
    
}





-(void)getTaps
{
    
    
    
    
    
    tapIDArray=[[NSMutableArray alloc]init];
    tapAmountArray=[[NSMutableArray alloc]init];
    
    senderImageArr=[[NSMutableArray alloc]init];
    senderFNameArr=[[NSMutableArray alloc]init];
    
    ReceiverImageArr=[[NSMutableArray alloc]init];
    ReceiverFNameArr=[[NSMutableArray alloc]init];
    
    senderLNameArr=[[NSMutableArray alloc]init];
    ReceiverLNameArr=[[NSMutableArray alloc]init];
    
    tapStatusArr=[[NSMutableArray alloc]init];
    tapexpireArr=[[NSMutableArray alloc]init];
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"MERCHANT_USER_ID"];
   
    
    // userID=@"230";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/merchant/merchnatTaplist",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"merchantid"];
   
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        
        
        
        NSArray *temp= [results1 valueForKeyPath:@"Response.tabs"];
        
        
        @try {
            
            for(NSDictionary *value in temp)
            {
                
                
                
                [tapIDArray  addObject:[value valueForKey:@"pk_id"]];
                [tapAmountArray  addObject:[value valueForKey:@"tap_amount"]];
                
                
                [ReceiverImageArr  addObject:[value valueForKey:@"receiverImage"]];
                [senderImageArr  addObject:[value valueForKey:@"senderImage"]];
                
                [senderLNameArr  addObject:[value valueForKey:@"senderLastName"]];
                [senderFNameArr  addObject:[value valueForKey:@"senderFirstName"]];
                
                [ReceiverFNameArr  addObject:[value valueForKey:@"receiverFirstName"]];
                [ReceiverLNameArr  addObject:[value valueForKey:@"receiverLastName"]];
                
                [tapStatusArr  addObject:[value valueForKey:@"tap_status"]];
                [tapexpireArr  addObject:[value valueForKey:@"expire_date"]];
            }
            
            [_tblView reloadData];
        }
        @catch (NSException *exception) {
            
        }
       
      
        
        
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [tapIDArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    
    
    TapCell1 * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"TapCell1" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (TapCell1*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
    
    
    
    NSString *status=tapStatusArr[indexPath.row];
    
    if ([status isEqualToString:@"valid"])
    {
        cell.status.text=@"VALID";
        cell.statusImg.image=[UIImage imageNamed:@"valid.png"];
        
    }
    
    if ([status isEqualToString:@"used"])
    {
        cell.status.text=@"USED";
        cell.statusImg.image=[UIImage imageNamed:@"used.png"];
    }
    
    if ([status isEqualToString:@"expired"])
    {
        cell.status.text=@"EXPIRED";
        cell.statusImg.image=[UIImage imageNamed:@"expired-icon.png"];
    }
    
    if ([status rangeOfString:@"converted"].location == NSNotFound) {
        NSLog(@"string does not contain bla");
    } else {
        
        cell.status.text=@"CASH RECEIVED";
        cell.statusImg.image=[UIImage imageNamed:@"converted_orange.png"];

    }
    
   
    
     cell.amount.text=[NSString stringWithFormat:@"%@",tapAmountArray[indexPath.row]];
    
    
    @try {
        
        cell.from.text=[NSString stringWithFormat:@"%@ %@",senderFNameArr[indexPath.row],senderLNameArr[indexPath.row]];
        
        cell.to.text=[NSString stringWithFormat:@"%@ %@",ReceiverFNameArr[indexPath.row],ReceiverLNameArr[indexPath.row]];
    }
    @catch (NSException *exception) {
        
    }
   
       
    
    @try {
        NSString *temp=[senderImageArr objectAtIndex:indexPath.row];
        [cell.senderImg setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:[UIImage imageNamed:@""]];
        
        
        CALayer *imageLayerrecImg =cell.senderImg.layer;
        [imageLayerrecImg setCornerRadius:25];
        [imageLayerrecImg setBorderWidth:0];
        [imageLayerrecImg setMasksToBounds:YES];
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }

    @try {
        NSString *temp=[ReceiverImageArr objectAtIndex:indexPath.row];
        [cell.receiverImg setImageWithURL:[NSURL URLWithString:temp]
                       placeholderImage:[UIImage imageNamed:@""]];
        
        CALayer *imageLayerrecImg1 =cell.receiverImg.layer;
        [imageLayerrecImg1 setCornerRadius:25];
        [imageLayerrecImg1 setBorderWidth:0];
        [imageLayerrecImg1 setMasksToBounds:YES];
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }

        
   cell.selectionStyle=UITableViewCellSelectionStyleNone;
     return cell;
    
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 77;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Select %d",indexPath.row);
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
     [check setObject:tapIDArray[indexPath.row] forKey:@"LAST_TAPPED_ID"];
    
    if (IS_IPHONE_5)
    {
        MerTapRoomViewController *signupView=[[MerTapRoomViewController alloc]initWithNibName:@"MerTapRoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MerTapRoomViewController *signupView=[[MerTapRoomViewController alloc]initWithNibName:@"MerTapRoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }

}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    [textField resignFirstResponder];
    
      
    
    return YES;
    
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
   
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}




@end

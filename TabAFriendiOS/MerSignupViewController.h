//
//  ViewController.h
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MerSignupViewController.h"
#import "ZBarSDK.h"
#import "ZBarReaderView.h"
@interface MerSignupViewController : UIViewController<ZBarReaderDelegate>
{
     
}
-(IBAction)back_button;
-(IBAction)login_button;
-(IBAction)signup_button;
-(IBAction)closeButton:(id)sender;
-(IBAction)scanButton:(id)sender;
@property(nonatomic,retain)IBOutlet UIView *scan_view;
@property(nonatomic,retain)IBOutlet UIView *scan_main_view;
@property(nonatomic,retain)IBOutlet ZBarReaderViewController *reader;


@property(nonatomic,retain)IBOutlet UITextField *emailTextField;
@property(nonatomic,retain)IBOutlet UITextField *loginPassTextField;
@property(nonatomic,retain)IBOutlet UITextField *signupPassField;
@property(nonatomic,retain)IBOutlet UITextField *SignupConPassTextField;
@property(nonatomic,retain)IBOutlet UIImageView *emailImgView;
@property(nonatomic,retain)IBOutlet UIImageView *passImgView;
@property(nonatomic,retain)IBOutlet UIImageView *conImgView;
@property(nonatomic,retain)IBOutlet UIImageView *loginpassImgView;
@property(nonatomic,retain)IBOutlet UIView *backView;
@property(nonatomic,retain)IBOutlet UIView *loginView;
@property(nonatomic,retain)IBOutlet UIView *signupView;

@end

//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "MerSignupViewController.h"
#import <QuartzCore/QuartzCore.h>
@interface MerSignupViewController ()

@end

@implementation MerSignupViewController
@synthesize emailImgView;
@synthesize reader;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    emailImgView.image=[UIImage imageNamed:@"si.png"];
    
    [self.view addSubview:_scan_main_view];
    _scan_main_view.hidden=YES;

     [_emailTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
	
    [_loginPassTextField addTarget:self action:@selector(textFieldDidChange2:) forControlEvents:UIControlEventEditingChanged];
    
     [_SignupConPassTextField addTarget:self action:@selector(textFieldDidChange1:) forControlEvents:UIControlEventEditingChanged];
       [_signupPassField addTarget:self action:@selector(textFieldDidChange1:) forControlEvents:UIControlEventEditingChanged];
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
   
    
    NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSString *subjectString =_emailTextField.text;
    
    if (textField==_signupPassField)
    {
         [_signupPassField resignFirstResponder];
         //[_SignupConPassTextField becomeFirstResponder];
    }
    else
    {
    
    if (textField==_loginPassTextField)
    {
        [_loginPassTextField resignFirstResponder];
    }
    else
    {
    if((_emailTextField.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
    {
        
      //  NSLog(@"NOT VALID");
        emailImgView.image=[UIImage imageNamed:@"sr.png"];
        
        _loginView.hidden=YES;
        _signupView.hidden=YES;
    }
    
    else
    {
        
       // NSLog(@"VALID");
        
        if ((_emailTextField.text.length==0))
        {
            emailImgView.image=[UIImage imageNamed:@"si.png"];
        }
        else
        {
            
            emailImgView.image=[UIImage imageNamed:@"sg.png"];
                        
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/checkEmailExist",CONFIG_BASE_URL]];
            
            NSLog(@"HELLO:%@",url);
            
            __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
            
                        
            [request_post setValidatesSecureCertificate:NO];
            [request_post setPostValue:_emailTextField.text forKey:@"email"];
          
            
            
            [request_post setTimeOutSeconds:30];
            
            
            [request_post setCompletionBlock:^{
                NSString *responseString = [request_post responseString];
                
                NSMutableData *results1 = [responseString JSONValue];
                
                
                
                
                
                NSString *error_string=[results1 valueForKeyPath:@"Response.email"];
                
                 NSString *email_available=@"";
                
                if ([error_string isEqualToString:@"Email ID Exists"])
                {
                    email_available=@"YES";
                }
                else
                {
                    email_available=@"NO";
                }
               
                
                
                if ([email_available isEqualToString:@"YES"])
                {
                    _loginView.hidden=NO;
                    _signupView.hidden=YES;
                    [_backView addSubview:_loginView];
                    [_loginPassTextField becomeFirstResponder];
                    
                    
                }
                else
                {
                    _loginView.hidden=YES;
                    _signupView.hidden=NO;
                    [_backView addSubview:_signupView];
                    if (textField==_emailTextField)
                    {
                        [_signupPassField becomeFirstResponder];
                    }
                    if (textField==_signupPassField)
                    {
                        [_SignupConPassTextField becomeFirstResponder];
                    }
                    if (textField==_SignupConPassTextField)
                    {
                        
                        if (![_SignupConPassTextField.text isEqualToString:_signupPassField.text])
                        {
                            
                        }
                        else
                            [_SignupConPassTextField resignFirstResponder];
                    }
                }
                
                
                
            }];
            [request_post setFailedBlock:^{
                NSError *error = [request_post error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [request_post startAsynchronous];

            
            
           
            
                        
            
            
            
        }
        
        
      
    }
    }
    }
    
    
    

    
    return YES;
}


-(IBAction)login_button
{
    NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSString *subjectString =_emailTextField.text;
    
    
    if((_emailTextField.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
    {
        
       // NSLog(@"NOT VALID");
        emailImgView.image=[UIImage imageNamed:@"sr.png"];
        
        _loginView.hidden=YES;
        _signupView.hidden=YES;
    }
    
    else
    {
        
      //  NSLog(@"VALID");
        
        if ((_emailTextField.text.length==0))
        {
            emailImgView.image=[UIImage imageNamed:@"si.png"];
        }
        else
        {
            
            emailImgView.image=[UIImage imageNamed:@"sg.png"];
            
            
            NSLog(@"EMAIL:%@",_emailTextField.text);
            NSLog(@"EMAIL:%@",_loginPassTextField.text);
            
            [_emailTextField resignFirstResponder];
            [_loginPassTextField resignFirstResponder];
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/merchant/merchnatLogin",CONFIG_BASE_URL]];
            
            NSLog(@"HELLO:%@",url);
            
            __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
            
            
            
            
            
            [request_post setValidatesSecureCertificate:NO];
            [request_post setPostValue:_emailTextField.text forKey:@"email"];
            [request_post setPostValue:_loginPassTextField.text forKey:@"password"];
            
            
            [request_post setTimeOutSeconds:30];
            
            
            [request_post setCompletionBlock:^{
                NSString *responseString = [request_post responseString];
                             
                NSMutableData *results1 = [responseString JSONValue];
                
                
                
                
                
                NSString *error_string=[results1 valueForKeyPath:@"error.description"];
                
                NSLog(@"HEADER DES:%@",results1);
                
                if ([error_string length]!=0)
                {
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Tab A Friend" message:error_string delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];
                    
                }
                else
                {
                    NSLog(@"RESULTS HELL:%@",responseString);
                    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
                    
                    NSArray *userID=[results1 valueForKeyPath:@"Response.user.pk_id"];
                     NSArray *first_name=[results1 valueForKeyPath:@"Response.user.first_name"];
                     NSArray *fk_facebook_user_id=[results1 valueForKeyPath:@"Response.user.fk_facebook_user_id"];
                     NSArray *image=[results1 valueForKeyPath:@"Response.user.image"];
                     NSArray *address1=[results1 valueForKeyPath:@"Response.user.address1"];
                     NSArray *address2=[results1 valueForKeyPath:@"Response.user.address2"];
                     NSArray *city=[results1 valueForKeyPath:@"Response.user.city"];
                     NSArray *state=[results1 valueForKeyPath:@"Response.user.state"];
                     NSArray *zipcode=[results1 valueForKeyPath:@"Response.user.zipcode"];
                     NSArray *user_type=[results1 valueForKeyPath:@"Response.user.user_type"];
                    
                    
                    [add setObject:fk_facebook_user_id[0] forKey:@"FB_USERID"];
                    [add setObject:userID[0] forKey:@"MERCHANT_USER_ID"];
                    [add setObject:@"" forKey:@"FB_ACCESS_TOKEN"];
                    
                    
                    [add setObject:first_name[0] forKey:@"NAME"];
                    [add setObject:image[0] forKey:@"IMAGE"];
                    [add setObject:address1[0] forKey:@"AD1"];
                    [add setObject:address2[0] forKey:@"AD2"];
                    [add setObject:city[0] forKey:@"CITY"];
                    [add setObject:state[0] forKey:@"STATE"];
                    [add setObject:zipcode[0] forKey:@"ZIP"];
                    [add setObject:@"MERCHANT" forKey:@"USER_TYPE"];
                    
                    
                    NSLog(@"userID:%@",userID);
                    
                    if (IS_IPHONE_5)
                    {
                        MerViewController *signupView=[[MerViewController alloc]initWithNibName:@"MerViewController" bundle:nil];
                        [self presentViewController:signupView animated:NO completion:nil];
                    }
                    else
                    {
                        MerViewController *signupView=[[MerViewController alloc]initWithNibName:@"MerViewController4" bundle:nil];
                        [self presentViewController:signupView animated:NO completion:nil];
                    }
                    

                    
                    
                }
                
;
                
                               
                
                
                
            }];
            [request_post setFailedBlock:^{
                NSError *error = [request_post error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [request_post startAsynchronous];

            
        }
        
        
        
    }
}

- (void)textFieldDidChange:(UITextField*)sender {
    
    
    
    if (sender==_emailTextField)
    {
        NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
        NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        NSString *subjectString =sender.text;
        
        
        if((sender.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
        {
            
           // NSLog(@"NOT VALID");
            emailImgView.image=[UIImage imageNamed:@"sr.png"];
            
            _loginView.hidden=YES;
            _signupView.hidden=YES;
        }
        
        else
        {
            
           // NSLog(@"VALID");
            
            if ((sender.text.length==0))
            {
                emailImgView.image=[UIImage imageNamed:@"si.png"];
            }
            else
            {
                
                emailImgView.image=[UIImage imageNamed:@"sg.png"];
                
            }
        }
    }
    else if (sender==_SignupConPassTextField)
    {
        
       // NSLog(@"ENTER");
        
        
        if ([_SignupConPassTextField.text isEqualToString:_signupPassField.text])
        {
            [_SignupConPassTextField resignFirstResponder];
            
            _passImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
            _conImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
        }
        else
        {
            _passImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
            _conImgView.image=[UIImage imageNamed:@"passwordicon-red.png"];
        }
        
    }
    
    
    
    
}
- (void)textFieldDidChange1:(UITextField*)sender {
    
    
    
    
   // NSLog(@"ENTER");
    
    if ([_signupPassField.text length]<6)
    {
        _passImgView.image=[UIImage imageNamed:@"passwordicon-red.png"];
        
    }
    else
    {
        if ([_SignupConPassTextField.text isEqualToString:_signupPassField.text])
        {
            [_SignupConPassTextField resignFirstResponder];
            
            _passImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
            _conImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
        }
        else
        {
            _passImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
            _conImgView.image=[UIImage imageNamed:@"passwordicon-red.png"];
        }
    }
    
    
    
    
    
}

- (void)textFieldDidChange2:(UITextField*)sender {
   
    
   
        
       // NSLog(@"ENTER");
    
    if ([_loginPassTextField.text length]<6)
    {
        _loginpassImgView.image=[UIImage imageNamed:@"passwordicon-red.png"];
        
    }
    else
    {
        _loginpassImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
    }
    
   
    
    
}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
     return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back_button
{
    if (IS_IPHONE_5)
    {
        SignupViewController *signupView=[[SignupViewController alloc]initWithNibName:@"SignupViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SignupViewController *signupView=[[SignupViewController alloc]initWithNibName:@"SignupViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    
    
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ////(@"info");
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    ////(@"datas %@",symbol.data);
    
    NSString *result=[NSString stringWithFormat:@"%@",symbol.data];
    
    NSArray *arrString = [result componentsSeparatedByString:@" "];
    
    NSString *finalString=@"";
    
    for(int i=0; i<arrString.count;i++){
        if([[arrString objectAtIndex:i] rangeOfString:@"http"].location != NSNotFound)
            ////(@"ARRA:%@", [arrString objectAtIndex:i]);
            
            finalString=[arrString objectAtIndex:i];
    }
    
    
    
    
    
    
    if ([finalString rangeOfString:@"http"].location == NSNotFound)
    {
        ////(@"string does not contain bla");
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"This code not contains valid URL" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
        ////(@"string contains bla!");
        
        _scan_main_view.hidden=YES;
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalString]];
        
        [reader.view removeFromSuperview];
    }
    
    
    // resultImage.image =
    [info objectForKey: UIImagePickerControllerOriginalImage];
    
    
    //
    
    
    
    
}




-(IBAction)scanButton:(id)sender
{
    _scan_main_view.hidden=NO;
    
    
    
    
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    
    
    reader.wantsFullScreenLayout = NO;
    
    reader.showsZBarControls = NO;
    
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
	
    [reader.view setFrame:CGRectMake(0, 0, 320,540)];
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
	
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
				   config: ZBAR_CFG_ENABLE
					   to: 0];
    
    // hideview.frame=CGRectMake(0, 420, 320, 40);
	
    // present and release the controller
    
    [self.scan_view addSubview:reader.view];
    
}
-(IBAction)closeButton:(id)sender
{
    _scan_main_view.hidden=YES;
}

@end

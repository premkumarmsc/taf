//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "MerTapRoomViewController.h"
#import "UIImageView+WebCache.h"

@interface MerTapRoomViewController ()

@end

@implementation MerTapRoomViewController
@synthesize collectionView;


NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *contactIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;


NSString *contactIDName;






int editTag;


- (void)viewDidLoad
{
    
    
      
    
    [self getTapDetails];
    [self getMerchantDetails];
    
	// Do any additional setup after loading the view, typically from a nib.
}


-(void)getMerchantDetails
{
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    NSString *merID=[check objectForKey:@"MERCHANT_USER_ID"];
    NSString *neme=[check objectForKey:@"NAME"];
    NSString *merImage=[check objectForKey:@"IMAGE"];
    
    NSString *addrStr=[NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@",[check valueForKey:@"AD1"],[check valueForKey:@"AD2"],[check valueForKey:@"CITY"],[check valueForKey:@"CONTCAT_STATE"],[check valueForKey:@"STATE"],[check valueForKey:@"ZIP"]];
    
    
    _MerchentName.text=neme;
    _MerchentAddress.text=addrStr;
    
    @try {
        
        [_merchentImgView setImageWithURL:[NSURL URLWithString:merImage]
                     placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
        
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }
    
    
    
}

-(void)getTapDetails
{
    
        
        
       
        
        
        
        NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
        
        NSString *userID=[checkval objectForKey:@"MERCHANT_USER_ID"];
         NSString *tapID=[checkval objectForKey:@"LAST_TAPPED_ID"];
        
        // userID=@"230";
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/tap/retrieveTaps",CONFIG_BASE_URL]];
        
        NSLog(@"HELLO:%@",url);
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:userID forKey:@"userID"];
        [request_post setPostValue:tapID forKey:@"tapID"];
        
        
        [request_post setTimeOutSeconds:30];
        
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            
            
            
            
            
            
            
            
            NSMutableData *results1 = [responseString JSONValue];
            
            
            
            NSLog(@"RESULTS HELL:%@",results1);
            
            
            
            
            NSArray *temp= [results1 valueForKeyPath:@"Response.tapDetails"];
            
            
            
            
            for(NSDictionary *value in temp)
            {
                
                
                
                NSString *tapAmountArr=[value valueForKey:@"tap_amount"];
                 NSString *fromFName=[value valueForKey:@"senderFirstName"];
                 NSString *fromLName=[value valueForKey:@"senderLastName"];
                 NSString *fromImage=[value valueForKey:@"senderImage"];
                 NSString *fromID=[value valueForKey:@"fk_sender_id"];
                
                NSString *recFName=[value valueForKey:@"receiverFirstName"];
                NSString *recLName=[value valueForKey:@"receiverLastName"];
                NSString *recImage=[value valueForKey:@"receiverImage"];
                NSString *recID=[value valueForKey:@"fk_receiver_id"];
                
                
                NSString *merFName=[value valueForKey:@"merchantFirstName"];
                NSString *merLName=[value valueForKey:@"merchantLastName"];
                NSString *merImage=[value valueForKey:@"merchantImage"];
                NSString *merID=[value valueForKey:@"fk_merchant_id"];
                NSString *merAd1=[value valueForKey:@"merchantAddress1"];
                NSString *merAd2=[value valueForKey:@"merchantAddress2"];
                NSString *meeCity=[value valueForKey:@"merchantCity"];
                NSString *merState=[value valueForKey:@"merchantState"];
               
                 NSString *merZip=[value valueForKey:@"merchantZipcode"];
                
                
                 NSString *expireDate=[value valueForKey:@"expire_date"];
                 NSString *merLAt=[value valueForKey:@"merchantLat"];
                 NSString *merLong=[value valueForKey:@"merchantLong"];
                 NSString *shortURL=[value valueForKey:@"merchantZipcode"];
                 NSString *qrImage=[value valueForKey:@"qr_code_image"];
                 NSString *status=[value valueForKey:@"tap_status"];
                
                
                NSLog(@"TAP_AMOUNT:%@",tapAmountArr);
                              
                _AmountLabel.text=[NSString stringWithFormat:@"%@",tapAmountArr];
                _fromName.text=[NSString stringWithFormat:@"%@ %@",fromFName,fromLName];
                @try {
                   
                    [_fromImgView setImageWithURL:[NSURL URLWithString:fromImage]
                                    placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                }
                
                _toName.text=[NSString stringWithFormat:@"%@ %@",recFName,recLName];
                @try {
                    
                    [_toImgView setImageWithURL:[NSURL URLWithString:recImage]
                                 placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                }
                
                _MerchentName.text=[NSString stringWithFormat:@"%@ %@",merFName,merLName];
                @try {
                    
                    [_merchentImgView setImageWithURL:[NSURL URLWithString:merImage]
                                 placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                }
                
                 _MerchentAddress.text=[NSString stringWithFormat:@"%@,%@\n%@,%@\n%@",merAd1,merAd2,meeCity,merState,merZip];
                
                
                @try {
                    
                    [_QRImgView setImageWithURL:[NSURL URLWithString:qrImage]
                                     placeholderImage:[UIImage imageNamed:@"Qrcode.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                    
                    _QRImgView.image=[UIImage imageNamed:@"Qrcode.png"];
                }
                
                
                
                NSUserDefaults *addLat=[NSUserDefaults standardUserDefaults];
                                
                NSString *locLat1  = [NSString stringWithFormat:@"%@", merLAt];
                NSString * locLong1 = [NSString stringWithFormat:@"%@", merLong];
                
                [addLat setObject:locLat1 forKey:@"TO_LAT"];
                [addLat setObject:locLong1 forKey:@"TO_LONG"];
                
                
                
                
                
                
                
                NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
                
                [dateFormat1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                 
                
                NSDate *someDate = [dateFormat1 dateFromString:expireDate];
                
                
                
                
                if ([status isEqualToString:@"valid"])
                {
                    if ([expireDate isEqualToString:@"0000-00-00 00:00:00"])
                    {
                        _ExpiredLabel.text=@"";
                       
                        _exBackground.hidden=YES;
                        _QRStatusImgView.image=[UIImage imageNamed:@"expired-icon.png"];
                        
                        _backImage.hidden=YES;
                        _acceptButton.hidden=YES;
                        _labelText.hidden=YES;
                    }
                    else
                    {
                        if ([someDate timeIntervalSinceNow] < 0.0) {
                            // Date has passed
                            _ExpiredLabel.text=@"";
                            _exBackground.hidden=YES;
                            _QRStatusImgView.image=[UIImage imageNamed:@"expired-icon.png"];
                            
                            _backImage.hidden=YES;
                            _acceptButton.hidden=YES;
                            _labelText.hidden=YES;
                        }
                        else
                        {
                             NSDate *now = [NSDate date];
                           
                            NSCalendar *calendar = [NSCalendar currentCalendar];
                            
                            NSDate *fromDate;
                            NSDate *toDate;
                            
                            
                            [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                                         interval:NULL forDate:now];
                            [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                                         interval:NULL forDate:someDate];
                            
                            NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                                                       fromDate:fromDate toDate:toDate options:0];
                            
                            NSLog(@"DAYYYY:%ld",(long)[difference day]);
                            
                            _backImage.hidden=NO;
                            _acceptButton.hidden=NO;
                            _labelText.hidden=NO;
                            
                            if ((long)[difference day]==0) {
                                 _ExpiredLabel.text=[NSString stringWithFormat:@"EXPIRES IN TODAY"];
                            }
                            else
                            {
                            
                            _ExpiredLabel.text=[NSString stringWithFormat:@"EXPIRES IN %ld DAY(S)",(long)[difference day]];
                            }
                        }
                    }
                }
                else
                {
                    if ([status isEqualToString:@"expired"]||[expireDate isEqualToString:@"0000-00-00 00:00:00"])
                    {
                        _ExpiredLabel.text=@"";
                        _exBackground.hidden=YES;
                        _QRStatusImgView.image=[UIImage imageNamed:@"expired-icon.png"];
                        
                        _backImage.hidden=YES;
                        _acceptButton.hidden=YES;
                        _labelText.hidden=YES;
                        
                    }
                   
                
                     if ([status isEqualToString:@"used"])
                     {
                        _QRStatusImgView.image=[UIImage imageNamed:@"used.png"];
                         _exBackground.hidden=YES;
                          _ExpiredLabel.text=@"";
                         
                         _backImage.hidden=YES;
                         _acceptButton.hidden=YES;
                         _labelText.hidden=YES;
                     }
                    
                    
                    if ([status rangeOfString:@"converted"].location == NSNotFound) {
                        NSLog(@"string does not contain bla");
                    } else {
                        
                        _QRStatusImgView.image=[UIImage imageNamed:@"converted_orange.png"];
                        _exBackground.hidden=YES;
                        _ExpiredLabel.text=@"";
                        
                        _backImage.hidden=YES;
                        _acceptButton.hidden=YES;
                        _labelText.hidden=YES;
                    }

                    
                    
                   
                }
        
                
            }
            
            //[_tblView reloadData];
            
            
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];
 
}


-(IBAction)acceptTap
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/tap/tapChangeStatus",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"MERCHANT_USER_ID"];
   
    NSString *tapID=[check objectForKey:@"LAST_TAPPED_ID"];
    
    
    
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:tapID forKey:@"tapID"];
    [request_post1234 setPostValue:@"used" forKey:@"tap_status"];
    
    
    
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        
        [self viewDidLoad];
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}

-(IBAction)addBack
{
    
    if (IS_IPHONE_5)
    {
        MerRoomViewController *signupView=[[MerRoomViewController alloc]initWithNibName:@"MerRoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MerRoomViewController *signupView=[[MerRoomViewController alloc]initWithNibName:@"MerRoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    [textField resignFirstResponder];
    
      
    
    return YES;
    
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
   
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}




@end

//
//  ViewController.h
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <AddressBook/AddressBook.h>
#import <Social/Social.h>
#import "ZBarSDK.h"
#import "ZBarReaderView.h"

@interface MerViewController : UIViewController<FBLoginViewDelegate,FBFriendPickerDelegate,ZBarReaderDelegate,UIAlertViewDelegate>
{
     
}
-(IBAction)facebookConnect:(id)sender;
-(IBAction)signupConnect:(id)sender;
-(IBAction)closeButton:(id)sender;
-(IBAction)scanButton:(id)sender;
@property(nonatomic,retain)IBOutlet UIView *scan_view;
@property(nonatomic,retain)IBOutlet UIView *scan_main_view;
@property(nonatomic,retain)IBOutlet ZBarReaderViewController *reader;
-(IBAction)transactionButton:(id)sender;
-(IBAction)logout:(id)sender;
@property(nonatomic,retain)IBOutlet UIImageView *toImageView;
@property(nonatomic,retain)IBOutlet UILabel *toNameLabel;
@property(nonatomic,retain)IBOutlet UILabel *toAddressLabel;

@end

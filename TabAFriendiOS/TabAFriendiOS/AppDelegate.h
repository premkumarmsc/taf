//
//  AppDelegate.h
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
@class ViewController;
@class FriendsViewController;
@class MerViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;
@property (strong, nonatomic) FriendsViewController *FriendviewController;
@property (strong, nonatomic) MerViewController *MerviewController;

@end

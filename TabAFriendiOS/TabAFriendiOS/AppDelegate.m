//
//  AppDelegate.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"
#import "FriendsViewController.h"
#import "MerViewController.h"
#import "IQKeyBoardManager.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    
    NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
    
   // [IQKeyBoardManager installKeyboardManager];
    
    NSString *addressStatus=[checkSta objectForKey:@"FIRSTTIME"];
    
    ////(@"STS:%@",addressStatus);
    
    if ([addressStatus isEqualToString:@"NO"])
    {
        NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
        
        [checkSta setObject:@"NO" forKey:@"FIRSTTIME"];
    }
    else
    {
               
        
        NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
        
        [checkSta setObject:@"YES" forKey:@"FIRSTTIME"];
    }
   
    
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[NSString stringWithFormat:@"%@",[checkval objectForKey:@"USERID"]];
    
    NSLog(@"USER:%@",userID);

    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USER_TYPE"];
    
    NSLog(@"TYPE:%@",user);
    
    
    if ([user isEqualToString:@"MERCHANT"])
    {
        if (IS_IPHONE_5)
        {
            
            self.MerviewController = [[MerViewController alloc] initWithNibName:@"MerViewController" bundle:nil];
        }
        else
        {
            
            self.MerviewController = [[MerViewController alloc] initWithNibName:@"MerViewController4" bundle:nil];
        }
        
        self.window.rootViewController = self.MerviewController;

    }
    else
    {
    if ([userID isEqualToString:@""]||userID==[NSNull null]||[userID isEqualToString:@"(null)"])
    {
        if (IS_IPHONE_5)
        {
            
            self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
        }
        else
        {
            
            self.viewController = [[ViewController alloc] initWithNibName:@"ViewController4" bundle:nil];
        }
        
        self.window.rootViewController = self.viewController;
    }
    else
    {
        if (IS_IPHONE_5)
        {
            
            self.FriendviewController = [[FriendsViewController alloc] initWithNibName:@"FriendsViewController" bundle:nil];
        }
        else
        {
            
            self.FriendviewController = [[FriendsViewController alloc] initWithNibName:@"FriendsViewController4" bundle:nil];
        }
        
        self.window.rootViewController = self.FriendviewController;
    }
    
    }
    
    
    
    
    
    
    
    
    [self.window makeKeyAndVisible];
    return YES;
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // FBSample logic
    // if the app is going away, we close the session object
    [FBSession.activeSession close];
}

@end

//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "BusinessFriendsViewController.h"
#import "receiptentCell.h"
#import "UIImageView+WebCache.h"

@interface BusinessFriendsViewController ()
@property (nonatomic, retain) NSIndexPath *selectedIndexPath;
@end

@implementation BusinessFriendsViewController
@synthesize collectionView;
@synthesize selectedIndexPath = _selectedIndexPath;
receiptentCell *cell;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *contactIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;
NSMutableArray *facebookUserID;


NSString *contactIDName;



-(IBAction)Back
{
    if (IS_IPHONE_5)
    {
        SubCategoryViewController *signupView=[[SubCategoryViewController alloc]initWithNibName:@"SubCategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SubCategoryViewController *signupView=[[SubCategoryViewController alloc]initWithNibName:@"SubCategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}


int editTag;

-(void)getUpdation
{
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    
    NSString *nameStr=[NSString stringWithFormat:@"%@ %@",[check valueForKey:@"BUSINESS_FNAME"],[check valueForKey:@"BUSINESS_LNAME"]];
    
    _catNameLabel.text=nameStr;
    
    
    NSString *imgStr=[check valueForKey:@"BUSINESS_IMG"];
    
    @try {
        NSString *temp=imgStr;
        [_catImageView setImageWithURL:[NSURL URLWithString:temp]
                     placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }
    
    
    
    NSString *addrStr=[NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@",[check valueForKey:@"BUSINESS_AD1"],[check valueForKey:@"BUSINESS_AD2"],[check valueForKey:@"BUSINESS_CITY"],[check valueForKey:@"BUSINESS_STATE"],[check valueForKey:@"BUSINESS_COUNTRY"],[check valueForKey:@"BUSINESS_ZIP"]];
    
    
    _catAddressLabel.text=addrStr;
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:_addContactView];
    _addContactView.hidden=YES;
    [self.view addSubview:_contactEditView];
    _contactEditView.hidden=YES;
    
    NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
    
    [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
    
    
    [_editButton setTitle:@"EDIT" forState:UIControlStateNormal];
    editTag=0;
  
    
    
    
    
    
    imageThumpArray=[[NSMutableArray alloc]init];
    FnameArray=[[NSMutableArray alloc]init];
    LnameArray=[[NSMutableArray alloc]init];
    checkmark_array=[[NSMutableArray alloc]init];
    contactIDArray=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    address_entered_arr=[[NSMutableArray alloc]init];
    
     facebookUserID=[[NSMutableArray alloc]init];
    editAddress1Arr=[[NSMutableArray alloc]init];
    editAddress2Arr=[[NSMutableArray alloc]init];
    editCityArr=[[NSMutableArray alloc]init];
    editCountryArr=[[NSMutableArray alloc]init];
    editEmailArr=[[NSMutableArray alloc]init];
    editPhoneArr=[[NSMutableArray alloc]init];
    editStateArr=[[NSMutableArray alloc]init];
    editZipArr=[[NSMutableArray alloc]init];     
    facebookUserID=[[NSMutableArray alloc]init];
    contactSourceArray=[[NSMutableArray alloc]init];
    
    
    
    
    [self getFriendsList];
    [self getUserDetails];
    [self getUpdation];
    
    checkmarkArray=[[NSMutableArray alloc]init];
    
 
    
    
     [self.collectionView registerNib:[UINib nibWithNibName:@"receiptentCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)getFriendsList
{
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    //[checkval setObject:@"227" forKey:@"USERID"];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    NSString *fbID=[checkval objectForKey:@"FB_USERID"];
    NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
    
     //userID=@"227";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/retrieveUserContact",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
    
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        NSArray *temp= [results1 valueForKeyPath:@"Response.contacts"];
        
      

        
        for(NSDictionary *value in temp)
        {
            [imageThumpArray  addObject:[value valueForKey:@"user_image"]];
          
            
            [FnameArray  addObject:[value valueForKey:@"first_name"]];
            [LnameArray  addObject:[value valueForKey:@"last_name"]];
           
            [contactIDArray  addObject:[value valueForKey:@"pk_id"]];
            
          //  [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
            
            
            
            [editZipArr  addObject:[value valueForKey:@"zipcode"]];
            [editStateArr  addObject:[value valueForKey:@"state"]];
            [editPhoneArr  addObject:[value valueForKey:@"phone"]];
            [editEmailArr  addObject:[value valueForKey:@"email"]];
            
            [editCountryArr  addObject:[value valueForKey:@"country"]];
            [editCityArr  addObject:[value valueForKey:@"city"]];
            [editAddress2Arr  addObject:[value valueForKey:@"address2"]];
            [editAddress1Arr  addObject:[value valueForKey:@"address1"]];
            
            [contactSourceArray  addObject:[value valueForKey:@"contact_source"]];
             [facebookUserID  addObject:[value valueForKey:@"fk_facebook_user_id"]];
           
            
        }
        
        
        NSLog(@"FIRST NAME:%@",FnameArray);
        
        for (int i=0; i<[FnameArray count]; i++)
        {
            [checkmarkArray addObject:[NSString stringWithFormat:@"NO"]];
        }
        
        [collectionView reloadData];
        
        /*
        NSString *pk_id=[results1 valueForKeyPath:@"Response.user.pk_id"];
        NSString *fk_facebook_user_id=[results1 valueForKeyPath:@"Response.user.fk_facebook_user_id"];
        NSString *fk_merchant_package_id=[results1 valueForKeyPath:@"Response.user.fk_merchant_package_id"];
        NSString *first_name=[results1 valueForKeyPath:@"Response.user.first_name"];
        NSString *last_name=[results1 valueForKeyPath:@"Response.user.last_name"];
        NSString *email=[results1 valueForKeyPath:@"Response.user.email"];
        NSString *password=[results1 valueForKeyPath:@"Response.user.password"];
        NSString *signup_type=[results1 valueForKeyPath:@"Response.user.signup_type"];
        NSString *facebook_access_token=[results1 valueForKeyPath:@"Response.user.facebook_access_token"];
        NSString *image=[results1 valueForKeyPath:@"Response.user.image"];
        NSString *address1=[results1 valueForKeyPath:@"Response.user.address1"];
        NSString *address2=[results1 valueForKeyPath:@"Response.user.address2"];
        NSString *city=[results1 valueForKeyPath:@"Response.user.city"];
        NSString *state=[results1 valueForKeyPath:@"Response.user.state"];
        NSString *zipcode=[results1 valueForKeyPath:@"Response.user.zipcode"];
        NSString *country=[results1 valueForKeyPath:@"Response.user.country"];
        NSString *phone=[results1 valueForKeyPath:@"Response.user.phone"];
        NSString *user_security_pin=[results1 valueForKeyPath:@"Response.user.user_security_pin"];
        NSString *user_type=[results1 valueForKeyPath:@"Response.user.user_type"];
        NSString *location_latitude=[results1 valueForKeyPath:@"Response.user.location_latitude"];
        NSString *location_longitude=[results1 valueForKeyPath:@"Response.user.location_longitude"];
        NSString *created_date=[results1 valueForKeyPath:@"Response.user.created_date"];
        NSString *created_ipaddress=[results1 valueForKeyPath:@"Response.user.created_ipaddress"];
        NSString *updated_date=[results1 valueForKeyPath:@"Response.user.updated_date"];
        NSString *updated_ipaddress=[results1 valueForKeyPath:@"Response.user.updated_ipaddress"];
        NSString *browser_details=[results1 valueForKeyPath:@"Response.user.browser_details"];
        NSString *user_status=[results1 valueForKeyPath:@"Response.user.user_status"];
        NSString *merchant_company_details=[results1 valueForKeyPath:@"Response.user.merchant_company_details"];
        
        
        NSLog(@"EMAIl:%@",email);
        
        */
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    
}



-(IBAction)editButtonClick
{
    if (editTag==0)
    {
        [_editButton setTitle:@"DONE" forState:UIControlStateNormal];
        editTag=1;
    }
   else
   {
       [_editButton setTitle:@"EDIT" forState:UIControlStateNormal];
       editTag=0;
   }
    
    [collectionView reloadData];
}
-(IBAction)addContact
{
   _addContactView.hidden=NO;
    [self getCountry];
}
-(void)getCountry
{
    countryArray=[[NSMutableArray alloc]init];
    
    
         NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/general/getCountryList",CONFIG_BASE_URL]];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setDelegate:self];
       [request startAsynchronous];

    
}
- (void)requestFinished:(ASIHTTPRequest *)request1
{
    
    NSString *responseString = [request1 responseString];
    
    
    
    NSMutableData *results1 = [responseString JSONValue];
    
    NSLog(@"HELLO:%@",responseString);
    
    
    NSArray *temp= [results1 valueForKeyPath:@"Response.countries"];
    
    
    
    
    for(NSDictionary *value in temp)
    {
        [countryArray  addObject:[value valueForKey:@"country_name"]];
        
        
    }
    
    NSLog(@"TEMPARY:%@",countryArray);
    
    NSUserDefaults *valueAdd=[NSUserDefaults standardUserDefaults];
    [valueAdd setObject:countryArray forKey:@"COUNTRY_ARRAY"];
    
    
    [_countrybtn setTitle:[NSString stringWithFormat:@"    %@",countryArray[0]] forState:UIControlStateNormal];
    
    
    
}

-(IBAction)addImage
{
    [_firstName resignFirstResponder];
    [_lastname resignFirstResponder];
    [_address2 resignFirstResponder];
    [_address1 resignFirstResponder];
    [_city resignFirstResponder];
    [_state resignFirstResponder];
    [_zipCode resignFirstResponder];
    
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
}

-(IBAction)addBack
{
    
    _editaddress1.text=@"";
    _editaddress2.text=@"";
    _editcity.text=@"";
    _editemailID.text=@"";
    _editfirstName.text=@"";
    _editstate.text=@"";
    _editphoneNo.text=@"";
    
    _addContactView.hidden=YES;
    _contactEditView.hidden=YES;
}
-(IBAction)addSave
{
   
    countrystr=@"United States";
    
    NSString *fname=_firstName.text;
    NSString *lname=_lastname.text;
    NSString *address1text=_address1.text;
    NSString *address2text=_address2.text;
    NSString *citystr=_city.text;
    NSString *statestr=_state.text;
    NSString *zipCodeStr=_zipCode.text;
    
    NSString *emailStr=_emailID.text;
    NSString *phoneStr=_phoneNo.text;
    
    
    
    //////(@"COUNTRY:%@",countrystr);
    
    if ([fname isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the first name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        if ([lname isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the last name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            if ([emailStr isEqualToString:@""])
            {
                
                UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [loginalert show];
                
            }
            else
            {
                //////(@"SUCCESS");
                
                NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                NSString *subjectString =emailStr;
                
                
                if((emailStr.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                {
                    
                    if ([emailTest evaluateWithObject:subjectString] != YES)
                        
                    {
                        
                        
                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [loginalert show];
                        
                        
                    }
                    
                    
                }
                
                else
                {
                    
                    
                    
                    [self addcontectFun];
                    
                }
            }
        }
    }
    
    
    

    
}

-(void)addcontectFun
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/createContact",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
   
    
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://www.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    //////(@"USER IMAGE:%@",userImage);
    
    
    NSString *fname=_firstName.text;
    NSString *lname=_lastname.text;
    NSString *address1text=_address1.text;
    NSString *address2text=_address2.text;
    NSString *citystr=_city.text;
    NSString *statestr=_state.text;
    NSString *zipCodeStr=_zipCode.text;
    NSString *emailStr=_emailID.text;
    
    NSUserDefaults *img=[NSUserDefaults standardUserDefaults];
    
    NSString *urlstr=[img objectForKey:@""];
    
    
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:fbuserid forKey:@"facebook_userid"];
    [request_post1234 setPostValue:fname forKey:@"first_name"];
    [request_post1234 setPostValue:lname forKey:@"last_name"];
    [request_post1234 setPostValue:address1text forKey:@"address1"];
    [request_post1234 setPostValue:address2text forKey:@"address2"];
    [request_post1234 setPostValue:citystr forKey:@"city"];
    [request_post1234 setPostValue:statestr forKey:@"state"];
    [request_post1234 setPostValue:countrystr forKey:@"country"];
    [request_post1234 setPostValue:zipCodeStr forKey:@"zipcode"];
    [request_post1234 setPostValue:_emailID.text forKey:@"email"];
    [request_post1234 setPostValue:_phoneNo.text forKey:@"phone"];
    [request_post1234 setPostValue:userImage forKey:@"user_image"];
   [request_post1234 setPostValue:@"iPhone" forKey:@"contact_source"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        [_firstName resignFirstResponder];
        [_lastname resignFirstResponder];
        [_address2 resignFirstResponder];
        [_address1 resignFirstResponder];
        [_city resignFirstResponder];
        [_state resignFirstResponder];
        [_zipCode resignFirstResponder];
        
        _firstName.text=@"";
        _lastname.text=@"";
        _address2.text=@"";
        _address1.text=@"";
        _city.text=@"";
        _state.text=@"";
        _zipCode.text=@"";
        _emailID.text=@"";
        _phoneNo.text=@"";
       
        //[_countrybtn setTitle:@"  United States" forState:UIControlStateNormal];
        _contactAddView.hidden=YES;
        _addContactView.hidden=YES;
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
     
}

-(IBAction)addContry
{
    LeveyPopListView *lplv = [[LeveyPopListView alloc] initWithTitle:@"Select country" options:countryArray handler:^(NSInteger anIndex)
                              {
                                  //////(@"%@",[NSString stringWithFormat:@"You have CLICKED %@", countryArray[anIndex]]);
                                  
                                  [_countrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[anIndex]] forState:UIControlStateNormal];
                                  
                                  _countrybtn=countryArray[anIndex];
                                  
                              }];
    //    lplv.delegate = self;
    [lplv showInView:self.view animated:YES];
}

-(void)getUserDetails
{
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    NSString *fbID=[checkval objectForKey:@"FB_USERID"];
    NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
    
   // userID=@"230";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserDetails",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
    
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        NSString *pk_id=[results1 valueForKeyPath:@"Response.user.pk_id"];
        NSString *fk_facebook_user_id=[results1 valueForKeyPath:@"Response.user.fk_facebook_user_id"];
        NSString *fk_merchant_package_id=[results1 valueForKeyPath:@"Response.user.fk_merchant_package_id"];
         NSString *first_name=[results1 valueForKeyPath:@"Response.user.first_name"];
          NSString *last_name=[results1 valueForKeyPath:@"Response.user.last_name"];
          NSString *email=[results1 valueForKeyPath:@"Response.user.email"];
          NSString *password=[results1 valueForKeyPath:@"Response.user.password"];
          NSString *signup_type=[results1 valueForKeyPath:@"Response.user.signup_type"];
          NSString *facebook_access_token=[results1 valueForKeyPath:@"Response.user.facebook_access_token"];
          NSString *image=[results1 valueForKeyPath:@"Response.user.image"];
          NSString *address1=[results1 valueForKeyPath:@"Response.user.address1"];
          NSString *address2=[results1 valueForKeyPath:@"Response.user.address2"];
          NSString *city=[results1 valueForKeyPath:@"Response.user.city"];
          NSString *state=[results1 valueForKeyPath:@"Response.user.state"];
          NSString *zipcode=[results1 valueForKeyPath:@"Response.user.zipcode"];
          NSString *country=[results1 valueForKeyPath:@"Response.user.country"];
          NSString *phone=[results1 valueForKeyPath:@"Response.user.phone"];
          NSString *user_security_pin=[results1 valueForKeyPath:@"Response.user.user_security_pin"];
          NSString *user_type=[results1 valueForKeyPath:@"Response.user.user_type"];
          NSString *location_latitude=[results1 valueForKeyPath:@"Response.user.location_latitude"];
          NSString *location_longitude=[results1 valueForKeyPath:@"Response.user.location_longitude"];
          NSString *created_date=[results1 valueForKeyPath:@"Response.user.created_date"];
          NSString *created_ipaddress=[results1 valueForKeyPath:@"Response.user.created_ipaddress"];
          NSString *updated_date=[results1 valueForKeyPath:@"Response.user.updated_date"];
          NSString *updated_ipaddress=[results1 valueForKeyPath:@"Response.user.updated_ipaddress"];
          NSString *browser_details=[results1 valueForKeyPath:@"Response.user.browser_details"];
          NSString *user_status=[results1 valueForKeyPath:@"Response.user.user_status"];
          NSString *merchant_company_details=[results1 valueForKeyPath:@"Response.user.merchant_company_details"];
        
      
        NSLog(@"EMAIl:%@",email);
        
        
              
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];

}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [contactIDArray count];
}


- (receiptentCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    if (editTag==1)
    {
        cell.transparentView.hidden=NO;
        
        
        [cell.editBtn setTag:indexPath.row];
        [cell.editBtn addTarget:self action:@selector(updatebuttonClicked:)
                 forControlEvents:UIControlEventTouchDown];

        [cell.deletebtn setTag:indexPath.row];
        [cell.deletebtn addTarget:self action:@selector(deletebuttonClicked:)
               forControlEvents:UIControlEventTouchDown];
        
    }
    
    else
    {
       cell.transparentView.hidden=YES;
    }
    if ([checkmarkArray[indexPath.row]isEqualToString:@"YES"])
    {
        cell.check_image_view.image=[UIImage imageNamed:@"click.png"];
    }
    else
    {
        cell.check_image_view.image=[UIImage imageNamed:@"checkbox.png"];
    }
    
    @try {
        cell.cell_title_Label.text=[NSString stringWithFormat:@"%@ %@",FnameArray[indexPath.row],LnameArray[indexPath.row]];
    }
    @catch (NSException *exception) {
        
    }
    
    
    
    
    @try {
        NSString *temp=[imageThumpArray objectAtIndex:indexPath.row];
        [cell.image_view setImageWithURL:[NSURL URLWithString:temp]
                        placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }
    return cell;
}

-(void)updatebuttonClicked:(UIButton*)button
{
    
    _contactEditView.hidden=NO;
    [self getCountry];
    
    contactIDSelected=contactIDArray[(long int)[button tag]];
    
    NSLog(@"Contact ID:%@", contactIDSelected);
    
    
    NSString *temp=[imageThumpArray objectAtIndex:(long int)[button tag]];
 
    
    UIImageView *imv=[[UIImageView alloc]init];
    
    [imv setImageWithURL:[NSURL URLWithString:temp]
placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
    
    
    NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
    
    [imgtemp setObject:temp forKey:@"USER_IMAGE"];
    
    
  
    [_editimageButton setImage:imv.image forState:UIControlStateNormal];
    
    
    [_editimageButton setTitle:nil forState:UIControlStateNormal];
    
   
    @try {
        _editfirstName.text=FnameArray[(long int)[button tag]];
        _editlastname.text=LnameArray[(long int)[button tag]];
        _editaddress1.text=editAddress1Arr[(long int)[button tag]];
        _editaddress2.text=editAddress2Arr[(long int)[button tag]];
        _editcity.text=editCityArr[(long int)[button tag]];
        _editstate.text=editStateArr[(long int)[button tag]];
        
        NSLog(@"Hello:%@",editZipArr[(long int)[button tag]]);
        
        
        _editzipCode.text=[NSString stringWithFormat:@"%@",editZipArr[(long int)[button tag]]];
        _editemailID.text=editEmailArr[(long int)[button tag]];
        
        NSLog(@"PH%@",editPhoneArr[(long int)[button tag]]);
        
         [_editcountryButton setTitle:[NSString stringWithFormat:@"    %@",editCountryArr[(long int)[button tag]]] forState:UIControlStateNormal];
        
        editcountrystr=editCountryArr[(long int)[button tag]];
        
        _editphoneNo.text=[NSString stringWithFormat:@"%@",editPhoneArr[(long int)[button tag]]];
    }
    @catch (NSException *exception)
    {
        
    }
    
    
        
}
-(IBAction)searchBtn
{
   
    [_searchTextField resignFirstResponder];
    
    if ([_searchTextField.text isEqualToString:@""])
    {
        [self viewDidLoad];
    }
    else
    {
         NSLog(@"SEAR:%@",_searchTextField.text);
        
        [self searchResults];
    }
}

-(void)searchResults
{
   
    
    
    
    imageThumpArray=[[NSMutableArray alloc]init];
    FnameArray=[[NSMutableArray alloc]init];
    LnameArray=[[NSMutableArray alloc]init];
    checkmark_array=[[NSMutableArray alloc]init];
    contactIDArray=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    address_entered_arr=[[NSMutableArray alloc]init];
    facebookUserID=[[NSMutableArray alloc]init];
    
    editAddress1Arr=[[NSMutableArray alloc]init];
    editAddress2Arr=[[NSMutableArray alloc]init];
    editCityArr=[[NSMutableArray alloc]init];
    editCountryArr=[[NSMutableArray alloc]init];
    editEmailArr=[[NSMutableArray alloc]init];
    editPhoneArr=[[NSMutableArray alloc]init];
    editStateArr=[[NSMutableArray alloc]init];
    editZipArr=[[NSMutableArray alloc]init];
    facebookUserID=[[NSMutableArray alloc]init];
    contactSourceArray=[[NSMutableArray alloc]init];
     checkmarkArray=[[NSMutableArray alloc]init];
    
    
    
    
        NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
        
        NSString *userID=[checkval objectForKey:@"USERID"];
        NSString *fbID=[checkval objectForKey:@"FB_USERID"];
        NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
        
        // userID=@"230";
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/searchContacts",CONFIG_BASE_URL]];
        
        NSLog(@"HELLO:%@",url);
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:userID forKey:@"userID"];
         [request_post setPostValue:_searchTextField.text forKey:@"searchCriteriaText"];
        
        
        [request_post setTimeOutSeconds:30];
        
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            
            
            
            
            NSMutableData *results1 = [responseString JSONValue];
            
            
            NSLog(@"RESULTS HELL:%@",results1);
            
            NSArray *temp= [results1 valueForKeyPath:@"Response.contacts"];
            
            
            
            
            for(NSDictionary *value in temp)
            {
                [imageThumpArray  addObject:[value valueForKey:@"user_image"]];
                
                
                [FnameArray  addObject:[value valueForKey:@"first_name"]];
                [LnameArray  addObject:[value valueForKey:@"last_name"]];
                
                [contactIDArray  addObject:[value valueForKey:@"pk_id"]];
                
                //  [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
                
                
                
                [editZipArr  addObject:[value valueForKey:@"zipcode"]];
                [editStateArr  addObject:[value valueForKey:@"state"]];
                [editPhoneArr  addObject:[value valueForKey:@"phone"]];
                [editEmailArr  addObject:[value valueForKey:@"email"]];
                
                [editCountryArr  addObject:[value valueForKey:@"country"]];
                [editCityArr  addObject:[value valueForKey:@"city"]];
                [editAddress2Arr  addObject:[value valueForKey:@"address2"]];
                [editAddress1Arr  addObject:[value valueForKey:@"address1"]];
                
                [contactSourceArray  addObject:[value valueForKey:@"contact_source"]];
                 [facebookUserID  addObject:[value valueForKey:@"fk_facebook_user_id"]];
                
                
            }
            
            
            NSLog(@"FIRST NAME:%@",FnameArray);
            
            for (int i=0; i<[FnameArray count]; i++)
            {
                [checkmarkArray addObject:[NSString stringWithFormat:@"NO"]];
            }
            
            [collectionView reloadData];
            
       
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];
        
    }


-(IBAction)editSave
{
    
    
    
  
    editcountrystr=@"United States";
    
    
    //////(@"COUNTRY:%@",countrystr);
    
    if ([_editfirstName.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the first name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        if ([_editemailID.text isEqualToString:@""])
        {
            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [loginalert show];
       
        }
        else
        {
            //////(@"SUCCESS");
            
            NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
            NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
            NSString *subjectString =_editemailID.text;
            
            
            if((_editemailID.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
            {
                
                if ([emailTest evaluateWithObject:subjectString] != YES)
                    
                {
                    
                    
                    UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                               cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    [loginalert show];
                    
                    
                }
                
                
            }
            
            else
            {
                
                
                
                [self editcontectFun];
                
            }
        }
    }
    
    
    
    
    
}



-(void)editcontectFun
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/updateContact",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
    
    
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://www.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    //////(@"USER IMAGE:%@",userImage);
    
    
    NSString *fname=_editfirstName.text;
    NSString *lname=_editlastname.text;
    NSString *address1text=_editaddress1.text;
    NSString *address2text=_editaddress2.text;
    NSString *citystr=_editcity.text;
    NSString *statestr=_editstate.text;
    NSString *zipCodeStr=_editzipCode.text;
    NSString *emailStr=_editemailID.text;
    
   
    
    
    
    [request_post1234 setPostValue:contactIDSelected forKey:@"contactID"];
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:fbuserid forKey:@"facebook_userid"];
    [request_post1234 setPostValue:fname forKey:@"first_name"];
    [request_post1234 setPostValue:lname forKey:@"last_name"];
    [request_post1234 setPostValue:address1text forKey:@"address1"];
    [request_post1234 setPostValue:address2text forKey:@"address2"];
    [request_post1234 setPostValue:citystr forKey:@"city"];
    [request_post1234 setPostValue:statestr forKey:@"state"];
    [request_post1234 setPostValue:editcountrystr forKey:@"country"];
    [request_post1234 setPostValue:zipCodeStr forKey:@"zipcode"];
    [request_post1234 setPostValue:_editemailID.text forKey:@"email"];
    [request_post1234 setPostValue:_editphoneNo.text forKey:@"phone"];
    [request_post1234 setPostValue:userImage forKey:@"user_image"];
   // [request_post1234 setPostValue:@"iPhone" forKey:@"contact_source"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        [_firstName resignFirstResponder];
        [_lastname resignFirstResponder];
        [_address2 resignFirstResponder];
        [_address1 resignFirstResponder];
        [_city resignFirstResponder];
        [_state resignFirstResponder];
        [_zipCode resignFirstResponder];
        
        _firstName.text=@"";
        _lastname.text=@"";
        _address2.text=@"";
        _address1.text=@"";
        _city.text=@"";
        _state.text=@"";
        _zipCode.text=@"";
        _emailID.text=@"";
        _phoneNo.text=@"";
        
        //[_countrybtn setTitle:@"  United States" forState:UIControlStateNormal];
        _contactAddView.hidden=YES;
        _addContactView.hidden=YES;
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}




-(IBAction)editContry
{
    LeveyPopListView *lplv = [[LeveyPopListView alloc] initWithTitle:@"Select country" options:countryArray handler:^(NSInteger anIndex)
                              {
                                  //////(@"%@",[NSString stringWithFormat:@"You have CLICKED %@", countryArray[anIndex]]);
                                  
                                  [_editcountrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[anIndex]] forState:UIControlStateNormal];
                                  
                                  editcountrystr=countryArray[anIndex];
                                  
                              }];
    //    lplv.delegate = self;
    [lplv showInView:self.view animated:YES];
}

-(IBAction)editImage
{
    [_editfirstName resignFirstResponder];
    [_editlastname resignFirstResponder];
    [_editaddress1 resignFirstResponder];
    [_editaddress2 resignFirstResponder];
    [_editcity resignFirstResponder];
    [_editstate resignFirstResponder];
    [_editzipCode resignFirstResponder];
    
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
  
}



-(void)deletebuttonClicked:(UIButton*)button
{
   
    
    contactIDName=contactIDArray[(long int)[button tag]];
    
    firstalert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Do you want to delete this contact?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [firstalert show];
    
    
    
    
    
}


- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView==firstalert)
    {
        if (buttonIndex == 0)
        {
            
            
        }else if (buttonIndex == 1)
        {
            
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/deleteContact",CONFIG_BASE_URL]];
            
            
            __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
            
            
            NSLog(@"Contact ID:%@",contactIDName);
            
            NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
            
            NSString *userID=[checkval objectForKey:@"USERID"];
            
            
            [requestmethod setPostValue:contactIDName forKey:@"contactID"];
             [requestmethod setPostValue:userID forKey:@"userID"];
            
            [requestmethod setTimeOutSeconds:30];
            
            
            [requestmethod setCompletionBlock:^{
                NSString *responseString23 = [requestmethod responseString];
                NSMutableData *results11 = [responseString23 JSONValue];
                
                
                
                
               NSLog(@"RESULTS:%@",results11);
                
                [self viewDidLoad];
                
                
            }];
            [requestmethod setFailedBlock:^{
                NSError *error = [requestmethod error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [requestmethod startAsynchronous];
            
        }
    }
    
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if (editTag==0) {
        
    
        NSString *email_str=[NSString stringWithFormat:@"%@",editEmailArr[indexPath.row]];
        NSString *phoneStr=[NSString stringWithFormat:@"%@",editPhoneArr[indexPath.row]];
        
        if ([email_str isEqualToString:@""])
        {
            
            
            NSString *fnandLnStr=[NSString stringWithFormat:@"Please add %@ %@'s email address",FnameArray[indexPath.row],LnameArray[indexPath.row]];
            
            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Email ID Missing" message:fnandLnStr delegate:self
                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [loginalert show];

            
            _contactEditView.hidden=NO;
            [self getCountry];
            
            contactIDSelected=contactIDArray[indexPath.row];
            
            NSLog(@"Contact ID:%@", contactIDSelected);
            
            
            NSString *temp=[imageThumpArray objectAtIndex:indexPath.row];
            
            
            UIImageView *imv=[[UIImageView alloc]init];
            
            [imv setImageWithURL:[NSURL URLWithString:temp]
                placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
            
            
            NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
            
            [imgtemp setObject:temp forKey:@"USER_IMAGE"];
            
            
            
            [_editimageButton setImage:imv.image forState:UIControlStateNormal];
            
            
            [_editimageButton setTitle:nil forState:UIControlStateNormal];
            
            
            @try {
                @try {
                    _editfirstName.text=FnameArray[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                
                @try {
                    _editlastname.text=LnameArray[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                @try {
                    _editaddress1.text=editAddress1Arr[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                @try {
                    _editaddress2.text=editAddress2Arr[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                @try {
                    _editcity.text=editCityArr[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                @try {
                    _editstate.text=editStateArr[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                
                @try {
                    _editzipCode.text=[NSString stringWithFormat:@"%@",editZipArr[indexPath.row]];
                    
                }
                @catch (NSException *exception) {
                    
                }
                
                
                
                
                @try {
                    _editemailID.text=editEmailArr[indexPath.row];
                    
                }
                @catch (NSException *exception) {
                    
                }
                
                
                @try {
                    NSLog(@"PH%@",editPhoneArr[indexPath.row]);
                    
                    [_editcountryButton setTitle:[NSString stringWithFormat:@"    %@",editCountryArr[indexPath.row]] forState:UIControlStateNormal];
                    
                    editcountrystr=editCountryArr[indexPath.row];
                    
                }
                @catch (NSException *exception) {
                    
                }
                
                @try {
                    _editphoneNo.text=[NSString stringWithFormat:@"%@",editPhoneArr[indexPath.row]];
                    
                }
                @catch (NSException *exception) {
                    
                }
                
                
                
                
            }
            @catch (NSException *exception)
            {
                
            }
            
            
            
            
        }
        else
        {

    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    @try {
        [check setObject:imageThumpArray[indexPath.row] forKey:@"CONTCAT_IMG"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"CONTCAT_IMG"];
    }
    
    
    @try {
        [check setObject:FnameArray[indexPath.row] forKey:@"CONTCAT_FNAME"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"CONTCAT_FNAME"];
    }
    
    @try {
        [check setObject:LnameArray[indexPath.row] forKey:@"CONTCAT_LNAME"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"CONTCAT_LNAME"];
    }
    
    
    
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editAddress1Arr[indexPath.row]] forKey:@"CONTCAT_AD1"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"CONTCAT_AD1"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editAddress2Arr[indexPath.row]] forKey:@"CONTCAT_AD2"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"CONTCAT_AD2"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editCityArr[indexPath.row]] forKey:@"CONTCAT_CITY"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"CONTCAT_CITY"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editCountryArr[indexPath.row]] forKey:@"CONTCAT_COUNTRY"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"CONTCAT_COUNTRY"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editZipArr[indexPath.row]] forKey:@"CONTCAT_ZIP"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"CONTCAT_ZIP"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editStateArr[indexPath.row]] forKey:@"CONTCAT_STATE"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"CONTCAT_STATE"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",contactIDArray[indexPath.row]] forKey:@"CONTCAT_ID"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"CONTCAT_ID"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editCountryArr[indexPath.row]] forKey:@"CONTCAT_COUNTRY"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"CONTCAT_COUNTRY"];
    }
    
        @try {
            [check setObject:[NSString stringWithFormat:@"%@",facebookUserID[indexPath.row]] forKey:@"CONTCAT_FBID"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_FBID"];
        }
            
            
            @try {
                [check setObject:[NSString stringWithFormat:@"%@",editEmailArr[indexPath.row]] forKey:@"CONTCAT_EMAIL"];
            }
            @catch (NSException *exception) {
                [check setObject:@"" forKey:@"CONTCAT_EMAIL"];
            }
            
            @try {
                [check setObject:[NSString stringWithFormat:@"%@",editPhoneArr[indexPath.row]] forKey:@"CONTCAT_PHONE"];
            }
            @catch (NSException *exception) {
                [check setObject:@"" forKey:@"CONTCAT_PHONE"];
            }

            
            
        
        /*
        ZSYPopoverListView *listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 200, 160)];
        listView.titleName.text = @"Choose";
        listView.datasource = self;
        listView.delegate = self;
        //    [listView setCancelButtonTitle:@"Cancel" block:^{
        //        NSLog(@"cancel");
        //    }];
        //    [listView setDoneButtonWithTitle:@"OK" block:^{
        //        NSLog(@"Ok%d", [listView indexPathForSelectedRow].row);
        //    }];
        
        [listView.btn1 addTarget:self action:@selector(updateView)
                forControlEvents:UIControlEventTouchDown];
        
        [listView show];
        */
       
        if (IS_IPHONE_5)
        {
            TapViewController *signupView=[[TapViewController alloc]initWithNibName:@"TapViewController" bundle:nil];
            [self presentViewController:signupView animated:NO completion:nil];
        }
        else
        {
            TapViewController *signupView=[[TapViewController alloc]initWithNibName:@"TapViewController4" bundle:nil];
            [self presentViewController:signupView animated:NO completion:nil];
        }
        
    }

              
    }

}
    



-(void)updateView
{
    NSLog(@"ENTER UPDATE VIEW");
}


- (NSInteger)popoverListView:(ZSYPopoverListView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)popoverListView:(ZSYPopoverListView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusablePopoverCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] ;
    }
    
    /*
     if ( self.selectedIndexPath && NSOrderedSame == [self.selectedIndexPath compare:indexPath])
     {
     cell.imageView.image = [UIImage imageNamed:@"fs_main_login_selected.png"];
     }
     else
     {
     cell.imageView.image = [UIImage imageNamed:@"fs_main_login_normal.png"];
     }
     */
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.imageView.image = [UIImage imageNamed:@"fs_main_login_normal.png"];
    
    cell.textLabel.text = [NSString stringWithFormat:@"-dsds---%d------", indexPath.row];
    return cell;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     UITableViewCell *cell = [tableView popoverCellForRowAtIndexPath:indexPath];
     cell.imageView.image = [UIImage imageNamed:@"fs_main_login_normal.png"];
     NSLog(@"deselect:%d", indexPath.row);
     */
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    UITableViewCell *cell = [tableView popoverCellForRowAtIndexPath:indexPath];
    
    if (cell.imageView.image==[UIImage imageNamed:@"fs_main_login_selected.png"])
    {
        cell.imageView.image = [UIImage imageNamed:@"fs_main_login_normal.png"];
    }
    else
    {
        cell.imageView.image = [UIImage imageNamed:@"fs_main_login_selected.png"];
    }
    
    
    NSLog(@"select:%d", indexPath.row);
}







- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    [textField resignFirstResponder];
    
      
    
    return YES;
    
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
   
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}
#pragma mark - LeveyPopListView delegates
- (void)leveyPopListView:(LeveyPopListView *)popListView didSelectedIndex:(NSInteger)anIndex
{
    NSLog(@"%@",[NSString stringWithFormat:@"You have selected %@", countryArray[anIndex]]);
}
- (void)leveyPopListViewDidCancel
{
    NSLog(@"CANCELLED");
}
////// FACEBOOK END //
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (buttonIndex==0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
        }
        
    }
    else  if (buttonIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
    
}


-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    NSData *imagedata = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage" ],1);
    UIImage *image = [[UIImage alloc] initWithData:imagedata];
    
    
    //  image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(240, 240)];
    
    
    float ori_height=image.size.height;
    float ori_width=image.size.width;
    
    NSLog(@"H:%f|W:%f",ori_height,ori_width);
    
    
    float height_ratio1=ori_height/4;
    float width_ratio1=ori_width/4;
    
    NSLog(@"HJ:%f|WJ:%f",height_ratio1,width_ratio1);
    
    
    
    image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(width_ratio1, height_ratio1)];
    
    
    if (editTag==0)
    {
        
        [_imageButton setImage:image forState:UIControlStateNormal];
        [_imageButton setTitle:nil forState:UIControlStateNormal];
    }
    else
    {
        [_editimageButton setImage:image forState:UIControlStateNormal];
        [_editimageButton setTitle:nil forState:UIControlStateNormal];
    }
    
   
   
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postProImgfile",@"http://api.socialnetgateapp.com/v1"]];
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    //[request_post setFile:@"/Users/apple/Desktop/Free-Grammar-Ebook-Level-2.pdf" forKey:@"filename"];
    
    //UIImage * ourImage = [UIImage imageNamed:@"rajini-bday2.jpg"];
    NSData * ourImageData = UIImageJPEGRepresentation(image, 100);
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:ss zzz"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    //////(@"DATE STRING:%@",dateString);
    
    NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
    NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSString *imageName=[NSString stringWithFormat:@"%@.jpg",newString3];
    
    
    
    [request_post addData:ourImageData withFileName:imageName andContentType:@"image/jpeg" forKey:@"file"];
    
    [request_post setTimeOutSeconds:60];
    
    
    [request_post setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request_post responseString];
        
        
        //////(@"RES:%@",responseString);
        
        NSMutableData *results11 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *temp= [results11 valueForKeyPath:@"photos.profile_img_url"];
        
        
        NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
        
        [imgtemp setObject:temp forKey:@"USER_IMAGE"];
        
        
        
        
        if ([temp length]==0) {
            
            NSString *temp= [results11 valueForKeyPath:@"error.text"];
            
            
            NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
            
            [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
            
            [_imageButton setImage:nil forState:UIControlStateNormal];
           
            
        }
        
        // Use when fetching binary data
        // NSData *responseData = [request_post responseData];
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        
        
        
        
    }];
    [request_post startAsynchronous];
    
    
    [picker dismissModalViewControllerAnimated:YES];
    
    
    
    
}
-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissModalViewControllerAnimated:YES];
}


-(IBAction)friends
{
    if (IS_IPHONE_5)
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }

}
-(IBAction)business
{
    if (IS_IPHONE_5)
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }

}
-(IBAction)room
{
    if (IS_IPHONE_5)
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)setttings
{
    if (IS_IPHONE_5)
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)more
{
    if (IS_IPHONE_5)
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}



@end

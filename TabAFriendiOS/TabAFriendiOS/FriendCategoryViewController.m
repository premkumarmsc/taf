//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "FriendCategoryViewController.h"
#import "CatCell.h"
#import "UIImageView+WebCache.h"

@interface FriendCategoryViewController ()

@end

@implementation FriendCategoryViewController
@synthesize collectionView;
CatCell *cell;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *catIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;


NSString *contactIDName;






int editTag;

-(IBAction)addBack
{
    [self friends];

}

-(void)getUpdation
{
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    
    NSString *nameStr=[NSString stringWithFormat:@"%@ %@",[check valueForKey:@"CONTCAT_FNAME"],[check valueForKey:@"CONTCAT_LNAME"]];
    
    _toNameLabel.text=nameStr;
    
    
    NSString *imgStr=[check valueForKey:@"CONTCAT_IMG"];
    
    @try {
        NSString *temp=imgStr;
        [_toImageView setImageWithURL:[NSURL URLWithString:temp]
                        placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }

    
    
   // NSString *addrStr=[NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@",[check valueForKey:@"CONTCAT_AD1"],[check valueForKey:@"CONTCAT_AD2"],[check valueForKey:@"CONTCAT_CITY"],[check valueForKey:@"CONTCAT_STATE"],[check valueForKey:@"CONTCAT_COUNTRY"],[check valueForKey:@"CONTCAT_ZIP"]];
    
    
     NSString *addrStr=[NSString stringWithFormat:@"Email: %@\nMobile: %@",[check valueForKey:@"CONTCAT_EMAIL"],[check valueForKey:@"CONTCAT_PHONE"]];
    
   
    _toAddressLabel.text=addrStr;
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:_addContactView];
    _addContactView.hidden=YES;
    [self.view addSubview:_contactEditView];
    _contactEditView.hidden=YES;
    
    NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
    
    [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
    
    
    [_editButton setTitle:@"Edit" forState:UIControlStateNormal];
    editTag=0;
  
    
    
    
    
    
    imageThumpArray=[[NSMutableArray alloc]init];
    FnameArray=[[NSMutableArray alloc]init];
    LnameArray=[[NSMutableArray alloc]init];
    checkmark_array=[[NSMutableArray alloc]init];
    catIDArray=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    address_entered_arr=[[NSMutableArray alloc]init];
    
  
    
    
    [self getUpdation];
    
    [self getCountry];
   // [self getUserDetails];
    
    
   // checkmarkArray=[[NSMutableArray alloc]init];
    
 
    
    
     [self.collectionView registerNib:[UINib nibWithNibName:@"CatCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)getCountry
{
    imageThumpArray=[[NSMutableArray alloc]init];
    FnameArray=[[NSMutableArray alloc]init];
    LnameArray=[[NSMutableArray alloc]init];
    checkmark_array=[[NSMutableArray alloc]init];
    catIDArray=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    catIDArray=[[NSMutableArray alloc]init];
    
    
         NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/merchant/getCategoryList",CONFIG_BASE_URL]];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setDelegate:self];
       [request startAsynchronous];

    
}
- (void)requestFinished:(ASIHTTPRequest *)request1
{
    
    NSString *responseString = [request1 responseString];
    
    
    
    NSMutableData *results1 = [responseString JSONValue];
    
    NSLog(@"HELLO  erw:%@",responseString);
       
    NSLog(@"RESULTS HELL fg:%@",results1);
    
       
    NSArray *temp= [results1 valueForKeyPath:@"Response.categories"];
    
    
     @try {
    
    for(NSDictionary *value in temp)
    {
        [imageThumpArray  addObject:[value valueForKey:@"category_image"]];
        
        
        [FnameArray  addObject:[value valueForKey:@"category_name"]];
        
        
        [catIDArray  addObject:[value valueForKey:@"pk_id"]];
        
        
        
        
        
    }
     }
    @catch (NSException *exception) {
        
    }
    
    [collectionView reloadData];
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [catIDArray count];
}


- (CatCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
       
    @try {
        cell.cell_title_Label.text=[NSString stringWithFormat:@"%@",FnameArray[indexPath.row]];
    }
    @catch (NSException *exception) {
        
    }
    
    
    
    
    @try {
        NSString *temp=[imageThumpArray objectAtIndex:indexPath.row];
        [cell.image_view setImageWithURL:[NSURL URLWithString:temp]
                        placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }
    return cell;
}


-(IBAction)searchBtn
{
   
    [_searchTextField resignFirstResponder];
    
    if ([_searchTextField.text isEqualToString:@""])
    {
        [self getCountry];
    }
    else
    {
         NSLog(@"SEAR:%@",_searchTextField.text);
        
        [self searchResults];
    }
}

-(void)searchResults
{
   
    
    
    imageThumpArray=[[NSMutableArray alloc]init];
    FnameArray=[[NSMutableArray alloc]init];
    LnameArray=[[NSMutableArray alloc]init];
    checkmark_array=[[NSMutableArray alloc]init];
    catIDArray=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    catIDArray=[[NSMutableArray alloc]init];
   
    
    
    
        NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
        
        NSString *userID=[checkval objectForKey:@"USERID"];
        NSString *fbID=[checkval objectForKey:@"FB_USERID"];
        NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
        
        // userID=@"230";
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/merchant/searchCategory",CONFIG_BASE_URL]];
        
        NSLog(@"HELLO:%@",url);
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:userID forKey:@"userID"];
         [request_post setPostValue:_searchTextField.text forKey:@"searchCriteriaText"];
        
        
        [request_post setTimeOutSeconds:30];
        
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            
            
            
            
          
            
            
            
            NSMutableData *results1 = [responseString JSONValue];
            
            NSLog(@"HELLO:%@",responseString);
            
            NSLog(@"RESULTS HELL:%@",results1);
            
            
            NSArray *temp= [results1 valueForKeyPath:@"Response.category"];
            
            
            
            
            @try {
                
                for(NSDictionary *value in temp)
                {
                    [imageThumpArray  addObject:[value valueForKey:@"category_image"]];
                    
                    
                    [FnameArray  addObject:[value valueForKey:@"category_name"]];
                    
                    
                    [catIDArray  addObject:[value valueForKey:@"pk_id"]];
                    
                    
                    
                    
                    
                }
            }
            @catch (NSException *exception) {
                
            }
            
            [collectionView reloadData];
            
       
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];
        
    }


-(IBAction)editSave
{
    
    
    
  
    
    
    
    //////(@"COUNTRY:%@",countrystr);
    
    if ([_editfirstName.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the first name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        if ([_editlastname.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the last name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            if ([_editaddress1.text isEqualToString:@""])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
            }
            else
            {
                if ([_editcity.text isEqualToString:@""])
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the City" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    [alert show];
                    
                }
                else
                {
                    if ([_editstate.text isEqualToString:@""])
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the state" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [alert show];
                    }
                    else
                    {
                        if ([editcountrystr isEqualToString:@""])
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Select the Country" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            
                            [alert show];
                        }
                        else
                        {
                            if ([_editzipCode.text isEqualToString:@""])
                            {
                                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the zip code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                
                                [alert show];
                            }
                            else
                            {
                                //////(@"SUCCESS");
                                
                                
                                if ([_editemailID.text isEqualToString:@""])
                                {
                                    if ([_editphoneNo.text isEqualToString:@""])
                                    {
                                       //[self addcontectFun];
                                    }
                                    else
                                    {
                                        NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
                                        
                                        // NSString *phoneRegex = @"^[\+(00)][0-9]{6,14}$";
                                        NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
                                        
                                        BOOL phoneValidates = [phoneTest evaluateWithObject:_editphoneNo.text];
                                        
                                        
                                        
                                        if (phoneValidates == YES){
                                            
                                            ////(@"Matched");
                                            
                                            [self editcontectFun];
                                        }
                                        else {
                                            ////(@"Not matched");
                                            
                                            
                                            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter Valid Phone Number" delegate:self
                                                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                            
                                            [loginalert show];
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                else
                                {
                                    //////(@"SUCCESS");
                                    
                                    NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                                    NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                                    NSString *subjectString =_editemailID.text;
                                    
                                    
                                    if((_editemailID.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                                    {
                                        
                                        if ([emailTest evaluateWithObject:subjectString] != YES)
                                            
                                        {
                                            
                                            
                                            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                            
                                            [loginalert show];
                                            
                                            
                                        }
                                        
                                        
                                    }
                                    
                                    else
                                    {
                                        
                                        
                                        
                                        [self editcontectFun];
                                        
                                    }
                                }
                                
                                
                                
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    
    
    
    
}



-(void)editcontectFun
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/updateContact",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
    
    
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://www.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    //////(@"USER IMAGE:%@",userImage);
    
    
    NSString *fname=_editfirstName.text;
    NSString *lname=_editlastname.text;
    NSString *address1text=_editaddress1.text;
    NSString *address2text=_editaddress2.text;
    NSString *citystr=_editcity.text;
    NSString *statestr=_editstate.text;
    NSString *zipCodeStr=_editzipCode.text;
    NSString *emailStr=_editemailID.text;
    
   
    
    
    
    [request_post1234 setPostValue:contactIDSelected forKey:@"contactID"];
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:fbuserid forKey:@"facebook_userid"];
    [request_post1234 setPostValue:fname forKey:@"first_name"];
    [request_post1234 setPostValue:lname forKey:@"last_name"];
    [request_post1234 setPostValue:address1text forKey:@"address1"];
    [request_post1234 setPostValue:address2text forKey:@"address2"];
    [request_post1234 setPostValue:citystr forKey:@"city"];
    [request_post1234 setPostValue:statestr forKey:@"state"];
    [request_post1234 setPostValue:editcountrystr forKey:@"country"];
    [request_post1234 setPostValue:zipCodeStr forKey:@"zipcode"];
    [request_post1234 setPostValue:_editemailID.text forKey:@"email"];
    [request_post1234 setPostValue:_editphoneNo.text forKey:@"phone"];
    [request_post1234 setPostValue:userImage forKey:@"user_image"];
   // [request_post1234 setPostValue:@"iPhone" forKey:@"contact_source"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        [_firstName resignFirstResponder];
        [_lastname resignFirstResponder];
        [_address2 resignFirstResponder];
        [_address1 resignFirstResponder];
        [_city resignFirstResponder];
        [_state resignFirstResponder];
        [_zipCode resignFirstResponder];
        
        _firstName.text=@"";
        _lastname.text=@"";
        _address2.text=@"";
        _address1.text=@"";
        _city.text=@"";
        _state.text=@"";
        _zipCode.text=@"";
        _emailID.text=@"";
        _phoneNo.text=@"";
        
        //[_countrybtn setTitle:@"  United States" forState:UIControlStateNormal];
        _contactAddView.hidden=YES;
        _addContactView.hidden=YES;
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}




-(IBAction)editContry
{
    LeveyPopListView *lplv = [[LeveyPopListView alloc] initWithTitle:@"Select country" options:countryArray handler:^(NSInteger anIndex)
                              {
                                  //////(@"%@",[NSString stringWithFormat:@"You have CLICKED %@", countryArray[anIndex]]);
                                  
                                  [_editcountrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[anIndex]] forState:UIControlStateNormal];
                                  
                                  editcountrystr=countryArray[anIndex];
                                  
                              }];
    //    lplv.delegate = self;
    [lplv showInView:self.view animated:YES];
}

-(IBAction)editImage
{
    [_editfirstName resignFirstResponder];
    [_editlastname resignFirstResponder];
    [_editaddress1 resignFirstResponder];
    [_editaddress2 resignFirstResponder];
    [_editcity resignFirstResponder];
    [_editstate resignFirstResponder];
    [_editzipCode resignFirstResponder];
    
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
  
}



-(void)deletebuttonClicked:(UIButton*)button
{
   
    
    contactIDName=catIDArray[(long int)[button tag]];
    
    firstalert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Do you want to delete this contact?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [firstalert show];
    
    
    
    
    
}


- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView==firstalert)
    {
        if (buttonIndex == 0)
        {
            
            
        }else if (buttonIndex == 1)
        {
            
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/deleteContact",CONFIG_BASE_URL]];
            
            
            __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
            
            
            NSLog(@"Contact ID:%@",contactIDName);
            
            NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
            
            NSString *userID=[checkval objectForKey:@"USERID"];
            
            
            [requestmethod setPostValue:contactIDName forKey:@"contactID"];
             [requestmethod setPostValue:userID forKey:@"userID"];
            
            [requestmethod setTimeOutSeconds:30];
            
            
            [requestmethod setCompletionBlock:^{
                NSString *responseString23 = [requestmethod responseString];
                NSMutableData *results11 = [responseString23 JSONValue];
                
                
                
                
               NSLog(@"RESULTS:%@",results11);
                
                [self viewDidLoad];
                
                
            }];
            [requestmethod setFailedBlock:^{
                NSError *error = [requestmethod error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [requestmethod startAsynchronous];
            
        }
    }
    
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
   
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    [check setObject:catIDArray[indexPath.row] forKey:@"SELECT_CATID"];
    
    if (IS_IPHONE_5)
    {
        FriendSubCategoryViewController *signupView=[[FriendSubCategoryViewController alloc]initWithNibName:@"FriendSubCategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendSubCategoryViewController *signupView=[[FriendSubCategoryViewController alloc]initWithNibName:@"FriendSubCategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    

    
    
    /*
    if (editTag==0)
    {
               
        
        if ([checkmarkArray[indexPath.row]isEqualToString:@"YES"])
        {
            [checkmarkArray replaceObjectAtIndex:indexPath.row withObject:@"NO"];
                       
            
            
        }
        else
        {
            [checkmarkArray replaceObjectAtIndex:indexPath.row withObject:@"YES"];
            
                      
            
            
            
        }
        [collectionView reloadData];
    }
     */
    
    
    
    
    
        
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    [textField resignFirstResponder];
    
      
    
    return YES;
    
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
   
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}
#pragma mark - LeveyPopListView delegates
- (void)leveyPopListView:(LeveyPopListView *)popListView didSelectedIndex:(NSInteger)anIndex
{
    NSLog(@"%@",[NSString stringWithFormat:@"You have selected %@", countryArray[anIndex]]);
}
- (void)leveyPopListViewDidCancel
{
    NSLog(@"CANCELLED");
}
////// FACEBOOK END //
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (buttonIndex==0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
        }
        
    }
    else  if (buttonIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
    
}


-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    NSData *imagedata = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage" ],1);
    UIImage *image = [[UIImage alloc] initWithData:imagedata];
    
    
    //  image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(240, 240)];
    
    
    float ori_height=image.size.height;
    float ori_width=image.size.width;
    
    NSLog(@"H:%f|W:%f",ori_height,ori_width);
    
    
    float height_ratio1=ori_height/4;
    float width_ratio1=ori_width/4;
    
    NSLog(@"HJ:%f|WJ:%f",height_ratio1,width_ratio1);
    
    
    
    image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(width_ratio1, height_ratio1)];
    
    
    if (editTag==0)
    {
        
        [_imageButton setImage:image forState:UIControlStateNormal];
        [_imageButton setTitle:nil forState:UIControlStateNormal];
    }
    else
    {
        [_editimageButton setImage:image forState:UIControlStateNormal];
        [_editimageButton setTitle:nil forState:UIControlStateNormal];
    }
    
   
   
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postProImgfile",@"http://api.socialnetgateapp.com/v1"]];
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    //[request_post setFile:@"/Users/apple/Desktop/Free-Grammar-Ebook-Level-2.pdf" forKey:@"filename"];
    
    //UIImage * ourImage = [UIImage imageNamed:@"rajini-bday2.jpg"];
    NSData * ourImageData = UIImageJPEGRepresentation(image, 100);
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:ss zzz"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    //////(@"DATE STRING:%@",dateString);
    
    NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
    NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSString *imageName=[NSString stringWithFormat:@"%@.jpg",newString3];
    
    
    
    [request_post addData:ourImageData withFileName:imageName andContentType:@"image/jpeg" forKey:@"file"];
    
    [request_post setTimeOutSeconds:60];
    
    
    [request_post setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request_post responseString];
        
        
        //////(@"RES:%@",responseString);
        
        NSMutableData *results11 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *temp= [results11 valueForKeyPath:@"photos.profile_img_url"];
        
        
        NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
        
        [imgtemp setObject:temp forKey:@"USER_IMAGE"];
        
        
        
        
        if ([temp length]==0) {
            
            NSString *temp= [results11 valueForKeyPath:@"error.text"];
            
            
            NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
            
            [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
            
            [_imageButton setImage:nil forState:UIControlStateNormal];
           
            
        }
        
        // Use when fetching binary data
        // NSData *responseData = [request_post responseData];
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        
        
        
        
    }];
    [request_post startAsynchronous];
    
    
    [picker dismissModalViewControllerAnimated:YES];
    
    
    
    
}
-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissModalViewControllerAnimated:YES];
}


-(IBAction)friends
{
    if (IS_IPHONE_5)
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)business
{
    if (IS_IPHONE_5)
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)room
{
    if (IS_IPHONE_5)
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)setttings
{
    if (IS_IPHONE_5)
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)more
{
    if (IS_IPHONE_5)
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}




@end

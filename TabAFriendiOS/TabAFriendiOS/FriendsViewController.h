//
//  ViewController.h
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate,UIActionSheetDelegate>
{
      
}
@property(retain,nonatomic)IBOutlet UICollectionView *collectionView;
@property(retain,nonatomic)IBOutlet UIView *addContactView;



@property (weak, nonatomic) IBOutlet UITextField *editaddress1;
@property (weak, nonatomic) IBOutlet UITextField *editaddress2;
@property (weak, nonatomic) IBOutlet UITextField *editzipCode;
@property (weak, nonatomic) IBOutlet UITextField *editfirstName;
@property (weak, nonatomic) IBOutlet UITextField *editlastname;
@property (weak, nonatomic) IBOutlet UITextField *editcity;
@property (weak, nonatomic) IBOutlet UITextField *editstate;
@property (weak, nonatomic) IBOutlet UIButton *editcountrybtn;
@property (weak, nonatomic) IBOutlet UIView  *contactEditView;
@property (weak, nonatomic) IBOutlet UITextField *editemailID;
@property (weak, nonatomic) IBOutlet UITextField *editphoneNo;

@property (weak, nonatomic) IBOutlet UIButton *editimageButton;
@property (weak, nonatomic) IBOutlet UIButton *editcountryButton;




@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UITextField *address1;
@property (weak, nonatomic) IBOutlet UITextField *address2;
@property (weak, nonatomic) IBOutlet UITextField *zipCode;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastname;
@property (weak, nonatomic) IBOutlet UITextField *city;
@property (weak, nonatomic) IBOutlet UITextField *state;
@property (weak, nonatomic) IBOutlet UIButton *countrybtn;
@property (weak, nonatomic) IBOutlet UIView  *contactAddView;
@property (weak, nonatomic) IBOutlet UITextField *emailID;
@property (weak, nonatomic) IBOutlet UITextField *phoneNo;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;

-(IBAction)searchBtn;
-(IBAction)addContact;
-(IBAction)addBack;
-(IBAction)addSave;
-(IBAction)editSave;
-(IBAction)addContry;
-(IBAction)editContry;
-(IBAction)addImage;
-(IBAction)editImage;
-(IBAction)editButtonClick;

-(IBAction)friends;
-(IBAction)business;
-(IBAction)room;
-(IBAction)setttings;
-(IBAction)more;






@end

//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "FriendsViewController.h"
#import "receiptentCell.h"
#import "UIImageView+WebCache.h"

@interface FriendsViewController ()

@end

@implementation FriendsViewController
@synthesize collectionView;
receiptentCell *cell;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *contactIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;
UIAlertView *contactAlert;

NSString *contactIDName;

NSMutableArray *facebookUserID;

int from_count;
int to_count;
int total_count;

int editId;

int editTag;


int checkEditedTag;

NSString *check_search;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    checkEditedTag=100000;
    
    
    from_count=0;
    to_count=30;
    
    
    NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
    
    
    NSString *addressStatus=[checkSta objectForKey:@"FIRSTTIME"];
    
    
    NSString *deviceStr = [UIDevice currentDevice].model;
    //////(@"device:%@",deviceStr);
    
    
    if (![deviceStr isEqualToString:@"iPod touch"])
    {
        
        /*
         if ([addressStatus isEqualToString:@"YES"])
         {
         
         contactAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"\"Tap A Friend\" Would Like to  Access Your Contacts" delegate:self cancelButtonTitle:@"Don't Allow" otherButtonTitles:@"Allow", nil];
         
         [contactAlert show];
         
         
         
         
         
         }
         */
        if ([addressStatus isEqualToString:@"YES"])
        {

            ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
            
            if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
                ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
                    // First time access has been granted, add the contact
                    
                    
                    NSLog(@"Access Granded:");
                    
                     [self addContactFuntion];
                    
                });
            }
            else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
                // The user has previously given access, add the contact
                
                NSLog(@"Access Granded Previously:");
                
                 [self addContactFuntion];
                
            }
            else
            {
                // The user has previously denied access
                // Send an alert telling user to change privacy setting in settings app
                
                NSLog(@"Access Not Granded:");
            }
            

            
            
        
        }
        
    }

    
    
    
    
    [self.view addSubview:_addContactView];
    _addContactView.hidden=YES;
    [self.view addSubview:_contactEditView];
    _contactEditView.hidden=YES;
    
    NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
    
    [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
    
    
    [_editButton setTitle:@"EDIT" forState:UIControlStateNormal];
    editTag=0;
  
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    
    NSLog(@"LOGGED USER:%@",userID);
    
    
    
    
    
     [self  getDatas:from_count to:to_count];
    
    [self getFriendsList];
    [self getUserDetails];
    
    
    checkmarkArray=[[NSMutableArray alloc]init];
    
 
    
    
     [self.collectionView registerNib:[UINib nibWithNibName:@"receiptentCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
	// Do any additional setup after loading the view, typically from a nib.
}






-(void)getDatas:(int)get_from_count to:(int)get_to_count
{
    
    
    NSLog(@"FROM:%d",get_from_count);
    NSLog(@"get_to_count:%d",get_to_count);
    
    /*
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/retrieveUserContacts",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *access=[check objectForKey:@"ACCESS_TOKEN"];
    
    
    ////(@"referesh_count_str:%d",get_from_count);
    ////(@"referesh_count_str:%d",get_to_count);
    
    
    
    NSString *referesh_count_from_str=[NSString stringWithFormat:@"%d",get_from_count];
    NSString *referesh_count_to_str=[NSString stringWithFormat:@"%d",get_to_count];
    
    NSString *cardID=[check objectForKey:@"CARD_ID"];
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:@"30" forKey:@"item_count"];
    [request_post1234 setPostValue:referesh_count_from_str forKey:@"current_count"];
    [request_post1234 setPostValue:[Config get_token] forKey:@"apikey"];
    [request_post1234 setPostValue:cardID forKey:@"cardID"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        // ////(@"RESULTS LOGIN:%@",results11);
        
       
        
        
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
     */
}

-(void)viewWillAppear:(BOOL)animated
{
    
    /*
    [self.collectionView.infiniteScrollingView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [self.collectionView addInfiniteScrollingWithActionHandler:^{
        ////(@"Infinitescroll");
        
        //referesh_count=referesh_count+1;
        // [self getDatas:referesh_count];
        
        
        from_count=from_count+30;
        
        [self  getDatas:from_count to:to_count];
        
        
        @try {
            int64_t delayInSeconds = 10.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //[self.dataSource addObjectsFromArray:tmp];
                [self.collectionView performBatchUpdates:^{
                    //[self.collectionView insertItemsAtIndexPaths:(NSArray*)indexPaths];
                    
                    ////(@"UPDAtES");
                    
                } completion:nil];
                [self.collectionView.infiniteScrollingView stopAnimating];
            });
        }
        @catch (NSException *exception) {
            
        }
        
        
        
        
        
    }];
    
    [super viewWillAppear:animated];
    /*
     [collectionView addPullToRefreshWithActionHandler:^{
     
     
     referesh_count=referesh_count+1;
     [self getDatas:referesh_count];
     
     } position:SVPullToRefreshPositionBottom];
     */
    
}


- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView==contactAlert)
    {
       
            
            
            
            
        }
    
    if (alertView==firstalert)
    {
        if (buttonIndex == 0)
        {
            
            
        }else if (buttonIndex == 1)
        {
            
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/deleteContact",CONFIG_BASE_URL]];
            
            
            __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
            
            
            NSLog(@"Contact ID:%@",contactIDName);
            
            NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
            
            NSString *userID=[checkval objectForKey:@"USERID"];
            
            
            [requestmethod setPostValue:contactIDName forKey:@"contactID"];
            [requestmethod setPostValue:userID forKey:@"userID"];
            
            [requestmethod setTimeOutSeconds:30];
            
            
            [requestmethod setCompletionBlock:^{
                NSString *responseString23 = [requestmethod responseString];
                NSMutableData *results11 = [responseString23 JSONValue];
                
                
                
                
                NSLog(@"RESULTS:%@",results11);
                
                [self viewDidLoad];
                
                
            }];
            [requestmethod setFailedBlock:^{
                NSError *error = [requestmethod error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [requestmethod startAsynchronous];
            
        }
    }
    
}
    
    
-(void)addContactFuntion
{
            
        
        
               
        
        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        CFArrayRef addressBookData = ABAddressBookCopyArrayOfAllPeople(addressBook);
        
        CFIndex count = CFArrayGetCount(addressBookData);
        
        NSMutableArray *firstNameArray = [NSMutableArray new];
        NSMutableArray *lastNameArray = [NSMutableArray new];
        NSMutableArray *emailArray = [NSMutableArray new];
        NSMutableArray *mobileArray = [NSMutableArray new];
        NSMutableArray *streetArr = [NSMutableArray new];
        NSMutableArray *cityArr = [NSMutableArray new];
        NSMutableArray *stateArr = [NSMutableArray new];
        NSMutableArray *zipArr = [NSMutableArray new];
        NSMutableArray *countryArr = [NSMutableArray new];
        
        
        for (CFIndex idx = 0; idx < count; idx++)
        {
            ABRecordRef person = CFArrayGetValueAtIndex(addressBookData, idx);
            
            ABMultiValueRef st = ABRecordCopyValue(person, kABPersonAddressProperty);
            
            
            
            
            
            NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
            
            // NSString *emailStr = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonEmailProperty);
            
            
            NSString *mustPhone;
            ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
            
            
            
            
            NSString *phoneno = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(phone, 0);
            
            //////(@"person.homeEmail = %@ ", phoneno);
            @try {
                [mobileArray addObject:phoneno];
                
                mustPhone=phoneno;
            }
            @catch (NSException *exception)
            {
                NSString *Phone1 = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(phone, 1);
                
                @try
                {
                    
                    [mobileArray addObject:Phone1];
                    mustPhone=Phone1;
                }
                @catch (NSException *exception)
                {
                    [mobileArray addObject:@""];
                    mustPhone=@"";
                }
                
            }
            /*
             if ([mustEmail isEqualToString:@""]&&[mustPhone isEqualToString:@""])
             {
             
             [mobileArray removeObjectAtIndex: [mobileArray count]-1];
             [emailArray removeObjectAtIndex: [emailArray count]-1];
             }*/
            if ([mustPhone isEqualToString:@""])
            {
                
                [mobileArray removeObjectAtIndex: [mobileArray count]-1];
                
            }
            else
            {
                
                
                
                NSString *mustEmail;
                
                
                
                ABMultiValueRef emails = ABRecordCopyValue(person, kABPersonEmailProperty);
                
                
                if (emails)
                {
                    
                    
                    NSString *email = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emails, 0);
                    
                    //////(@"person.homeEmail = %@ ", email);
                    @try {
                        [emailArray addObject:email];
                        
                        mustEmail=email;
                    }
                    @catch (NSException *exception)
                    {
                        NSString *email1 = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emails, 1);
                        
                        @try
                        {
                            
                            [emailArray addObject:email1];
                            mustEmail=email1;
                            
                        }
                        @catch (NSException *exception)
                        {
                            [emailArray addObject:@""];
                            mustEmail=@"";
                        }
                        
                    }
                    
                    
                    
                    
                }
                
                
                
                
                
                
                
                
                if (firstName)
                {
                    
                    
                    [firstNameArray addObject:firstName];
                    
                }
                else
                {
                    [firstNameArray addObject:@""];
                }
                
                if (lastName)
                {
                    
                    [lastNameArray addObject:lastName];
                }
                else
                {
                    
                    if ([firstName isEqualToString:@""]||[firstName length]==0)
                    {
                        [lastNameArray addObject:@"Unknown"];
                    }
                    else
                    {
                        
                        [lastNameArray addObject:@""];
                    }
                }
                
                
                
                if (ABMultiValueGetCount(st) > 0)
                {
                    
                    //////(@"ENTER");
                    
                    CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(st, 0);
                    NSString *dictString = CFDictionaryGetValue(dict, kABPersonAddressStreetKey);
                    NSString *cityString = CFDictionaryGetValue(dict, kABPersonAddressCityKey);
                    NSString *stateString = CFDictionaryGetValue(dict, kABPersonAddressStateKey);
                    NSString *zipString = CFDictionaryGetValue(dict, kABPersonAddressZIPKey);
                    NSString *countryString = CFDictionaryGetValue(dict, kABPersonAddressCountryKey);
                    @try
                    {
                        [streetArr addObject:dictString];
                    }
                    @catch (NSException *exception)
                    {
                        [streetArr addObject:@""];
                    }
                    
                    @try
                    {
                        [cityArr addObject:cityString];
                    }
                    @catch (NSException *exception)
                    {
                        [cityArr addObject:@""];
                    }
                    
                    
                    @try
                    {
                        [stateArr addObject:stateString];
                    }
                    @catch (NSException *exception)
                    {
                        [stateArr addObject:@""];
                    }
                    
                    @try
                    {
                        [countryArr addObject:countryString];
                    }
                    @catch (NSException *exception)
                    {
                        [countryArr addObject:@""];
                    }
                    
                    @try
                    {
                        [zipArr addObject:zipString];
                    }
                    @catch (NSException *exception)
                    {
                        [zipArr addObject:@""];
                    }
                    
                    
                    
                }
                else
                {
                    [streetArr addObject:@" "];
                    [zipArr addObject:@" "];
                    [countryArr addObject:@"USA"];
                    [stateArr addObject:@" "];
                    [cityArr addObject:@" "];
                }
                
            }
            
            
        }
        CFRelease(addressBook);
        CFRelease(addressBookData);
        
        
        
        NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
        
        [checkSta setObject:@"NO" forKey:@"FIRSTTIME"];
        
        NSLog(@"Contacts Array:%@,%@,%@",firstNameArray,lastNameArray,streetArr);
        
        NSLog(@"Contacts Array:%@,%@,%@,%@,%@,%@,%@",cityArr,stateArr,countryArr,zipArr,mobileArray,emailArray,stateArr);
        
        
        NSMutableString* firstNameStr = [NSMutableString stringWithCapacity:2000];
        NSMutableString* lastNameStr = [NSMutableString stringWithCapacity:2000];
        NSMutableString* mobileStr = [NSMutableString stringWithCapacity:2000];
        NSMutableString* emailStr = [NSMutableString stringWithCapacity:2000];
        NSMutableString* streetStr = [NSMutableString stringWithCapacity:2000];
        NSMutableString* cityStr = [NSMutableString stringWithCapacity:2000];
        NSMutableString* countryStr = [NSMutableString stringWithCapacity:2000];
        NSMutableString* zipStr = [NSMutableString stringWithCapacity:2000];
        NSMutableString* stateStr = [NSMutableString stringWithCapacity:2000];
        
        //(@"COUNTEY:%@",countryArr);
        
        for (int i=0; i<[firstNameArray count]; i++)
        {
            [firstNameStr appendString:[NSString stringWithFormat:@"%@|",firstNameArray[i]]];
            [lastNameStr appendString:[NSString stringWithFormat:@"%@|",lastNameArray[i]]];
            [mobileStr appendString:[NSString stringWithFormat:@"%@|",mobileArray[i]]];
            [emailStr appendString:[NSString stringWithFormat:@"%@|",emailArray[i]]];
            [streetStr appendString:[NSString stringWithFormat:@"%@|",streetArr[i]]];
            [cityStr appendString:[NSString stringWithFormat:@"%@|",cityArr[i]]];
            [countryStr appendString:[NSString stringWithFormat:@"%@|",countryArr[i]]];
            [zipStr appendString:[NSString stringWithFormat:@"%@|",zipArr[i]]];
            [stateStr appendString:[NSString stringWithFormat:@"%@|",stateArr[i]]];
        }
        
        
        @try {
            [firstNameStr deleteCharactersInRange:NSMakeRange([firstNameStr length] - 1, 1)];
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            
            [lastNameStr deleteCharactersInRange:NSMakeRange([lastNameStr length] - 1, 1)];
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            [mobileStr deleteCharactersInRange:NSMakeRange([mobileStr length] - 1, 1)];
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            [emailStr deleteCharactersInRange:NSMakeRange([emailStr length] - 1, 1)];
        }
        @catch (NSException *exception) {
            
        }
        @try {
            [streetStr deleteCharactersInRange:NSMakeRange([streetStr length] - 1, 1)];
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            
        }
        @catch (NSException *exception) {
            [cityStr deleteCharactersInRange:NSMakeRange([cityStr length] - 1, 1)];
        }
        @try {
            
        }
        @catch (NSException *exception)
        {
            
            [countryStr deleteCharactersInRange:NSMakeRange([countryStr length] - 1, 1)];
        }
        
        
        @try {
            [zipStr deleteCharactersInRange:NSMakeRange([zipStr length] - 1, 1)];
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            
            [stateStr deleteCharactersInRange:NSMakeRange([stateStr length] - 1, 1)];
        }
        @catch (NSException *exception)
        {
            
        }
        
        
        
        
        NSLog(@"firstNameStr:  %@",firstNameStr);
        NSLog(@"lastNameStr:  %@",lastNameStr);
        NSLog(@"mobileStr: %@",mobileStr);
        NSLog(@"emailStr: %@",emailStr);
        NSLog(@"streetStr: %@",streetStr);
        NSLog(@"cityStr: %@",cityStr);
        NSLog(@"countryStr: %@",countryStr);
        NSLog(@"zipStr: %@",zipStr);
        
        
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/mobileContactsImport",CONFIG_BASE_URL]];
        
        
        
        __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
        
        
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        NSString *user=[check objectForKey:@"USERID"];
         [check setObject:@"USER" forKey:@"USER_TYPE"];
        
        
        [requestmethod setPostValue:user forKey:@"userID"];
        [requestmethod setPostValue:firstNameStr forKey:@"firstNameStr"];
        [requestmethod setPostValue:lastNameStr forKey:@"lastNameStr"];
        [requestmethod setPostValue:mobileStr forKey:@"mobileStr"];
        [requestmethod setPostValue:emailStr forKey:@"emailStr"];
        [requestmethod setPostValue:streetStr forKey:@"streetStr"];
        [requestmethod setPostValue:cityStr forKey:@"cityStr"];
        [requestmethod setPostValue:countryStr forKey:@"countryStr"];
        [requestmethod setPostValue:stateStr forKey:@"stateStr"];
        [requestmethod setPostValue:zipStr forKey:@"zipStr"];
        
        [requestmethod setTimeOutSeconds:30];
        
        
        [requestmethod setCompletionBlock:^{
            NSString *responseString23 = [requestmethod responseString];
            NSMutableData *results11 = [responseString23 JSONValue];
            
            NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
            
            [checkSta setObject:@"NO" forKey:@"FIRSTTIME"];
            
            NSLog(@"TEMPARY:%@",responseString23);
            
            [self getFriendsList];
            
            
            
        }];
        [requestmethod setFailedBlock:^{
            NSError *error = [requestmethod error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [requestmethod startAsynchronous];
        
        
    }




-(void)getFriendsList
{
    
    check_search=@"NO";
    
    
    imageThumpArray=[[NSMutableArray alloc]init];
    FnameArray=[[NSMutableArray alloc]init];
    LnameArray=[[NSMutableArray alloc]init];
    checkmark_array=[[NSMutableArray alloc]init];
    contactIDArray=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    address_entered_arr=[[NSMutableArray alloc]init];
    facebookUserID=[[NSMutableArray alloc]init];
    
    editAddress1Arr=[[NSMutableArray alloc]init];
    editAddress2Arr=[[NSMutableArray alloc]init];
    editCityArr=[[NSMutableArray alloc]init];
    editCountryArr=[[NSMutableArray alloc]init];
    editEmailArr=[[NSMutableArray alloc]init];
    editPhoneArr=[[NSMutableArray alloc]init];
    editStateArr=[[NSMutableArray alloc]init];
    editZipArr=[[NSMutableArray alloc]init];
    
    contactSourceArray=[[NSMutableArray alloc]init];
    
    
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
   // //[checkval setObject:@"227" forKey:@"USERID"];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    NSString *fbID=[checkval objectForKey:@"FB_USERID"];
    NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
    
     //userID=@"227";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/retrieveUserContact",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
    
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        NSArray *temp= [results1 valueForKeyPath:@"Response.contacts"];
        
      

        
        for(NSDictionary *value in temp)
        {
            [imageThumpArray  addObject:[value valueForKey:@"user_image"]];
          
            
            [FnameArray  addObject:[value valueForKey:@"first_name"]];
            [LnameArray  addObject:[value valueForKey:@"last_name"]];
           
            [contactIDArray  addObject:[value valueForKey:@"pk_id"]];
            
          //  [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
            
             [facebookUserID  addObject:[value valueForKey:@"fk_facebook_user_id"]];
            
            [editZipArr  addObject:[value valueForKey:@"zipcode"]];
            [editStateArr  addObject:[value valueForKey:@"state"]];
            [editPhoneArr  addObject:[value valueForKey:@"phone"]];
            [editEmailArr  addObject:[value valueForKey:@"email"]];
            
            [editCountryArr  addObject:[value valueForKey:@"country"]];
            [editCityArr  addObject:[value valueForKey:@"city"]];
            [editAddress2Arr  addObject:[value valueForKey:@"address2"]];
            [editAddress1Arr  addObject:[value valueForKey:@"address1"]];
            
            [contactSourceArray  addObject:[value valueForKey:@"contact_source"]];
            
           
            
        }
        
        
        NSLog(@"FIRST NAME:%@",FnameArray);
        
        for (int i=0; i<[FnameArray count]; i++)
        {
            [checkmarkArray addObject:[NSString stringWithFormat:@"NO"]];
        }
        
        [collectionView reloadData];
        
        
        if ([FnameArray count]==0) {
            UIAlertView *fn=[[UIAlertView alloc]initWithTitle:@"Information" message:@"No Contacts found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [fn show];
        }

        
       if(checkEditedTag!=100000)
     {
         NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
         
         @try {
             [check setObject:imageThumpArray[editId] forKey:@"CONTCAT_IMG"];
         }
         @catch (NSException *exception) {
             [check setObject:@"" forKey:@"CONTCAT_IMG"];
         }
         
         
         @try {
             [check setObject:FnameArray[editId] forKey:@"CONTCAT_FNAME"];
         }
         @catch (NSException *exception) {
             [check setObject:@"" forKey:@"CONTCAT_FNAME"];
         }
         
         @try {
             [check setObject:LnameArray[editId] forKey:@"CONTCAT_LNAME"];
         }
         @catch (NSException *exception) {
             [check setObject:@"" forKey:@"CONTCAT_LNAME"];
         }
         
         @try {
             [check setObject:[NSString stringWithFormat:@"%@",editEmailArr[editId]] forKey:@"CONTCAT_EMAIL"];
         }
         @catch (NSException *exception) {
             [check setObject:@"" forKey:@"CONTCAT_EMAIL"];
         }
         
         @try {
             [check setObject:[NSString stringWithFormat:@"%@",editPhoneArr[editId]] forKey:@"CONTCAT_PHONE"];
         }
         @catch (NSException *exception) {
             [check setObject:@"" forKey:@"CONTCAT_PHONE"];
         }
         
         @try {
             [check setObject:[NSString stringWithFormat:@"%@",contactIDArray[editId]] forKey:@"CONTCAT_ID"];
         }
         @catch (NSException *exception) {
             [check setObject:@"" forKey:@"CONTCAT_ID"];
         }
         
         
         
         
         if (IS_IPHONE_5)
         {
             FriendCategoryViewController *signupView=[[FriendCategoryViewController alloc]initWithNibName:@"FriendCategoryViewController" bundle:nil];
             [self presentViewController:signupView animated:NO completion:nil];
         }
         else
         {
             FriendCategoryViewController *signupView=[[FriendCategoryViewController alloc]initWithNibName:@"FriendCategoryViewController4" bundle:nil];
             [self presentViewController:signupView animated:NO completion:nil];
         }

     }
     
     
     
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    
}



-(IBAction)editButtonClick
{
    if (editTag==0)
    {
        [_editButton setTitle:@"DONE" forState:UIControlStateNormal];
        editTag=1;
    }
   else
   {
       [_editButton setTitle:@"EDIT" forState:UIControlStateNormal];
       editTag=0;
   }
    
    [collectionView reloadData];
}
-(IBAction)addContact
{
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    [check setObject:@"NULL" forKey:@"USER_IMAGE"];
   
    
   _addContactView.hidden=NO;
    [self getCountry];
}
-(void)getCountry
{
    countryArray=[[NSMutableArray alloc]init];
    
    
         NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/general/getCountryList",CONFIG_BASE_URL]];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setDelegate:self];
       [request startAsynchronous];

    
}
- (void)requestFinished:(ASIHTTPRequest *)request1
{
    
    NSString *responseString = [request1 responseString];
    
    
    
    NSMutableData *results1 = [responseString JSONValue];
    
    NSLog(@"HELLO:%@",responseString);
    
    
    NSArray *temp= [results1 valueForKeyPath:@"Response.countries"];
    
    
    
    
    for(NSDictionary *value in temp)
    {
        [countryArray  addObject:[value valueForKey:@"country_name"]];
        
        
    }
    
    NSLog(@"TEMPARY:%@",countryArray);
    
    NSUserDefaults *valueAdd=[NSUserDefaults standardUserDefaults];
    [valueAdd setObject:countryArray forKey:@"COUNTRY_ARRAY"];
    
    
    [_countrybtn setTitle:[NSString stringWithFormat:@"    %@",countryArray[0]] forState:UIControlStateNormal];
    
    
    
}

-(IBAction)addImage
{
    [_firstName resignFirstResponder];
    [_lastname resignFirstResponder];
    [_address2 resignFirstResponder];
    [_address1 resignFirstResponder];
    [_city resignFirstResponder];
    [_state resignFirstResponder];
    [_zipCode resignFirstResponder];
    
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
}

-(IBAction)addBack
{
    
    _editaddress1.text=@"";
    _editaddress2.text=@"";
    _editcity.text=@"";
    _editemailID.text=@"";
    _editfirstName.text=@"";
    _editstate.text=@"";
    _editphoneNo.text=@"";
    
    _addContactView.hidden=YES;
    _contactEditView.hidden=YES;
}
-(IBAction)addSave
{
   
      countrystr=@"United States";
    
    NSString *fname=_firstName.text;
    NSString *lname=_lastname.text;
    NSString *address1text=_address1.text;
    NSString *address2text=_address2.text;
    NSString *citystr=_city.text;
    NSString *statestr=_state.text;
    NSString *zipCodeStr=_zipCode.text;
    
    NSString *emailStr=_emailID.text;
    NSString *phoneStr=_phoneNo.text;
    
    
    
    //////(@"COUNTRY:%@",countrystr);
    
    if ([fname isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the first name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        if ([lname isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the last name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            if ([emailStr isEqualToString:@""])
            {
                
                UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [loginalert show];
                
                /*
                if ([phoneStr isEqualToString:@""])
                {
                    [self addcontectFun];
                }
                else
                {
                    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
                    
                    // NSString *phoneRegex = @"^[\+(00)][0-9]{6,14}$";
                    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
                    
                    BOOL phoneValidates = [phoneTest evaluateWithObject:phoneStr];
                    
                    
                    
                    if (phoneValidates == YES){
                        
                        ////(@"Matched");
                        
                        [self addcontectFun];
                    }
                    else {
                        ////(@"Not matched");
                        
                        
                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter Valid Phone Number" delegate:self
                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [loginalert show];
                        
                    }
                    
                }
                 */
                
                
            }
            else
            {
                //////(@"SUCCESS");
                
                NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                NSString *subjectString =emailStr;
                
                
                if((emailStr.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                {
                    
                    if ([emailTest evaluateWithObject:subjectString] != YES)
                        
                    {
                        
                        
                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [loginalert show];
                        
                        
                    }
                    
                    
                }
                
                else
                {
                    
                    
                    
                    [self addcontectFun];
                    
                }
            }
        }
    }
    
    
    

    
}

-(void)addcontectFun
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/createContact",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
   
    
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://www.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    //////(@"USER IMAGE:%@",userImage);
    
    
    NSString *fname=_firstName.text;
    NSString *lname=_lastname.text;
    NSString *address1text=_address1.text;
    NSString *address2text=_address2.text;
    NSString *citystr=_city.text;
    NSString *statestr=_state.text;
    NSString *zipCodeStr=_zipCode.text;
    NSString *emailStr=_emailID.text;
    
    NSUserDefaults *img=[NSUserDefaults standardUserDefaults];
    
    NSString *urlstr=[img objectForKey:@""];
    
    
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:fbuserid forKey:@"facebook_userid"];
    [request_post1234 setPostValue:fname forKey:@"first_name"];
    [request_post1234 setPostValue:lname forKey:@"last_name"];
    [request_post1234 setPostValue:address1text forKey:@"address1"];
    [request_post1234 setPostValue:address2text forKey:@"address2"];
    [request_post1234 setPostValue:citystr forKey:@"city"];
    [request_post1234 setPostValue:statestr forKey:@"state"];
    [request_post1234 setPostValue:countrystr forKey:@"country"];
    [request_post1234 setPostValue:zipCodeStr forKey:@"zipcode"];
    [request_post1234 setPostValue:_emailID.text forKey:@"email"];
    [request_post1234 setPostValue:_phoneNo.text forKey:@"phone"];
    [request_post1234 setPostValue:userImage forKey:@"user_image"];
   [request_post1234 setPostValue:@"iPhone" forKey:@"contact_source"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        [_firstName resignFirstResponder];
        [_lastname resignFirstResponder];
        [_address2 resignFirstResponder];
        [_address1 resignFirstResponder];
        [_city resignFirstResponder];
        [_state resignFirstResponder];
        [_zipCode resignFirstResponder];
        
        _firstName.text=@"";
        _lastname.text=@"";
        _address2.text=@"";
        _address1.text=@"";
        _city.text=@"";
        _state.text=@"";
        _zipCode.text=@"";
        _emailID.text=@"";
        _phoneNo.text=@"";
       
        //[_countrybtn setTitle:@"  United States" forState:UIControlStateNormal];
        _contactAddView.hidden=YES;
        _addContactView.hidden=YES;
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
     
}

-(IBAction)addContry
{
    LeveyPopListView *lplv = [[LeveyPopListView alloc] initWithTitle:@"Select country" options:countryArray handler:^(NSInteger anIndex)
                              {
                                  //////(@"%@",[NSString stringWithFormat:@"You have CLICKED %@", countryArray[anIndex]]);
                                  
                                  [_countrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[anIndex]] forState:UIControlStateNormal];
                                  
                                  _countrybtn=countryArray[anIndex];
                                  
                              }];
    //    lplv.delegate = self;
    [lplv showInView:self.view animated:YES];
}

-(void)getUserDetails
{
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    NSString *fbID=[checkval objectForKey:@"FB_USERID"];
    NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
    
   // userID=@"230";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserDetails",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
    
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        NSString *pk_id=[results1 valueForKeyPath:@"Response.user.pk_id"];
        NSString *fk_facebook_user_id=[results1 valueForKeyPath:@"Response.user.fk_facebook_user_id"];
        NSString *fk_merchant_package_id=[results1 valueForKeyPath:@"Response.user.fk_merchant_package_id"];
         NSString *first_name=[results1 valueForKeyPath:@"Response.user.first_name"];
          NSString *last_name=[results1 valueForKeyPath:@"Response.user.last_name"];
          NSString *email=[results1 valueForKeyPath:@"Response.user.email"];
          NSString *password=[results1 valueForKeyPath:@"Response.user.password"];
          NSString *signup_type=[results1 valueForKeyPath:@"Response.user.signup_type"];
          NSString *facebook_access_token=[results1 valueForKeyPath:@"Response.user.facebook_access_token"];
          NSString *image=[results1 valueForKeyPath:@"Response.user.image"];
          NSString *address1=[results1 valueForKeyPath:@"Response.user.address1"];
          NSString *address2=[results1 valueForKeyPath:@"Response.user.address2"];
          NSString *city=[results1 valueForKeyPath:@"Response.user.city"];
          NSString *state=[results1 valueForKeyPath:@"Response.user.state"];
          NSString *zipcode=[results1 valueForKeyPath:@"Response.user.zipcode"];
          NSString *country=[results1 valueForKeyPath:@"Response.user.country"];
          NSString *phone=[results1 valueForKeyPath:@"Response.user.phone"];
          NSString *user_security_pin=[results1 valueForKeyPath:@"Response.user.user_security_pin"];
          NSString *user_type=[results1 valueForKeyPath:@"Response.user.user_type"];
          NSString *location_latitude=[results1 valueForKeyPath:@"Response.user.location_latitude"];
          NSString *location_longitude=[results1 valueForKeyPath:@"Response.user.location_longitude"];
          NSString *created_date=[results1 valueForKeyPath:@"Response.user.created_date"];
          NSString *created_ipaddress=[results1 valueForKeyPath:@"Response.user.created_ipaddress"];
          NSString *updated_date=[results1 valueForKeyPath:@"Response.user.updated_date"];
          NSString *updated_ipaddress=[results1 valueForKeyPath:@"Response.user.updated_ipaddress"];
          NSString *browser_details=[results1 valueForKeyPath:@"Response.user.browser_details"];
          NSString *user_status=[results1 valueForKeyPath:@"Response.user.user_status"];
          NSString *merchant_company_details=[results1 valueForKeyPath:@"Response.user.merchant_company_details"];
        
      
        NSLog(@"EMAIl:%@",email);
        
        
              
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];

}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [contactIDArray count];
}


- (receiptentCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    if (editTag==1)
    {
        cell.transparentView.hidden=NO;
        
        
        [cell.editBtn setTag:indexPath.row];
        [cell.editBtn addTarget:self action:@selector(updatebuttonClicked:)
                 forControlEvents:UIControlEventTouchDown];

        [cell.deletebtn setTag:indexPath.row];
        [cell.deletebtn addTarget:self action:@selector(deletebuttonClicked:)
               forControlEvents:UIControlEventTouchDown];
        
    }
    
    else
    {
       cell.transparentView.hidden=YES;
    }
    if ([checkmarkArray[indexPath.row]isEqualToString:@"YES"])
    {
        cell.check_image_view.image=[UIImage imageNamed:@"click.png"];
    }
    else
    {
        cell.check_image_view.image=[UIImage imageNamed:@"checkbox.png"];
    }
    
    @try {
        cell.cell_title_Label.text=[NSString stringWithFormat:@"%@ %@",FnameArray[indexPath.row],LnameArray[indexPath.row]];
    }
    @catch (NSException *exception) {
        
    }
    
    
    
    
    @try {
        NSString *temp=[imageThumpArray objectAtIndex:indexPath.row];
        [cell.image_view setImageWithURL:[NSURL URLWithString:temp]
                        placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }
    return cell;
}

-(void)updatebuttonClicked:(UIButton*)button
{
    
    _contactEditView.hidden=NO;
    [self getCountry];
    
    contactIDSelected=contactIDArray[(long int)[button tag]];
    
    NSLog(@"Contact ID:%@", contactIDSelected);
    
    
    NSString *temp=[imageThumpArray objectAtIndex:(long int)[button tag]];
 
    
    UIImageView *imv=[[UIImageView alloc]init];
    
    [imv setImageWithURL:[NSURL URLWithString:temp]
placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
    
    
    NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
    
    [imgtemp setObject:temp forKey:@"USER_IMAGE"];
    
    
  
    [_editimageButton setImage:imv.image forState:UIControlStateNormal];
    
    
    [_editimageButton setTitle:nil forState:UIControlStateNormal];
    
   
    @try {
        @try {
               _editfirstName.text=FnameArray[(long int)[button tag]];
        }
        @catch (NSException *exception) {
            
        }
        
     
        @try {
             _editlastname.text=LnameArray[(long int)[button tag]];
        }
        @catch (NSException *exception) {
            
        }
       
        @try {
           _editaddress1.text=editAddress1Arr[(long int)[button tag]]; 
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
           _editaddress2.text=editAddress2Arr[(long int)[button tag]];  
        }
        @catch (NSException *exception) {
            
        }
       
        @try {
             _editcity.text=editCityArr[(long int)[button tag]];
        }
        @catch (NSException *exception) {
            
        }
       
        @try {
           _editstate.text=editStateArr[(long int)[button tag]];   
        }
        @catch (NSException *exception) {
            
        }
       
        
        @try {
            _editzipCode.text=[NSString stringWithFormat:@"%@",editZipArr[(long int)[button tag]]];
 
        }
        @catch (NSException *exception) {
            
        }
        
    
        
        
        @try {
             _editemailID.text=editEmailArr[(long int)[button tag]];
            
        }
        @catch (NSException *exception) {
            
        }
        
        
        @try {
            NSLog(@"PH%@",editPhoneArr[(long int)[button tag]]);
            
            [_editcountryButton setTitle:[NSString stringWithFormat:@"    %@",editCountryArr[(long int)[button tag]]] forState:UIControlStateNormal];
            
            editcountrystr=editCountryArr[(long int)[button tag]];
            
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            _editphoneNo.text=[NSString stringWithFormat:@"%@",editPhoneArr[(long int)[button tag]]];
            
        }
        @catch (NSException *exception) {
            
        }
        

        
       
    }
    @catch (NSException *exception)
    {
        
    }
    
    
        
}
-(IBAction)searchBtn
{
   
    [_searchTextField resignFirstResponder];
    
    if ([_searchTextField.text isEqualToString:@""])
    {
        [self viewDidLoad];
    }
    else
    {
         NSLog(@"SEAR:%@",_searchTextField.text);
        
        [self searchResults];
    }
}

-(void)searchResults
{
   
    
    check_search=@"YES";
    
    imageThumpArray=[[NSMutableArray alloc]init];
    FnameArray=[[NSMutableArray alloc]init];
    LnameArray=[[NSMutableArray alloc]init];
    checkmark_array=[[NSMutableArray alloc]init];
    contactIDArray=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    address_entered_arr=[[NSMutableArray alloc]init];
    facebookUserID=[[NSMutableArray alloc]init];
    
    editAddress1Arr=[[NSMutableArray alloc]init];
    editAddress2Arr=[[NSMutableArray alloc]init];
    editCityArr=[[NSMutableArray alloc]init];
    editCountryArr=[[NSMutableArray alloc]init];
    editEmailArr=[[NSMutableArray alloc]init];
    editPhoneArr=[[NSMutableArray alloc]init];
    editStateArr=[[NSMutableArray alloc]init];
    editZipArr=[[NSMutableArray alloc]init];
    
    contactSourceArray=[[NSMutableArray alloc]init];
     checkmarkArray=[[NSMutableArray alloc]init];
    
    
    
    
        NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
        
        NSString *userID=[checkval objectForKey:@"USERID"];
        NSString *fbID=[checkval objectForKey:@"FB_USERID"];
        NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
        
        // userID=@"230";
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/searchContacts",CONFIG_BASE_URL]];
        
        NSLog(@"HELLO:%@",url);
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:userID forKey:@"userID"];
         [request_post setPostValue:_searchTextField.text forKey:@"searchCriteriaText"];
        
        
        [request_post setTimeOutSeconds:30];
        
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            
            
            
            
            NSMutableData *results1 = [responseString JSONValue];
            
            
            NSLog(@"RESULTS HELL:%@",results1);
            
            NSArray *temp= [results1 valueForKeyPath:@"Response.contacts"];
            
            
            
            
            for(NSDictionary *value in temp)
            {
                [imageThumpArray  addObject:[value valueForKey:@"user_image"]];
                
                
                [FnameArray  addObject:[value valueForKey:@"first_name"]];
                [LnameArray  addObject:[value valueForKey:@"last_name"]];
                
                [contactIDArray  addObject:[value valueForKey:@"pk_id"]];
                
                //  [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
                
                
                
                [editZipArr  addObject:[value valueForKey:@"zipcode"]];
                [editStateArr  addObject:[value valueForKey:@"state"]];
                [editPhoneArr  addObject:[value valueForKey:@"phone"]];
                [editEmailArr  addObject:[value valueForKey:@"email"]];
                
                [editCountryArr  addObject:[value valueForKey:@"country"]];
                [editCityArr  addObject:[value valueForKey:@"city"]];
                [editAddress2Arr  addObject:[value valueForKey:@"address2"]];
                [editAddress1Arr  addObject:[value valueForKey:@"address1"]];
                 [facebookUserID  addObject:[value valueForKey:@"fk_facebook_user_id"]];
                [contactSourceArray  addObject:[value valueForKey:@"contact_source"]];
                
                
                
            }
            
            
            NSLog(@"FIRST NAME:%@",FnameArray);
            
            for (int i=0; i<[FnameArray count]; i++)
            {
                [checkmarkArray addObject:[NSString stringWithFormat:@"NO"]];
            }
            
            [collectionView reloadData];
            
            if ([FnameArray count]==0) {
                UIAlertView *fn=[[UIAlertView alloc]initWithTitle:@"Information" message:@"No Search results found" delegate:self cancelButtonTitle:@"No" otherButtonTitles:nil, nil];
                
                [fn show];
            }
            
       
            if(checkEditedTag!=100000)
            {
                NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
                
                @try {
                    [check setObject:imageThumpArray[editId] forKey:@"CONTCAT_IMG"];
                }
                @catch (NSException *exception) {
                    [check setObject:@"" forKey:@"CONTCAT_IMG"];
                }
                
                
                @try {
                    [check setObject:FnameArray[editId] forKey:@"CONTCAT_FNAME"];
                }
                @catch (NSException *exception) {
                    [check setObject:@"" forKey:@"CONTCAT_FNAME"];
                }
                
                @try {
                    [check setObject:LnameArray[editId] forKey:@"CONTCAT_LNAME"];
                }
                @catch (NSException *exception) {
                    [check setObject:@"" forKey:@"CONTCAT_LNAME"];
                }
                
                @try {
                    [check setObject:[NSString stringWithFormat:@"%@",editEmailArr[editId]] forKey:@"CONTCAT_EMAIL"];
                }
                @catch (NSException *exception) {
                    [check setObject:@"" forKey:@"CONTCAT_EMAIL"];
                }
                
                @try {
                    [check setObject:[NSString stringWithFormat:@"%@",editPhoneArr[editId]] forKey:@"CONTCAT_PHONE"];
                }
                @catch (NSException *exception) {
                    [check setObject:@"" forKey:@"CONTCAT_PHONE"];
                }
                
                
                @try {
                    [check setObject:[NSString stringWithFormat:@"%@",contactIDArray[editId]] forKey:@"CONTCAT_ID"];
                }
                @catch (NSException *exception) {
                    [check setObject:@"" forKey:@"CONTCAT_ID"];
                }
                
                
                
                
                if (IS_IPHONE_5)
                {
                    FriendCategoryViewController *signupView=[[FriendCategoryViewController alloc]initWithNibName:@"FriendCategoryViewController" bundle:nil];
                    [self presentViewController:signupView animated:NO completion:nil];
                }
                else
                {
                    FriendCategoryViewController *signupView=[[FriendCategoryViewController alloc]initWithNibName:@"FriendCategoryViewController4" bundle:nil];
                    [self presentViewController:signupView animated:NO completion:nil];
                }
                
            }
       
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];
        
    }


-(IBAction)editSave
{
    
    
    editcountrystr=@"United States";
  
    
    
    
    //////(@"COUNTRY:%@",countrystr);
    
    if ([_editfirstName.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the first name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        if ([_editlastname.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the last name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            
            if ([_editemailID.text isEqualToString:@""])
            {
                
                
                
                
                UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [loginalert show];
                
                /*
                if ([_editphoneNo.text isEqualToString:@""])
                {
                    //[self addcontectFun];
                }
                else
                {
                    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
                    
                    // NSString *phoneRegex = @"^[\+(00)][0-9]{6,14}$";
                    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
                    
                    BOOL phoneValidates = [phoneTest evaluateWithObject:_editphoneNo.text];
                    
                    
                    
                    if (phoneValidates == YES){
                        
                        ////(@"Matched");
                        
                        [self editcontectFun];
                    }
                    else {
                        ////(@"Not matched");
                        
                        
                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter Valid Phone Number" delegate:self
                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [loginalert show];
                        
                    }
                    
                }
                 */
                
                
            }
            else
            {
                //////(@"SUCCESS");
                
                NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                NSString *subjectString =_editemailID.text;
                
                
                if((_editemailID.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                {
                    
                    if ([emailTest evaluateWithObject:subjectString] != YES)
                        
                    {
                        
                        
                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [loginalert show];
                        
                        
                    }
                    
                    
                }
                
                else
                {
                    
                    
                    
                    [self editcontectFun];
                    
                }
            }
            
                   }
    }
    
    
    
    
    
}



-(void)editcontectFun
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/updateContact",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
    
    
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://www.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    //////(@"USER IMAGE:%@",userImage);
    
    
    NSString *fname=_editfirstName.text;
    NSString *lname=_editlastname.text;
    NSString *address1text=_editaddress1.text;
    NSString *address2text=_editaddress2.text;
    NSString *citystr=_editcity.text;
    NSString *statestr=_editstate.text;
    NSString *zipCodeStr=_editzipCode.text;
    NSString *emailStr=_editemailID.text;
    
   
    
    
    
    [request_post1234 setPostValue:contactIDSelected forKey:@"contactID"];
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:fbuserid forKey:@"facebook_userid"];
    [request_post1234 setPostValue:fname forKey:@"first_name"];
    [request_post1234 setPostValue:lname forKey:@"last_name"];
    [request_post1234 setPostValue:address1text forKey:@"address1"];
    [request_post1234 setPostValue:address2text forKey:@"address2"];
    [request_post1234 setPostValue:citystr forKey:@"city"];
    [request_post1234 setPostValue:statestr forKey:@"state"];
    [request_post1234 setPostValue:editcountrystr forKey:@"country"];
    [request_post1234 setPostValue:zipCodeStr forKey:@"zipcode"];
    [request_post1234 setPostValue:_editemailID.text forKey:@"email"];
    [request_post1234 setPostValue:_editphoneNo.text forKey:@"phone"];
    [request_post1234 setPostValue:userImage forKey:@"user_image"];
   // [request_post1234 setPostValue:@"iPhone" forKey:@"contact_source"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        [_firstName resignFirstResponder];
        [_lastname resignFirstResponder];
        [_address2 resignFirstResponder];
        [_address1 resignFirstResponder];
        [_city resignFirstResponder];
        [_state resignFirstResponder];
        [_zipCode resignFirstResponder];
        
        _firstName.text=@"";
        _lastname.text=@"";
        _address2.text=@"";
        _address1.text=@"";
        _city.text=@"";
        _state.text=@"";
        _zipCode.text=@"";
        _emailID.text=@"";
        _phoneNo.text=@"";
        
        //[_countrybtn setTitle:@"  United States" forState:UIControlStateNormal];
        _contactAddView.hidden=YES;
        _addContactView.hidden=YES;
        
        
        if (editTag==0)
            
            
        {
            
            if ([check_search isEqualToString:@"YES"])
            {
                checkEditedTag=456;
                [self searchResults];
            }
            else
            {
            checkEditedTag=456;
            [self getFriendsList];
            }
            
            
        }

        
        else
        {
        [self viewDidLoad];
        }
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}




-(IBAction)editContry
{
    LeveyPopListView *lplv = [[LeveyPopListView alloc] initWithTitle:@"Select country" options:countryArray handler:^(NSInteger anIndex)
                              {
                                  //////(@"%@",[NSString stringWithFormat:@"You have CLICKED %@", countryArray[anIndex]]);
                                  
                                  [_editcountrybtn setTitle:[NSString stringWithFormat:@"    %@", countryArray[anIndex]] forState:UIControlStateNormal];
                                  
                                  editcountrystr=countryArray[anIndex];
                                  
                              }];
    //    lplv.delegate = self;
    [lplv showInView:self.view animated:YES];
}

-(IBAction)editImage
{
    [_editfirstName resignFirstResponder];
    [_editlastname resignFirstResponder];
    [_editaddress1 resignFirstResponder];
    [_editaddress2 resignFirstResponder];
    [_editcity resignFirstResponder];
    [_editstate resignFirstResponder];
    [_editzipCode resignFirstResponder];
    
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
  
}



-(void)deletebuttonClicked:(UIButton*)button
{
   
    
    contactIDName=contactIDArray[(long int)[button tag]];
    
    firstalert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Do you want to delete this contact?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [firstalert show];
    
    
    
    
    
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if (editTag==0)
    {
        
        NSString *email_str=[NSString stringWithFormat:@"%@",editEmailArr[indexPath.row]];
       
        
        if ([email_str isEqualToString:@""])
        {
            
            
            editId=indexPath.row;
            
            
            NSString *fnandLnStr=[NSString stringWithFormat:@"Please add %@ %@'s email address",FnameArray[indexPath.row],LnameArray[indexPath.row]];
            
            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Email ID Missing" message:fnandLnStr delegate:self
                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [loginalert show];
            
            _contactEditView.hidden=NO;
            [self getCountry];
            
            contactIDSelected=contactIDArray[indexPath.row];
            
            NSLog(@"Contact ID:%@", contactIDSelected);
            
            
            NSString *temp=[imageThumpArray objectAtIndex:indexPath.row];
            
            
            UIImageView *imv=[[UIImageView alloc]init];
            
            [imv setImageWithURL:[NSURL URLWithString:temp]
                placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
            
            
            NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
            
            [imgtemp setObject:temp forKey:@"USER_IMAGE"];
            
            
            
            [_editimageButton setImage:imv.image forState:UIControlStateNormal];
            
            
            [_editimageButton setTitle:nil forState:UIControlStateNormal];
            
            
            @try {
                @try {
                    _editfirstName.text=FnameArray[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                
                @try {
                    _editlastname.text=LnameArray[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                @try {
                    _editaddress1.text=editAddress1Arr[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                @try {
                    _editaddress2.text=editAddress2Arr[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                @try {
                    _editcity.text=editCityArr[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                @try {
                    _editstate.text=editStateArr[indexPath.row];
                }
                @catch (NSException *exception) {
                    
                }
                
                
                @try {
                    _editzipCode.text=[NSString stringWithFormat:@"%@",editZipArr[indexPath.row]];
                    
                }
                @catch (NSException *exception) {
                    
                }
                
                
                
                
                @try {
                    _editemailID.text=editEmailArr[indexPath.row];
                    
                }
                @catch (NSException *exception) {
                    
                }
                
                
                @try {
                    NSLog(@"PH%@",editPhoneArr[indexPath.row]);
                    
                    [_editcountryButton setTitle:[NSString stringWithFormat:@"    %@",editCountryArr[indexPath.row]] forState:UIControlStateNormal];
                    
                    editcountrystr=editCountryArr[indexPath.row];
                    
                }
                @catch (NSException *exception) {
                    
                }
                
                @try {
                    _editphoneNo.text=[NSString stringWithFormat:@"%@",editPhoneArr[indexPath.row]];
                    
                }
                @catch (NSException *exception) {
                    
                }
                
                
                
                
            }
            @catch (NSException *exception)
            {
                
            }
            
            

            
        }
        else
        {
        
        
        
        
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        
        @try {
            [check setObject:imageThumpArray[indexPath.row] forKey:@"CONTCAT_IMG"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_IMG"];
        }
        
        
        @try {
            [check setObject:FnameArray[indexPath.row] forKey:@"CONTCAT_FNAME"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_FNAME"];
        }
        
        @try {
            [check setObject:LnameArray[indexPath.row] forKey:@"CONTCAT_LNAME"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_LNAME"];
        }
        
            @try {
                [check setObject:[NSString stringWithFormat:@"%@",editEmailArr[indexPath.row]] forKey:@"CONTCAT_EMAIL"];
            }
            @catch (NSException *exception) {
                [check setObject:@"" forKey:@"CONTCAT_EMAIL"];
            }
            
            @try {
                [check setObject:[NSString stringWithFormat:@"%@",editPhoneArr[indexPath.row]] forKey:@"CONTCAT_PHONE"];
            }
            @catch (NSException *exception) {
                [check setObject:@"" forKey:@"CONTCAT_PHONE"];
            }

        
        
        @try {
            [check setObject:[NSString stringWithFormat:@"%@",editAddress1Arr[indexPath.row]] forKey:@"CONTCAT_AD1"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_AD1"];
        }
        
        @try {
            [check setObject:[NSString stringWithFormat:@"%@",editAddress2Arr[indexPath.row]] forKey:@"CONTCAT_AD2"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_AD2"];
        }
        
        @try {
            [check setObject:[NSString stringWithFormat:@"%@",editCityArr[indexPath.row]] forKey:@"CONTCAT_CITY"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_CITY"];
        }
        
        @try {
            [check setObject:[NSString stringWithFormat:@"%@",editCountryArr[indexPath.row]] forKey:@"CONTCAT_COUNTRY"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_COUNTRY"];
        }
        
        @try {
            [check setObject:[NSString stringWithFormat:@"%@",editZipArr[indexPath.row]] forKey:@"CONTCAT_ZIP"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_ZIP"];
        }
        
        @try {
            [check setObject:[NSString stringWithFormat:@"%@",editStateArr[indexPath.row]] forKey:@"CONTCAT_STATE"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_STATE"];
        }
        
        @try {
            [check setObject:[NSString stringWithFormat:@"%@",contactIDArray[indexPath.row]] forKey:@"CONTCAT_ID"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_ID"];
        }
        
        @try {
            [check setObject:[NSString stringWithFormat:@"%@",editCountryArr[indexPath.row]] forKey:@"CONTCAT_COUNTRY"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_COUNTRY"];
        }
        
        @try {
            [check setObject:[NSString stringWithFormat:@"%@",facebookUserID[indexPath.row]] forKey:@"CONTCAT_FBID"];
        }
        @catch (NSException *exception) {
            [check setObject:@"" forKey:@"CONTCAT_FBID"];
        }

        
        
        if (IS_IPHONE_5)
        {
            FriendCategoryViewController *signupView=[[FriendCategoryViewController alloc]initWithNibName:@"FriendCategoryViewController" bundle:nil];
            [self presentViewController:signupView animated:NO completion:nil];
        }
        else
        {
            FriendCategoryViewController *signupView=[[FriendCategoryViewController alloc]initWithNibName:@"FriendCategoryViewController4" bundle:nil];
            [self presentViewController:signupView animated:NO completion:nil];
        }
        }

    }
    
   
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    [textField resignFirstResponder];
    
      
    
    return YES;
    
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
   
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}
#pragma mark - LeveyPopListView delegates
- (void)leveyPopListView:(LeveyPopListView *)popListView didSelectedIndex:(NSInteger)anIndex
{
    NSLog(@"%@",[NSString stringWithFormat:@"You have selected %@", countryArray[anIndex]]);
}
- (void)leveyPopListViewDidCancel
{
    NSLog(@"CANCELLED");
}
////// FACEBOOK END //
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (buttonIndex==0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
        }
        
    }
    else  if (buttonIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
    
}


-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    NSData *imagedata = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage" ],1);
    UIImage *image = [[UIImage alloc] initWithData:imagedata];
    
    
    //  image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(240, 240)];
    
    
    float ori_height=image.size.height;
    float ori_width=image.size.width;
    
    NSLog(@"H:%f|W:%f",ori_height,ori_width);
    
    
    float height_ratio1=ori_height/4;
    float width_ratio1=ori_width/4;
    
    NSLog(@"HJ:%f|WJ:%f",height_ratio1,width_ratio1);
    
    
    
    image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(width_ratio1, height_ratio1)];
    
    
    if (editTag==0)
    {
        
        [_imageButton setImage:image forState:UIControlStateNormal];
        [_imageButton setTitle:nil forState:UIControlStateNormal];
    }
    else
    {
        [_editimageButton setImage:image forState:UIControlStateNormal];
        [_editimageButton setTitle:nil forState:UIControlStateNormal];
    }
    
   
   
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postProImgfile",@"http://api.socialnetgateapp.com/v1"]];
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    //[request_post setFile:@"/Users/apple/Desktop/Free-Grammar-Ebook-Level-2.pdf" forKey:@"filename"];
    
    //UIImage * ourImage = [UIImage imageNamed:@"rajini-bday2.jpg"];
    NSData * ourImageData = UIImageJPEGRepresentation(image, 100);
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:ss zzz"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    //////(@"DATE STRING:%@",dateString);
    
    NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
    NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSString *imageName=[NSString stringWithFormat:@"%@.jpg",newString3];
    
    
    
    [request_post addData:ourImageData withFileName:imageName andContentType:@"image/jpeg" forKey:@"file"];
    
    [request_post setTimeOutSeconds:60];
    
    
    [request_post setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request_post responseString];
        
        
        //////(@"RES:%@",responseString);
        
        NSMutableData *results11 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *temp= [results11 valueForKeyPath:@"photos.profile_img_url"];
        
        
        NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
        
        [imgtemp setObject:temp forKey:@"USER_IMAGE"];
        
        
        
        
        if ([temp length]==0) {
            
            NSString *temp= [results11 valueForKeyPath:@"error.text"];
            
            
            NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
            
            [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
            
            [_imageButton setImage:nil forState:UIControlStateNormal];
           
            
        }
        
        // Use when fetching binary data
        // NSData *responseData = [request_post responseData];
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        
        
        
        
    }];
    [request_post startAsynchronous];
    
    
    [picker dismissModalViewControllerAnimated:YES];
    
    
    
    
}
-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissModalViewControllerAnimated:YES];
}


-(IBAction)friends
{
    if (IS_IPHONE_5)
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }

}
-(IBAction)business
{
    if (IS_IPHONE_5)
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }

}
-(IBAction)room
{
    if (IS_IPHONE_5)
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)setttings
{
    if (IS_IPHONE_5)
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)more
{
    if (IS_IPHONE_5)
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}



@end

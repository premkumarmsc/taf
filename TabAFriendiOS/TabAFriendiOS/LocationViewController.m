//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "LocationViewController.h"
#import "receiptentCell.h"
#import "UIImageView+WebCache.h"

@interface LocationViewController ()

@end

@implementation LocationViewController
@synthesize collectionView;
receiptentCell *cell;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *contactIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;
NSArray *origin_address_drive;
NSArray *destination_addresses_drive;
NSArray *origin_address;
NSArray *destination_addresses;

NSString *contactIDName;


float from_lat_value;
float from_long_value;

float lat_value;
float long_value;


int editTag;


- (void)viewDidLoad
{
   
    locationController = [[MyCLController alloc] init];
	locationController.delegate = self;
	[locationController.locationManager startUpdatingLocation];
    
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

-(IBAction)addBack
{
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction)update:(id)sender
{
    locationController = [[MyCLController alloc] init];
	locationController.delegate = self;
	[locationController.locationManager startUpdatingLocation];
}
- (void)locationUpdate:(CLLocation *)location
{
	//NSLog(@"%@",[location description]);
    
    
    
    
    NSString *locLat  = [NSString stringWithFormat:@"%lf", location.coordinate.latitude];
    NSString * locLong = [NSString stringWithFormat:@"%lf", location.coordinate.longitude];
    
    float from_lat_value=[locLat floatValue];
    float from_long_value=[locLong floatValue];
    
    
    float to_lat_value1=[locLat floatValue]+.2;
    float to_long_value=[locLong floatValue]+.2;
    
    
    NSUserDefaults *addLat=[NSUserDefaults standardUserDefaults];
    [addLat setObject:locLat forKey:@"FROM_LAT"];
      [addLat setObject:locLong forKey:@"FROM_LONG"];
    
    NSString *locLat1  = [NSString stringWithFormat:@"%f", to_lat_value1];
    NSString * locLong1 = [NSString stringWithFormat:@"%f", to_long_value];
    
   // [addLat setObject:locLat1 forKey:@"TO_LAT"];
   // [addLat setObject:locLong1 forKey:@"TO_LONG"];
    
    
    
    [self walk_distance];
    [self drive_distance];
        
    [locationController.locationManager stopUpdatingLocation];
}

- (void)locationError:(NSError *)error
{
	NSLog(@"Error");
}


-(void)walk_distance
{
    
    
    //lat_value=from_lat_value+.2;
   // long_value=from_long_value+.2;
    

        
    
    NSUserDefaults *addLat=[NSUserDefaults standardUserDefaults];
   
    
    float fr_lat=[[NSString stringWithFormat:@"%@",[addLat objectForKey:@"FROM_LAT"]] floatValue];
     float fr_long=[[NSString stringWithFormat:@"%@",[addLat objectForKey:@"FROM_LONG"]] floatValue];

    
    float to_lat=[[NSString stringWithFormat:@"%@",[addLat objectForKey:@"TO_LAT"]] floatValue];
    float to_long=[[NSString stringWithFormat:@"%@",[addLat objectForKey:@"TO_LONG"]] floatValue];

    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&units=metric&sensor=false&mode=walking",fr_lat,fr_long,to_lat,to_long]];
    
    //walking, cycling and car
    
    
    NSLog(@"URL:%@",url);
    
    
    __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    
    [request setShouldContinueWhenAppEntersBackground:YES];
    
    
    [request setCompletionBlock:^{
        // Use when fetching text data
        
        NSString *responseString = [request responseString];
        
        
        SBJSON *json=[[SBJSON alloc]init];
        NSMutableDictionary *dict=[json objectWithString:responseString allowScalar:NO error:nil];
        
        [self Walk:dict];
        
        
    }];
    [request setFailedBlock:^{
        
    }];
    [request startAsynchronous];
}

- (IBAction)Walk:(id)sender
{
    NSMutableDictionary *new_dict=[[NSMutableDictionary alloc]init];
    new_dict=(id)sender;
    
    NSLog(@"HELLO:%@",new_dict);
    
    
    origin_address=[new_dict valueForKey:@"origin_addresses"];
    destination_addresses=[new_dict valueForKey:@"destination_addresses"];
    NSArray *distance_string=[new_dict valueForKeyPath:@"rows.elements.distance.text"];
    NSArray *distance_duration=[new_dict valueForKeyPath:@"rows.elements.duration.text"];
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    [check setObject:origin_address[0] forKey:@"WALK_ORI"];
    [check setObject:destination_addresses[0] forKey:@"WALK_DES"];
    
    _Distance_lbl.text=distance_string[0][0];
    _Duration_lbl.text=distance_duration[0][0];
    
    NSLog(@"%@ TO %@",origin_address[0],destination_addresses[0]);
    NSLog(@"DISTANCE %@ TO DURATION :%@",distance_string[0][0],distance_duration[0][0]);
    
}

-(void)drive_distance
{
    
    
    
    NSUserDefaults *addLat=[NSUserDefaults standardUserDefaults];
    
    
    float fr_lat=[[NSString stringWithFormat:@"%@",[addLat objectForKey:@"FROM_LAT"]] floatValue];
    float fr_long=[[NSString stringWithFormat:@"%@",[addLat objectForKey:@"FROM_LONG"]] floatValue];
    
    
    float to_lat=[[NSString stringWithFormat:@"%@",[addLat objectForKey:@"TO_LAT"]] floatValue];
    float to_long=[[NSString stringWithFormat:@"%@",[addLat objectForKey:@"TO_LONG"]] floatValue];
    
       
    
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&units=metric&sensor=false&mode=driving",fr_lat,fr_long,to_lat,to_long]];
    
    NSLog(@"URL:%@",url);
    
    
    __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    
    [request setShouldContinueWhenAppEntersBackground:YES];
    
    
    [request setCompletionBlock:^{
        // Use when fetching text data
        
        NSString *responseString = [request responseString];
        
        
        SBJSON *json=[[SBJSON alloc]init];
        NSMutableDictionary *dict=[json objectWithString:responseString allowScalar:NO error:nil];
        
        [self Drive:dict];
        
        
    }];
    [request setFailedBlock:^{
        
    }];
    [request startAsynchronous];
}


- (IBAction)Drive:(id)sender
{
    NSMutableDictionary *new_dict=[[NSMutableDictionary alloc]init];
    new_dict=(id)sender;
    
    NSLog(@"HELLO:%@",new_dict);
    
    
    origin_address_drive=[new_dict valueForKey:@"origin_addresses"];
    destination_addresses_drive=[new_dict valueForKey:@"destination_addresses"];
    NSArray *distance_string=[new_dict valueForKeyPath:@"rows.elements.distance.text"];
    NSArray *distance_duration=[new_dict valueForKeyPath:@"rows.elements.duration.text"];
    
    NSLog(@"%@ TO %@",origin_address_drive[0],destination_addresses_drive[0]);
    NSLog(@"DISTANCE %@ TO DURATION :%@",distance_string[0][0],distance_duration[0][0]);
    
    _Drive_dis_lbl.text=distance_string[0][0];
    _Drive_dur_lbl.text=distance_duration[0][0];
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    [check setObject:origin_address_drive[0] forKey:@"DRIVE_ORI"];
    [check setObject:destination_addresses_drive[0] forKey:@"DRIVE_DES"];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"File"
                                                     ofType:@"txt"];
    NSString* content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    
    
    content=[content stringByReplacingOccurrencesOfString:@"SOURCE" withString:origin_address_drive[0]];
    
    content=[content stringByReplacingOccurrencesOfString:@"DESTINATION" withString:destination_addresses_drive[0]];
    
    content=[content stringByReplacingOccurrencesOfString:@"google.maps.DirectionsTravelMode.WALKING" withString:@"google.maps.DirectionsTravelMode.DRIVING"];
    
    if (IS_IPHONE_5)
    {
        content=[content stringByReplacingOccurrencesOfString:@"height: 685px;" withString:@"height: 330px;"];
        content=[content stringByReplacingOccurrencesOfString:@"width: 1004px;" withString:@"width: 305px;"];
    }
    else
    {
        content=[content stringByReplacingOccurrencesOfString:@"height: 685px;" withString:@"height: 270px;"];
        content=[content stringByReplacingOccurrencesOfString:@"width: 1004px;" withString:@"width: 305px;"];
    }
   
    
    
    [_web loadHTMLString:content baseURL:nil];
    
       
    
}

-(IBAction)friends
{
    if (IS_IPHONE_5)
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)business
{
    if (IS_IPHONE_5)
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)room
{
    if (IS_IPHONE_5)
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)setttings
{
    if (IS_IPHONE_5)
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)more
{
    if (IS_IPHONE_5)
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}




@end

//
//  ViewController.h
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate,UIActionSheetDelegate>
{
      
}@property(retain,nonatomic)IBOutlet UICollectionView *collectionView;
@property(retain,nonatomic)IBOutlet UILabel *titleLabel;


@property(nonatomic,retain)IBOutlet UIView *backView;
@property(nonatomic,retain)IBOutlet UIView *bankView;
@property(nonatomic,retain)IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
-(IBAction)pickOne:(id)sender;
-(IBAction)profile;
-(IBAction)aboutus;
-(IBAction)contactus;
-(IBAction)account;

-(IBAction)searchBtn;


-(IBAction)addBack;
-(IBAction)saveContact;
-(IBAction)imageButton;


-(IBAction)friends;
-(IBAction)business;
-(IBAction)room;
-(IBAction)setttings;
-(IBAction)more;

-(IBAction)saveAccount;
-(IBAction)backAccount;


-(IBAction)clearCard;
-(IBAction)clearbank;

-(IBAction)saveCard;
-(IBAction)cardFieldChange:(id)sender;
-(IBAction)cardFieldChange1:(id)sender;


@property (weak, nonatomic) IBOutlet UIView  *accountEditView;
@property (weak, nonatomic) IBOutlet UITextField *accountName;
@property (weak, nonatomic) IBOutlet UITextField *accountNumber;
@property (weak, nonatomic) IBOutlet UITextField *bankName;
@property (weak, nonatomic) IBOutlet UITextField *bankcode;


@property (weak, nonatomic) IBOutlet UITextField *cardName;
@property (weak, nonatomic) IBOutlet UITextField *cardNumber;
@property (weak, nonatomic) IBOutlet UITextField *cardExpiry;
@property (weak, nonatomic) IBOutlet UITextField *cardExpiryYear;


@property (weak, nonatomic) IBOutlet UITextField *editaddress1;
@property (weak, nonatomic) IBOutlet UITextField *editaddress2;
@property (weak, nonatomic) IBOutlet UITextField *editzipCode;
@property (weak, nonatomic) IBOutlet UITextField *editfirstName;
@property (weak, nonatomic) IBOutlet UITextField *editlastname;
@property (weak, nonatomic) IBOutlet UITextField *editcity;
@property (weak, nonatomic) IBOutlet UITextField *editstate;
@property (weak, nonatomic) IBOutlet UIButton *editcountrybtn;
@property (weak, nonatomic) IBOutlet UIView  *contactEditView;
@property (weak, nonatomic) IBOutlet UITextField *editpayemailID;
@property (weak, nonatomic) IBOutlet UITextField *editphoneNo;
@property (weak, nonatomic) IBOutlet UIImageView *imv;
@property (weak, nonatomic) IBOutlet UIImageView *imv1;
@property (weak, nonatomic) IBOutlet UIButton *editimageButton;
@property (weak, nonatomic) IBOutlet UIButton *editcountryButton;


-(IBAction)cardNumberClick:(id)sender;
-(IBAction)cardNumberClick1:(id)sender;

@property (weak, nonatomic)IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic)IBOutlet UIScrollView *scrollView1;

@end

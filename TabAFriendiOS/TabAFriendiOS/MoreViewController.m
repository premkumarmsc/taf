//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "MoreViewController.h"
#import "receiptentCell.h"
#import "UIImageView+WebCache.h"

@interface MoreViewController ()

@end

@implementation MoreViewController
@synthesize collectionView;
receiptentCell *cell;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *contactIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;


NSString *contactIDName;



-(void)viewDidLoad
{
    
    if (!IS_IPHONE_5)
    {
        
        _scrollView.contentSize = CGSizeMake(306,480);
        
        [self.view addSubview:_contactEditView];
        _contactEditView.hidden=YES;
        
        [self.view addSubview:_accountEditView];
        _accountEditView.hidden=YES;
        
        
        [_scrollView addSubview:_bankView];
        [_scrollView addSubview:_cardView];
    }
    else
    {
    [self.view addSubview:_contactEditView];
    _contactEditView.hidden=YES;
    
    [self.view addSubview:_accountEditView];
    _accountEditView.hidden=YES;
    
    
    [_backView addSubview:_bankView];
    [_backView addSubview:_cardView];
    }
    
    _cardView.hidden=YES;
    
    
    [self getUserDetails];
}
-(IBAction)pickOne:(id)sender
{
    if (_segment.selectedSegmentIndex==0)
    {
        [self keyboardHide];
       _cardView.hidden=YES;
        _bankView.hidden=NO;
        [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    else
    {
        [self keyboardHide];
        _cardView.hidden=NO;
        _bankView.hidden=YES;
        [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}
-(void)getUserDetails
{
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    NSString *fbID=[checkval objectForKey:@"FB_USERID"];
    NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
    
   // userID=@"230";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserDetails",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
    
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        NSArray *pk_id=[results1 valueForKeyPath:@"Response.user.pk_id"];
        NSArray *fk_facebook_user_id=[results1 valueForKeyPath:@"Response.user.fk_facebook_user_id"];
        NSArray *fk_merchant_package_id=[results1 valueForKeyPath:@"Response.user.fk_merchant_package_id"];
         NSArray *first_name=[results1 valueForKeyPath:@"Response.user.first_name"];
          NSArray *last_name=[results1 valueForKeyPath:@"Response.user.last_name"];
          NSArray *email=[results1 valueForKeyPath:@"Response.user.email"];
          NSArray *password=[results1 valueForKeyPath:@"Response.user.password"];
          NSArray *signup_type=[results1 valueForKeyPath:@"Response.user.signup_type"];
          NSArray *facebook_access_token=[results1 valueForKeyPath:@"Response.user.facebook_access_token"];
          NSArray *image=[results1 valueForKeyPath:@"Response.user.image"];
          NSArray *address1=[results1 valueForKeyPath:@"Response.user.address1"];
          NSArray *address2=[results1 valueForKeyPath:@"Response.user.address2"];
          NSArray *city=[results1 valueForKeyPath:@"Response.user.city"];
          NSArray *state=[results1 valueForKeyPath:@"Response.user.state"];
          NSArray *zipcode=[results1 valueForKeyPath:@"Response.user.zipcode"];
          NSArray *country=[results1 valueForKeyPath:@"Response.user.country"];
          NSArray *phone=[results1 valueForKeyPath:@"Response.user.phone"];
          NSArray *user_security_pin=[results1 valueForKeyPath:@"Response.user.user_security_pin"];
          NSArray *user_type=[results1 valueForKeyPath:@"Response.user.user_type"];
          NSArray *location_latitude=[results1 valueForKeyPath:@"Response.user.location_latitude"];
          NSArray *location_longitude=[results1 valueForKeyPath:@"Response.user.location_longitude"];
          NSArray *created_date=[results1 valueForKeyPath:@"Response.user.created_date"];
          NSArray *created_ipaddress=[results1 valueForKeyPath:@"Response.user.created_ipaddress"];
          NSArray *updated_date=[results1 valueForKeyPath:@"Response.user.updated_date"];
          NSArray *updated_ipaddress=[results1 valueForKeyPath:@"Response.user.updated_ipaddress"];
          NSArray *browser_details=[results1 valueForKeyPath:@"Response.user.browser_details"];
          NSArray *user_status=[results1 valueForKeyPath:@"Response.user.user_status"];
          NSArray *merchant_company_details=[results1 valueForKeyPath:@"Response.user.merchant_company_details"];
        
        
        
        NSArray *payemail=[results1 valueForKeyPath:@"Response.user.paypal_mailid"];
        NSArray *fbshare=[results1 valueForKeyPath:@"Response.user.fb_share"];
        
      
        
        NSArray *accNameArr=[results1 valueForKeyPath:@"Response.user.bank_acc_holder_name"];
        NSArray *accNumArr=[results1 valueForKeyPath:@"Response.user.bank_acc_number"];
        NSArray *bankNameArr=[results1 valueForKeyPath:@"Response.user.bank_name"];
        NSArray *bankCodeArr=[results1 valueForKeyPath:@"Response.user.bank_swift_code"];
        
        
        NSArray *cardNameArr=[results1 valueForKeyPath:@"Response.user.card_holder_name"];
        NSArray *cardNumArr=[results1 valueForKeyPath:@"Response.user.card_number"];
        NSArray *cardExpArr=[results1 valueForKeyPath:@"Response.user.card_expiry_date"];
        
        
        
        NSLog(@"EMAIl:%@",email);
        
        
        @try {
              NSString *text=[NSString stringWithFormat:@"Hi \"%@ %@\"",first_name[0],last_name[0]];
            
            _titleLabel.text=text;
        }
        @catch (NSException *exception) {
            
        }
       
      
        @try {
            NSString *text=[NSString stringWithFormat:@"%@",first_name[0]];
            
            _editfirstName.text=text;
        }
        @catch (NSException *exception) {
            
        }

        @try {
            NSString *text=[NSString stringWithFormat:@"%@",last_name[0]];
            
            _editlastname.text=text;
        }
        @catch (NSException *exception) {
            
        }

        @try {
            NSString *text=[NSString stringWithFormat:@"%@",phone[0]];
            
            _editphoneNo.text=text;
        }
        @catch (NSException *exception) {
            
        }

        @try {
            NSString *text=[NSString stringWithFormat:@"%@",payemail[0]];
            
            _editpayemailID.text=text;
        }
        @catch (NSException *exception) {
            
        }

        
        
        
        @try {
            NSString *text=[NSString stringWithFormat:@"%@",accNameArr[0]];
            
            _accountName.text=text;
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            NSString *text=[NSString stringWithFormat:@"%@",accNumArr[0]];
            
            text = [text stringByReplacingOccurrencesOfString:@"B"
                                                   withString:@""];
            
            _accountNumber.text=text;
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            NSString *text=[NSString stringWithFormat:@"%@",bankNameArr[0]];
            
            _bankName.text=text;
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            NSString *text=[NSString stringWithFormat:@"%@",bankCodeArr[0]];
            
            _bankcode.text=text;
        }
        @catch (NSException *exception) {
            
        }

        
        @try {
            NSString *text=[NSString stringWithFormat:@"%@",cardNameArr[0]];
            
            _cardName.text=text;
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            NSString *text=[NSString stringWithFormat:@"%@",cardNumArr[0]];
            
            
          
            
            text = [text stringByReplacingOccurrencesOfString:@"C"
                                                                         withString:@""];

            
            _cardNumber.text=text;
        }
        @catch (NSException *exception) {
            
        }
        
        @try {
            NSString *text=[NSString stringWithFormat:@"%@",cardExpArr[0]];
            
            
            NSArray *lines = [text componentsSeparatedByString: @"/"];

            
            
            _cardExpiry.text=lines[0];
            _cardExpiryYear.text=lines[1];
        }
        @catch (NSException *exception) {
            
        }

        
        
        
        NSString *temp=[image objectAtIndex:0];
        
        
        NSLog(@"TEMP:%@",temp);
        
        
        
        
        [_imv setImageWithURL:[NSURL URLWithString:temp]
            placeholderImage:[UIImage imageNamed:@"bigicon.png"]];
        
        [_imv1 setImageWithURL:[NSURL URLWithString:temp]
             placeholderImage:[UIImage imageNamed:@"bigicon.png"]];

        
        
        NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
        
        [imgtemp setObject:temp forKey:@"USER_IMAGE11"];
        
        
        if ([fbshare[0]isEqualToString:@"yes"])
        {
            [imgtemp setObject:@"ON" forKey:@"FB_POST"]; 
        }
        else
        {
        
        [imgtemp setObject:@"OFF" forKey:@"FB_POST"];
        }
        
       
        
                
        //[_editimageButton setImage:_imv.image forState:UIControlStateNormal];
        
        
       // [_editimageButton setTitle:nil forState:UIControlStateNormal];

        
        
              
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];

}

-(IBAction)cardFieldChange:(id)sender
{
    
    if (sender==_cardExpiry) {
        
        
        NSString *lenStr=_cardExpiry.text;
        int len=[lenStr length];
        
        if (len==2)
        {
            
            [_cardExpiryYear becomeFirstResponder];
        }
        
          int lenVal=[lenStr intValue];
          if(lenVal<10&&lenVal>1)
          {
              _cardExpiry.text=[NSString stringWithFormat:@"0%d",lenVal];
              [_cardExpiryYear becomeFirstResponder];
              
           }

        
    }
           
    
    
    
}

-(IBAction)cardFieldChange1:(id)sender
{
    
    if (sender==_cardExpiryYear) {
        
        
        NSString *lenStr=_cardExpiryYear.text;
        int len=[lenStr length];
        
        
        NSString *lenStr1=_cardExpiry.text;
               
        int lenVal=[lenStr1 intValue];
        if(lenVal==1)
        {
            _cardExpiry.text=[NSString stringWithFormat:@"0%d",lenVal];
           
            
        }
        
        
        if (len==2)
        {
            
            [_cardExpiryYear resignFirstResponder];
        }
        
       
        
        
    }
    
    
    
    
}
 




-(IBAction)clearCard
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/editUserProfile",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:@"" forKey:@"card_holder_name"];
    [request_post1234 setPostValue:@"" forKey:@"card_number"];
    [request_post1234 setPostValue:@"" forKey:@"card_expiry_date"];
    
    
    
    
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        [self viewDidLoad];
        
        
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}

-(IBAction)clearbank
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/editUserProfile",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
    
    
    
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:@"" forKey:@"bank_acc_holder_name"];
    [request_post1234 setPostValue:@"" forKey:@"bank_acc_number"];
    [request_post1234 setPostValue:@"" forKey:@"bank_name"];
    [request_post1234 setPostValue:@"" forKey:@"bank_swift_code"];
    
    
    
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}




-(IBAction)saveCard
{
    if ([_cardName.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter Card name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        if ([_cardNumber.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter 16 digit card number." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            
            if ([_cardExpiry.text isEqualToString:@""])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter expiry Month in (MM) format" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
            }
            else
            {
                
               
                    
                if ([_cardExpiryYear.text isEqualToString:@""])
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter expiry Year in (YY format)" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    [alert show];
                }
                else
                {
                    
                    
                    
                    
                    
                    [self updateCardDetails];
                    
                    
                    
                }

                    
                
                
            }
            
        }
    }
}


-(void)updateCardDetails
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/editUserProfile",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
    
    
    
    NSString *expString=[NSString stringWithFormat:@"%@/%@",_cardExpiry.text,_cardExpiryYear.text];
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:_cardName.text forKey:@"card_holder_name"];
    [request_post1234 setPostValue:_cardNumber.text forKey:@"card_number"];
    [request_post1234 setPostValue:expString forKey:@"card_expiry_date"];
   
    
    
    
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        
       
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}




-(IBAction)profile
{
    _contactEditView.hidden=NO;
}
-(IBAction)aboutus
{
    
}
-(IBAction)contactus
{
    
}
-(IBAction)account
{
     _accountEditView.hidden=NO;
    _cardView.hidden=YES;
    _bankView.hidden=NO;
}
-(IBAction)addBack
{
    [self viewDidLoad];
    _contactEditView.hidden=YES;
}

-(IBAction)saveAccount
{
    if ([_accountName.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter your full name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        if ([_accountNumber.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Enter Account Number" message:@"Your account number (usually 10-12 digits) is specific to your personal account. It’s the second set of numbers printed on the bottom of your checks, just to the right of the routing number. You can also find your account number on your monthly statement." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
                            
                if ([_bankcode.text isEqualToString:@""])
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Enter Routing Number" message:@"Your bank routing number is a nine-digit code that’s based on the U.S. Bank location where your account was opened. It’s the first set of numbers printed on the bottom of your checks, on the left side." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    
                    [alert show];
                }
                else
                {
                    
                    NSRange whiteSpaceRange = [_accountName.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
                    if (whiteSpaceRange.location != NSNotFound) {
                        NSLog(@"Found whitespace");
                        
                         [self verifyBankDetails];
                    }
                   else
                   {
                       UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Enter Valid Name" message:@"You will want to double check that your Name is correct to a verified bank account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                       
                       [alert show];
                   }
                    
                }
                
            
            
        }
    }
}

-(void)verifyBankDetails
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/BankdetailValidation",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
      
    
   
    [request_post1234 setPostValue:@"US" forKey:@"Country"];
    [request_post1234 setPostValue:_accountNumber.text forKey:@"Accountnumber"];
    [request_post1234 setPostValue:_bankcode.text forKey:@"Routingnumber"];
    
    
    
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESPONS:%@",responseString23);
        if ([responseString23 isEqualToString:@""])
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:@"Invalid Bank details" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        [self updateBankDetails];
        }
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}

-(void)updateBankDetails
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/editUserProfile",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
    
       
    
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:_accountName.text forKey:@"bank_acc_holder_name"];
    [request_post1234 setPostValue:_accountNumber.text forKey:@"bank_acc_number"];
    [request_post1234 setPostValue:@"B" forKey:@"bank_name"];
    [request_post1234 setPostValue:_bankcode.text forKey:@"bank_swift_code"];
    
    
   
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}



-(IBAction)backAccount
{
    [self viewDidLoad];
    [self keyboardHide];
    _accountEditView.hidden=YES;
}



-(IBAction)saveContact
{
    
    
    editcountrystr=@"United States";
    
    
    
    
    //////(@"COUNTRY:%@",countrystr);
    
    if ([_editfirstName.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the first name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        if ([_editlastname.text isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Enter the last name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
        else
        {
            
            if ([_editpayemailID.text isEqualToString:@""])
            {
                
                
                
                
                UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [loginalert show];
                
                              
                
            }
            else
            {
                //////(@"SUCCESS");
                
                NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                NSString *subjectString =_editpayemailID.text;
                
                
                if((_editpayemailID.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                {
                    
                    if ([emailTest evaluateWithObject:subjectString] != YES)
                        
                    {
                        
                        
                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [loginalert show];
                        
                        
                    }
                    
                    
                }
                
                else
                {
                    
                    
                    
                    [self editcontectFun];
                    
                }
            }
            
                 }
    }
    
    
    
    
    
}


-(void)editcontectFun
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/editUserProfile",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE11"];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
    
    
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://www.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    //////(@"USER IMAGE:%@",userImage);
    
    
    NSString *fname=_editfirstName.text;
    NSString *lname=_editlastname.text;
   
    NSString *emailStr=_editpayemailID.text;
     NSString *phoneStr=_editphoneNo.text;
    
    
    NSLog(@"FN:%@/LN:%@/emai:%@/pay:%@/img:%@",fname,lname,emailStr,phoneStr,userImage);
    
    
  
    
    
    
    
   
    [request_post1234 setPostValue:user forKey:@"userID"];
       [request_post1234 setPostValue:fname forKey:@"first_name"];
    [request_post1234 setPostValue:lname forKey:@"last_name"];
    [request_post1234 setPostValue:_editpayemailID.text forKey:@"paypal_mailid"];
    [request_post1234 setPostValue:_editphoneNo.text forKey:@"phone"];
    
    
    [request_post1234 setPostValue:userImage forKey:@"profile_pic"];
    // [request_post1234 setPostValue:@"iPhone" forKey:@"contact_source"];
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
       
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}



-(IBAction)imageButton
{

    [_editfirstName resignFirstResponder];
    [_editlastname resignFirstResponder];
    [_editaddress1 resignFirstResponder];
    [_editaddress2 resignFirstResponder];
    [_editcity resignFirstResponder];
    [_editstate resignFirstResponder];
    [_editpayemailID resignFirstResponder];
    [_editphoneNo resignFirstResponder];
    [_editzipCode resignFirstResponder];
    
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
    
}


-(IBAction)cardNumberClick:(id)sender
{
    [self animateTextField: _cardNumber up: YES];
}

-(IBAction)cardNumberClick1:(id)sender
{
    [self animateTextField: _cardNumber up: NO];
}


-(void)keyboardHide
{
    [_bankcode resignFirstResponder];
    [_accountName resignFirstResponder];
    [_accountNumber resignFirstResponder];
    [_cardName resignFirstResponder];
    [_cardNumber resignFirstResponder];
    [_cardExpiry resignFirstResponder];
    [_cardExpiryYear resignFirstResponder];
}


- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    if (!IS_IPHONE_5)
    {
        if(textField ==_accountNumber || textField == _cardNumber || textField ==_cardExpiry || textField == _cardExpiryYear|| textField == _bankcode)
        {
            [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
    }
    
    [textField resignFirstResponder];
    
    return YES;
    
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
    if (!IS_IPHONE_5)
    {
        
        if(textField ==_accountNumber || textField == _cardNumber)
        {
            [_scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
        }
        else if (textField ==_cardExpiry || textField == _cardExpiryYear|| textField == _bankcode)
        {
            [_scrollView setContentOffset:CGPointMake(0, 80) animated:YES];
        }
    }
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}


-(IBAction)friends
{
    if (IS_IPHONE_5)
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)business
{
    if (IS_IPHONE_5)
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)room
{
    if (IS_IPHONE_5)
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)setttings
{
    if (IS_IPHONE_5)
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)more
{
    if (IS_IPHONE_5)
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}


////// FACEBOOK END //
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (buttonIndex==0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
        }
        
    }
    else  if (buttonIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
    
}


-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    NSData *imagedata = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage" ],1);
    UIImage *image = [[UIImage alloc] initWithData:imagedata];
    
    
    //  image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(240, 240)];
    
    
    float ori_height=image.size.height;
    float ori_width=image.size.width;
    
    NSLog(@"H:%f|W:%f",ori_height,ori_width);
    
    
    float height_ratio1=ori_height/4;
    float width_ratio1=ori_width/4;
    
    NSLog(@"HJ:%f|WJ:%f",height_ratio1,width_ratio1);
    
    
    
    image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(width_ratio1, height_ratio1)];
    
    
    
    _imv.image=image;
   
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postProImgfile",@"http://api.socialnetgateapp.com/v1"]];
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    //[request_post setFile:@"/Users/apple/Desktop/Free-Grammar-Ebook-Level-2.pdf" forKey:@"filename"];
    
    //UIImage * ourImage = [UIImage imageNamed:@"rajini-bday2.jpg"];
    NSData * ourImageData = UIImageJPEGRepresentation(image, 100);
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:ss zzz"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    //////(@"DATE STRING:%@",dateString);
    
    NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
    NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSString *imageName=[NSString stringWithFormat:@"%@.jpg",newString3];
    
    
    
    [request_post addData:ourImageData withFileName:imageName andContentType:@"image/jpeg" forKey:@"file"];
    
    [request_post setTimeOutSeconds:60];
    
    
    [request_post setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request_post responseString];
        
        
        //////(@"RES:%@",responseString);
        
        NSMutableData *results11 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *temp= [results11 valueForKeyPath:@"photos.profile_img_url"];
        
        
        NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
        
        [imgtemp setObject:temp forKey:@"USER_IMAGE11"];
        
        
        
        
                
        // Use when fetching binary data
        // NSData *responseData = [request_post responseData];
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        
        
        
        
    }];
    [request_post startAsynchronous];
    
    
    [picker dismissModalViewControllerAnimated:YES];
    
    
    
    
}
-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissModalViewControllerAnimated:YES];
}



@end

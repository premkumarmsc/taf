//
//  ViewController.h
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaypalConfirmViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate,UIActionSheetDelegate>
{
    
    
}
@property(retain,nonatomic)IBOutlet UICollectionView *collectionView;
@property(retain,nonatomic)IBOutlet UIView *addContactView;


@property(nonatomic,retain)IBOutlet UIImageView *fromImgView;
@property(nonatomic,retain)IBOutlet UIImageView *toImgView;
@property(nonatomic,retain)IBOutlet UIImageView *merchentImgView;
@property(nonatomic,retain)IBOutlet UIImageView *QRImgView;
@property(nonatomic,retain)IBOutlet UIImageView *QRStatusImgView;

@property(nonatomic,retain)IBOutlet UIImageView *exBackground;


@property(nonatomic,retain)IBOutlet UILabel *fromName;
@property(nonatomic,retain)IBOutlet UILabel *toName;
@property(nonatomic,retain)IBOutlet UILabel *MerchentName;
@property(nonatomic,retain)IBOutlet UILabel *MerchentAddress;
@property(nonatomic,retain)IBOutlet UILabel *ExpiredLabel;
@property(nonatomic,retain)IBOutlet UILabel *ShortURLLabel;
@property(nonatomic,retain)IBOutlet UILabel *AmountLabel;

@property(nonatomic,retain)IBOutlet UITextField *paypalEmailField;


@property (weak, nonatomic) IBOutlet UITextField *editaddress1;
@property (weak, nonatomic) IBOutlet UITextField *editaddress2;
@property (weak, nonatomic) IBOutlet UITextField *editzipCode;
@property (weak, nonatomic) IBOutlet UITextField *editfirstName;
@property (weak, nonatomic) IBOutlet UITextField *editlastname;
@property (weak, nonatomic) IBOutlet UITextField *editcity;
@property (weak, nonatomic) IBOutlet UITextField *editstate;
@property (weak, nonatomic) IBOutlet UIButton *editcountrybtn;
@property (weak, nonatomic) IBOutlet UIView  *contactEditView;
@property (weak, nonatomic) IBOutlet UITextField *editemailID;
@property (weak, nonatomic) IBOutlet UITextField *editphoneNo;

@property (weak, nonatomic) IBOutlet UIButton *editimageButton;
@property (weak, nonatomic) IBOutlet UIButton *editcountryButton;
@property (weak, nonatomic) IBOutlet UIButton *paypalButton;
-(IBAction)paypalClick;


@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UITextField *address1;
@property (weak, nonatomic) IBOutlet UITextField *address2;
@property (weak, nonatomic) IBOutlet UITextField *zipCode;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastname;
@property (weak, nonatomic) IBOutlet UITextField *city;
@property (weak, nonatomic) IBOutlet UITextField *state;
@property (weak, nonatomic) IBOutlet UIButton *countrybtn;
@property (weak, nonatomic) IBOutlet UIView  *contactAddView;
@property (weak, nonatomic) IBOutlet UITextField *emailID;
@property (weak, nonatomic) IBOutlet UITextField *phoneNo;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property(nonatomic,retain)IBOutlet UIImageView *emailImgView;
-(IBAction)searchBtn;
-(IBAction)addContact;
-(IBAction)addBack;
-(IBAction)addSave;
-(IBAction)editSave;
-(IBAction)addContry;
-(IBAction)editContry;
-(IBAction)addImage;
-(IBAction)editImage;
-(IBAction)editButtonClick;

-(IBAction)friends;
-(IBAction)business;
-(IBAction)room;
-(IBAction)setttings;
-(IBAction)more;


-(IBAction)createPaypalID;
-(IBAction)usedExistingPaypal;



@end

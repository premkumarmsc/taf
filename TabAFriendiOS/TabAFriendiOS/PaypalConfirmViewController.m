//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "PaypalConfirmViewController.h"
#import "receiptentCell.h"
#import "UIImageView+WebCache.h"

@interface PaypalConfirmViewController ()

@end

@implementation PaypalConfirmViewController
@synthesize collectionView;
receiptentCell *cell;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *contactIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;


NSString *contactIDName;






int editTag;

-(IBAction)paypalClick
{
    
   
}
- (void)viewDidLoad
{
    
    
    NSUserDefaults *ch=[NSUserDefaults standardUserDefaults];
    
    
    
    NSString *conStatus=[ch objectForKey:@"CONVERTER"];
    
    
    
    if ([conStatus isEqualToString:@"ON"])
    {
        _paypalButton.hidden=NO;
    }
    else
    {
        _paypalButton.hidden=YES;
    }

    [self getUserDetails];
    
    [self getTapDetails];
}

-(void)getTapDetails
{
    
        
        
       
        
        
        
        NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
        
        NSString *userID=[checkval objectForKey:@"USERID"];
         NSString *tapID=[checkval objectForKey:@"LAST_TAPPED_ID"];
        
        // userID=@"230";
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/tap/retrieveTaps",CONFIG_BASE_URL]];
        
        NSLog(@"HELLO:%@",url);
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:userID forKey:@"userID"];
        [request_post setPostValue:tapID forKey:@"tapID"];
        
        
        [request_post setTimeOutSeconds:30];
        
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            
            
            
            
            
            
            
            
            NSMutableData *results1 = [responseString JSONValue];
            
            
            
            NSLog(@"RESULTS HELL:%@",results1);
            
            
            
            
            NSArray *temp= [results1 valueForKeyPath:@"Response.tapDetails"];
            
            
            
            
            for(NSDictionary *value in temp)
            {
                
                
                
                NSString *tapAmountArr=[value valueForKey:@"tap_amount"];
                 NSString *fromFName=[value valueForKey:@"senderFirstName"];
                 NSString *fromLName=[value valueForKey:@"senderLastName"];
                 NSString *fromImage=[value valueForKey:@"senderImage"];
                 NSString *fromID=[value valueForKey:@"fk_sender_id"];
                
                NSString *recFName=[value valueForKey:@"receiverFirstName"];
                NSString *recLName=[value valueForKey:@"receiverLastName"];
                NSString *recImage=[value valueForKey:@"receiverImage"];
                NSString *recID=[value valueForKey:@"fk_receiver_id"];
                
                
                NSString *merFName=[value valueForKey:@"merchantFirstName"];
                NSString *merLName=[value valueForKey:@"merchantLastName"];
                NSString *merImage=[value valueForKey:@"merchantImage"];
                NSString *merID=[value valueForKey:@"fk_merchant_id"];
                NSString *merAd1=[value valueForKey:@"merchantAddress1"];
                NSString *merAd2=[value valueForKey:@"merchantAddress2"];
                NSString *meeCity=[value valueForKey:@"merchantCity"];
                NSString *merState=[value valueForKey:@"merchantState"];
               
                 NSString *merZip=[value valueForKey:@"merchantZipcode"];
                
                
                 NSString *expireDate=[value valueForKey:@"expire_date"];
                 NSString *merLAt=[value valueForKey:@"merchantLat"];
                 NSString *merLong=[value valueForKey:@"merchantLong"];
                 NSString *shortURL=[value valueForKey:@"merchantZipcode"];
                 NSString *qrImage=[value valueForKey:@"qr_code_image"];
                 NSString *status=[value valueForKey:@"tap_status"];
                
                
                NSLog(@"TAP_AMOUNT:%@",tapAmountArr);
                              
                _AmountLabel.text=[NSString stringWithFormat:@"%@",tapAmountArr];
                _fromName.text=[NSString stringWithFormat:@"%@ %@",fromFName,fromLName];
                @try {
                   
                    [_fromImgView setImageWithURL:[NSURL URLWithString:fromImage]
                                    placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                }
                
                _toName.text=[NSString stringWithFormat:@"%@ %@",recFName,recLName];
                @try {
                    
                    [_toImgView setImageWithURL:[NSURL URLWithString:recImage]
                                 placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                }
                
                _MerchentName.text=[NSString stringWithFormat:@"%@ %@",merFName,merLName];
                @try {
                    
                    [_merchentImgView setImageWithURL:[NSURL URLWithString:merImage]
                                 placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                }
                
                 _MerchentAddress.text=[NSString stringWithFormat:@"%@,%@\n%@,%@\n%@",merAd1,merAd2,meeCity,merState,merZip];
                
                
                @try {
                    
                    [_QRImgView setImageWithURL:[NSURL URLWithString:qrImage]
                                     placeholderImage:[UIImage imageNamed:@"Qrcode.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                    
                    _QRImgView.image=[UIImage imageNamed:@"Qrcode.png"];
                }
                
                
                
                NSUserDefaults *addLat=[NSUserDefaults standardUserDefaults];
                                
                NSString *locLat1  = [NSString stringWithFormat:@"%@", merLAt];
                NSString * locLong1 = [NSString stringWithFormat:@"%@", merLong];
                
                [addLat setObject:locLat1 forKey:@"TO_LAT"];
                [addLat setObject:locLong1 forKey:@"TO_LONG"];
                
                
                
                
                
                
                
                NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
                
                [dateFormat1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                 
                
                NSDate *someDate = [dateFormat1 dateFromString:expireDate];
                
                
                
                
                if ([status isEqualToString:@"valid"])
                {
                    if ([expireDate isEqualToString:@"0000-00-00 00:00:00"])
                    {
                        _ExpiredLabel.text=@"";
                       
                        _exBackground.hidden=YES;
                        _QRStatusImgView.image=[UIImage imageNamed:@"expired-icon.png"];
                    }
                    else
                    {
                        if ([someDate timeIntervalSinceNow] < 0.0) {
                            // Date has passed
                            _ExpiredLabel.text=@"";
                            _exBackground.hidden=YES;
                            _QRStatusImgView.image=[UIImage imageNamed:@"expired-icon.png"];
                        }
                        else
                        {
                             NSDate *now = [NSDate date];
                           
                            NSCalendar *calendar = [NSCalendar currentCalendar];
                            
                            NSDate *fromDate;
                            NSDate *toDate;
                            
                            
                            [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                                         interval:NULL forDate:now];
                            [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                                         interval:NULL forDate:someDate];
                            
                            NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                                                       fromDate:fromDate toDate:toDate options:0];
                            
                            NSLog(@"DAYYYY:%ld",(long)[difference day]);
                            
                            if ((long)[difference day]==0) {
                                 _ExpiredLabel.text=[NSString stringWithFormat:@"EXPIRES IN TODAY"];
                            }
                            else
                            {
                            
                            _ExpiredLabel.text=[NSString stringWithFormat:@"EXPIRES IN %ld DAY(S)",(long)[difference day]];
                            }
                        }
                    }
                }
                else
                {
                    if ([status isEqualToString:@"expired"]||[expireDate isEqualToString:@"0000-00-00 00:00:00"])
                    {
                        _ExpiredLabel.text=@"";
                        _exBackground.hidden=YES;
                        _QRStatusImgView.image=[UIImage imageNamed:@"expired-icon.png"];
                    }
                   
                
                     if ([status isEqualToString:@"used"])
                     {
                        _QRStatusImgView.image=[UIImage imageNamed:@"used.png"];
                         _exBackground.hidden=YES;
                          _ExpiredLabel.text=@"";
                     }
                }
        
                
            }
            
            //[_tblView reloadData];
            
            
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];
 
}
-(IBAction)createPaypalID
{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.paypal.com"]];
}
-(IBAction)usedExistingPaypal
{
    
}
-(IBAction)addBack
{
    [self dismissViewControllerAnimated:NO completion:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
-(IBAction)Direction
{
    if (IS_IPHONE_5)
    {
        LocationViewController *signupView=[[LocationViewController alloc]initWithNibName:@"LocationViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        LocationViewController *signupView=[[LocationViewController alloc]initWithNibName:@"LocationViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
   
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}

-(IBAction)addSave
{
    NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSString *subjectString =_paypalEmailField.text;
    
 
            if((_paypalEmailField.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
            {
                
                //  NSLog(@"NOT VALID");
                _emailImgView.image=[UIImage imageNamed:@"sr.png"];
                
                
            }
            else
            {
                if ((_paypalEmailField.text.length==0))
                {
                    _emailImgView.image=[UIImage imageNamed:@"si.png"];
                }
                else
                {
                    
                    _emailImgView.image=[UIImage imageNamed:@"sg.png"];
                    
                    
                    [self editcontectFun:_paypalEmailField.text];
                    
                }
            }
          
}

-(void)editcontectFun:(NSString *)status
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/editUserProfile",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    
    [request_post1234 setPostValue:status forKey:@"paypal_mailid"];
    
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}

- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    
    
    return YES;
    
    
}

-(void)getUserDetails
{
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    NSString *fbID=[checkval objectForKey:@"FB_USERID"];
    NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
    
    // userID=@"230";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserDetails",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
    
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        NSArray *pk_id=[results1 valueForKeyPath:@"Response.user.pk_id"];
        NSArray *fk_facebook_user_id=[results1 valueForKeyPath:@"Response.user.fk_facebook_user_id"];
        NSArray *fk_merchant_package_id=[results1 valueForKeyPath:@"Response.user.fk_merchant_package_id"];
        NSArray *first_name=[results1 valueForKeyPath:@"Response.user.first_name"];
        NSArray *last_name=[results1 valueForKeyPath:@"Response.user.last_name"];
        NSArray *email=[results1 valueForKeyPath:@"Response.user.email"];
        NSArray *password=[results1 valueForKeyPath:@"Response.user.password"];
        NSArray *signup_type=[results1 valueForKeyPath:@"Response.user.signup_type"];
        NSArray *facebook_access_token=[results1 valueForKeyPath:@"Response.user.facebook_access_token"];
        NSArray *image=[results1 valueForKeyPath:@"Response.user.image"];
        NSArray *address1=[results1 valueForKeyPath:@"Response.user.address1"];
        NSArray *address2=[results1 valueForKeyPath:@"Response.user.address2"];
        NSArray *city=[results1 valueForKeyPath:@"Response.user.city"];
        NSArray *state=[results1 valueForKeyPath:@"Response.user.state"];
        NSArray *zipcode=[results1 valueForKeyPath:@"Response.user.zipcode"];
        NSArray *country=[results1 valueForKeyPath:@"Response.user.country"];
        NSArray *phone=[results1 valueForKeyPath:@"Response.user.phone"];
        NSArray *user_security_pin=[results1 valueForKeyPath:@"Response.user.user_security_pin"];
        NSArray *user_type=[results1 valueForKeyPath:@"Response.user.user_type"];
        NSArray *location_latitude=[results1 valueForKeyPath:@"Response.user.location_latitude"];
        NSArray *location_longitude=[results1 valueForKeyPath:@"Response.user.location_longitude"];
        NSArray *created_date=[results1 valueForKeyPath:@"Response.user.created_date"];
        NSArray *created_ipaddress=[results1 valueForKeyPath:@"Response.user.created_ipaddress"];
        NSArray *updated_date=[results1 valueForKeyPath:@"Response.user.updated_date"];
        NSArray *updated_ipaddress=[results1 valueForKeyPath:@"Response.user.updated_ipaddress"];
        NSArray *browser_details=[results1 valueForKeyPath:@"Response.user.browser_details"];
        NSArray *user_status=[results1 valueForKeyPath:@"Response.user.user_status"];
        NSArray *merchant_company_details=[results1 valueForKeyPath:@"Response.user.merchant_company_details"];
        
        
        
        NSArray *payemail=[results1 valueForKeyPath:@"Response.user.paypal_mailid"];
        NSArray *fbshare=[results1 valueForKeyPath:@"Response.user.fb_share"];
        
        
        NSLog(@"EMAIl:%@",email);
        
        _paypalEmailField.text=payemail[0];
        
        
        
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    
}

-(IBAction)friends
{
    if (IS_IPHONE_5)
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)business
{
    if (IS_IPHONE_5)
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)room
{
    if (IS_IPHONE_5)
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)setttings
{
    if (IS_IPHONE_5)
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)more
{
    if (IS_IPHONE_5)
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}




@end

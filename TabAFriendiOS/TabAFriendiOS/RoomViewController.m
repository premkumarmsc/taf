//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "RoomViewController.h"
#import "CatCell.h"
#import "UIImageView+WebCache.h"
#import "TapCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ReflectionView.h"
@interface RoomViewController ()

@end

@implementation RoomViewController
@synthesize collectionView;
@synthesize carousel;
CatCell *cell;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;


NSMutableArray *merimageThumpArray;
NSMutableArray *merNameThumpArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;

NSMutableArray *senderFNameArr;
NSMutableArray *senderImageArr;
NSMutableArray *ReceiverFNameArr;
NSMutableArray *ReceiverImageArr;

NSMutableArray *senderLNameArr;

NSMutableArray *ReceiverLNameArr;


NSString *contactIDName;

NSMutableArray *tapIDArray;
NSMutableArray *tapAmountArray;

NSMutableArray *tapStatusArray;
//NSMutableArray *expireDateArray;




- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //generate 100 item views
    //normally we'd use a backing array
    //as shown in the basic iOS example
    //but for this example we haven't bothered
    return [tapIDArray count];;
}
-(void)updatebuttonClicked:(UIButton*)button
{
    
       
   
    
   // NSLog(@"Contact ID:%ld",  (long int)[button tag]);
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    
  
    NSString *getidStr=[check objectForKey:@"LAST_INDEX_ID"];
   
    
    int tagid=[getidStr intValue];
    
    
    [check setObject:tapIDArray[tagid] forKey:@"LAST_TAPPED_ID"];
    
    
    NSLog(@"TAG ID:%d",tagid);
    
    
    if (IS_IPHONE_5)
    {
        TapRoomViewController *signupView=[[TapRoomViewController alloc]initWithNibName:@"TapRoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        TapRoomViewController *signupView=[[TapRoomViewController alloc]initWithNibName:@"TapRoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }

    

}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(ReflectionView *)view
{
    
    UILabel *label = nil;
    UILabel *tapLabel=nil;
    UILabel *sentlabel=nil;
    UILabel *receivelabel=nil;
    UIImageView *recImg=nil;
    UIImageView *senImg=nil;
    
    UIImageView *statusImg=nil;
    
    UIImageView *merImg=nil;
    
    
    UIView *viewCheck=nil;
    UIButton *clickButton=nil;
    
    
    
    UILabel *merlabel=nil;
   
    
    
    
    //NSLog(@"HELLO:%d",index);
	
	//create new view if no view is available for recycling
	if (view == nil)
	{
        //set up reflection view
        
        
        
        
		view = [[[ReflectionView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 200.0f, 200.0f)] autorelease];
        
        //set up content
        
        
        viewCheck=[[[UIView alloc]initWithFrame:view.bounds] autorelease];
        viewCheck.backgroundColor = [UIColor lightGrayColor];
		viewCheck.layer.borderColor = [UIColor whiteColor].CGColor;
        viewCheck.layer.borderWidth = 4.0f;
        viewCheck.layer.cornerRadius = 8.0f;
          viewCheck.tag = 10001;

        [view addSubview:viewCheck];
        
        /*
         label = [[[UILabel alloc] initWithFrame:view.bounds] autorelease];
         label.backgroundColor = [UIColor lightGrayColor];
         label.layer.borderColor = [UIColor whiteColor].CGColor;
         label.layer.borderWidth = 4.0f;
         label.layer.cornerRadius = 8.0f;
         label.textAlignment = UITextAlignmentCenter;
         label.font = [label.font fontWithSize:50];
         label.tag = 9999;
         */
        
        
        merImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 135, 55, 55)];
        merImg.image=[UIImage imageNamed:@"rajini-bday2.jpg"];
        merImg.contentMode=UIViewContentModeScaleAspectFill;
        merImg.backgroundColor=[UIColor whiteColor];
        merImg.tag = 10011;
        [viewCheck addSubview:merImg];
        
        CALayer *imageLayer11 =merImg.layer;
        [imageLayer11 setCornerRadius:25];
        [imageLayer11 setBorderWidth:0];
        [imageLayer11 setMasksToBounds:YES];
        
        
       senImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 55, 55)];
        senImg.image=[UIImage imageNamed:@"rajini-bday2.jpg"];
        senImg.contentMode=UIViewContentModeScaleAspectFill;
        senImg.backgroundColor=[UIColor whiteColor];
        senImg.tag = 10000;
        [viewCheck addSubview:senImg];
        
        CALayer *imageLayer =senImg.layer;
        [imageLayer setCornerRadius:25];
        [imageLayer setBorderWidth:0];
        [imageLayer setMasksToBounds:YES];
        
        
        sentlabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 55, 100, 40)];
	    sentlabel.text=@"Rajini Kanth";
        sentlabel.backgroundColor=[UIColor clearColor];
        //sentlabel.font = [view.textLabel.font fontWithSize:12];
        [sentlabel setFont:[UIFont fontWithName:@"Futura-Medium" size:12.0f]];
        sentlabel.tag = 9998;
		[viewCheck addSubview:sentlabel];
        
        
        merlabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 140, 120, 40)];
	    merlabel.text=@"Rajini Kanth";
        merlabel.backgroundColor=[UIColor clearColor];
        merlabel.numberOfLines=2;
        //sentlabel.font = [view.textLabel.font fontWithSize:12];
        [merlabel setFont:[UIFont fontWithName:@"Futura-Medium" size:15.0f]];
        merlabel.tag = 9988;
		[viewCheck addSubview:merlabel];
        
        
        
        recImg=[[UIImageView alloc]initWithFrame:CGRectMake(135, 10, 55, 55)];
        recImg.image=[UIImage imageNamed:@"rajini-bday2.jpg"];
        recImg.contentMode=UIViewContentModeScaleAspectFill;
        recImg.backgroundColor=[UIColor whiteColor];
        recImg.tag = 11111;
        [viewCheck addSubview:recImg];
        
        
        statusImg=[[UIImageView alloc]initWithFrame:CGRectMake(75, 92, 50, 40)];
        statusImg.image=[UIImage imageNamed:@"rajini-bday2.jpg"];
        statusImg.contentMode=UIViewContentModeScaleAspectFit;
        statusImg.backgroundColor=[UIColor clearColor];
        statusImg.tag = 10012;
        [viewCheck addSubview:statusImg];
        
        
        
        CALayer *imageLayerrecImg =recImg.layer;
        [imageLayerrecImg setCornerRadius:25];
        [imageLayerrecImg setBorderWidth:0];
        [imageLayerrecImg setMasksToBounds:YES];
        
        
        receivelabel = [[UILabel alloc] initWithFrame:CGRectMake(95, 55, 100, 40)];
	    receivelabel.text=@"Rajini Kanth";
        receivelabel.backgroundColor=[UIColor clearColor];
        receivelabel.textAlignment = UITextAlignmentRight;
        [receivelabel setFont:[UIFont fontWithName:@"Futura-Medium" size:12.0f]];
        receivelabel.tag = 9997;
		[viewCheck addSubview:receivelabel];
        
        
        UIImageView *centerImage=[[UIImageView alloc]initWithFrame:CGRectMake(87.5, 13, 25, 25)];//(85, 25, 25, 25)
        centerImage.image=[UIImage imageNamed:@"taplogoicon.png"];
        centerImage.contentMode=UIViewContentModeScaleAspectFit;
        [viewCheck addSubview:centerImage];
        
        
//        UIImageView *statusImage=[[UIImageView alloc]initWithFrame:CGRectMake(85, 100, 25, 25)];
//        statusImage.image=[UIImage imageNamed:@"taplogoicon.png"];
//        statusImage.contentMode=UIViewContentModeScaleAspectFit;
//        [viewCheck addSubview:statusImage];
        
        
        
        tapLabel = [[UILabel alloc] initWithFrame:CGRectMake(66, 35, 70, 30)];//(66, 92, 70, 40)
	   
        tapLabel.backgroundColor=[UIColor clearColor];
        tapLabel.textAlignment = NSTextAlignmentCenter;
        tapLabel.textColor=[UIColor whiteColor];
        [tapLabel setFont:[UIFont fontWithName:@"Futura-Medium" size:16.0f]];
        tapLabel.tag = 9999;
		[viewCheck addSubview:tapLabel];
        
        
        clickButton=[[UIButton alloc] initWithFrame:CGRectMake(90.0f, 85.0f, 50.0f, 50.0f)] ;
        [clickButton setTag:index];
        [clickButton addTarget:self action:@selector(updatebuttonClicked:)
              forControlEvents:UIControlEventTouchDown];
        [viewCheck addSubview:clickButton];
        
	}
	else
	{
		tapLabel = (UILabel *)[view viewWithTag:9999];
        sentlabel = (UILabel *)[view viewWithTag:9998];
        receivelabel = (UILabel *)[view viewWithTag:9997];
         viewCheck = (UIImageView *)[view viewWithTag:10001];
        senImg = (UIImageView *)[view viewWithTag:10000];
        merImg = (UIImageView *)[view viewWithTag:10011];
        recImg = (UIImageView *)[view viewWithTag:11111];
        clickButton= (UIButton *)[view viewWithTag:index];
        merlabel = (UILabel *)[view viewWithTag:9988];
        
        statusImg = (UIImageView *)[view viewWithTag:10012];
	}
	
    //set label
	//label.text = [NSString stringWithFormat:@"%i", index];
    
//     tapLabel.text=[NSString stringWithFormat:@"$%@",tapAmountArray[index]];
     tapLabel.text=[NSString stringWithFormat:@"$%.2f",[tapAmountArray[index] floatValue]];
    sentlabel.text=[NSString stringWithFormat:@"%@ %@",senderFNameArr[index],senderLNameArr[index]];
    receivelabel.text=[NSString stringWithFormat:@"%@ %@",ReceiverFNameArr[index],ReceiverLNameArr[index]];
    
     merlabel.text=[NSString stringWithFormat:@"%@",merNameThumpArray[index]];
    
    @try {
        NSString *temp=[merimageThumpArray objectAtIndex:index];
        [merImg setImageWithURL:[NSURL URLWithString:temp]
               placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
        
        merImg.image=[UIImage imageNamed:@"vbigicon.png"];
    }

    
    
    @try {
        NSString *temp=[senderImageArr objectAtIndex:index];
        [senImg setImageWithURL:[NSURL URLWithString:temp]
               placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
        
        senImg.image=[UIImage imageNamed:@"vbigicon.png"];
    }

    @try {
        NSString *temp=[ReceiverImageArr objectAtIndex:index];
        [recImg setImageWithURL:[NSURL URLWithString:temp]
               placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }
    
    NSString * status =[NSString stringWithFormat:@"%@",[tapStatusArray objectAtIndex:index]];
//    NSString * expireDate =[NSString stringWithFormat:@"%@",[expireDateArray objectAtIndex:index]];
    
    if ([status isEqualToString:@"valid"])
    {
        statusImg.image=[UIImage imageNamed:@"valid.png"];
    }
    else if ([status isEqualToString:@"used"])
    {
        statusImg.image=[UIImage imageNamed:@"used.png"];
    }
    else if ([status isEqualToString:@"converted_to_cash_success"])
    {
        statusImg.image=[UIImage imageNamed:@"converted_orange.png"];
    }
    else if ([status isEqualToString:@"expired"])
    {
        statusImg.image=[UIImage imageNamed:@"expired-icon.png"];
    }
    
    //update reflection
    //this step is expensive, so if you don't need
    //unique reflections for each item, don't do this
    //and you'll get much smoother peformance
    [view update];
	
	return view;
    
    
    /*
	UILabel *label = nil;
    UIView *viewCheck=nil;
    UIImageView *senImg=nil;
    UILabel *sentlabel=nil;
    
    
    NSLog(@"HELLO:%d",index);
	
	//create new view if no view is available for recycling
	if (view == nil)
	{
        //set up reflection view
        
        
        
        
		view = [[ReflectionView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 200.0f, 200.0f)] ;
        
        //set up content
        
        
        viewCheck=[[[UIView alloc]initWithFrame:view.bounds] autorelease];
        viewCheck.backgroundColor = [UIColor lightGrayColor];
		viewCheck.layer.borderColor = [UIColor whiteColor].CGColor;
        viewCheck.layer.borderWidth = 4.0f;
        viewCheck.layer.cornerRadius = 8.0f;
        viewCheck.tag=9999;
        [view addSubview:viewCheck];
        
        
        
        
        
        
        UIButton *clickButton=[[UIButton alloc] initWithFrame:CGRectMake(90.0f, 85.0f, 50.0f, 50.0f)] ;
        [clickButton setTag:index];
        [clickButton addTarget:self action:@selector(updatebuttonClicked:)
               forControlEvents:UIControlEventTouchDown];
        [viewCheck addSubview:clickButton];
        
        
              
              
               
        
        
             
       
        
        senImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 55, 55)];
        //senImg.image=[UIImage imageNamed:@"rajini-bday2.jpg"];
        senImg.contentMode=UIViewContentModeScaleAspectFit;
         senImg.tag=9999;
        senImg.backgroundColor=[UIColor whiteColor];
        
        
        @try {
            NSString *temp=[senderImageArr objectAtIndex:index];
            [senImg setImageWithURL:[NSURL URLWithString:temp]
                   placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
            
            
            
        }
        @catch (NSException *exception) {
            //////////////(@"CHJECK");
        }

        
        
        [viewCheck addSubview:senImg];
        
        CALayer *imageLayer =senImg.layer;
        [imageLayer setCornerRadius:25];
        [imageLayer setBorderWidth:0];
        [imageLayer setMasksToBounds:YES];
        
        
        sentlabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 55, 200, 40)];
	    sentlabel.text=[NSString stringWithFormat:@"%@ %@",senderFNameArr[index],senderLNameArr[index]];
        sentlabel.backgroundColor=[UIColor clearColor];
        sentlabel.tag=9999;
         sentlabel.textColor=[UIColor grayColor];
                
        [sentlabel setFont:[UIFont fontWithName:@"Futura-Medium" size:12.0f]];
		[viewCheck addSubview:sentlabel];
               
        
        UIImageView *recImg=[[UIImageView alloc]initWithFrame:CGRectMake(130, 120, 55, 55)];
        //recImg.image=[UIImage imageNamed:@"rajini-bday2.jpg"];
        recImg.contentMode=UIViewContentModeScaleAspectFit;
        recImg.backgroundColor=[UIColor whiteColor];
        
        
        @try {
            NSString *temp=[ReceiverImageArr objectAtIndex:index];
            [recImg setImageWithURL:[NSURL URLWithString:temp]
                             placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
                }
        @catch (NSException *exception) {
            //////////////(@"CHJECK");
        }

        
        
        [viewCheck addSubview:recImg];
        
        CALayer *imageLayerrecImg =recImg.layer;
        [imageLayerrecImg setCornerRadius:25];
        [imageLayerrecImg setBorderWidth:0];
        [imageLayerrecImg setMasksToBounds:YES];
        
        
        UILabel *receivelabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 165, 175, 40)];
	    receivelabel.text=[NSString stringWithFormat:@"%@ %@",ReceiverFNameArr[index],ReceiverLNameArr[index]];
        receivelabel.backgroundColor=[UIColor clearColor];
        receivelabel.textAlignment = UITextAlignmentRight;
         receivelabel.textColor=[UIColor grayColor];
        [receivelabel setFont:[UIFont fontWithName:@"Futura-Medium" size:12.0f]];
		[viewCheck addSubview:receivelabel];
        
        
        UIImageView *centerImage=[[UIImageView alloc]initWithFrame:CGRectMake(90, 85, 25, 25)];
        centerImage.image=[UIImage imageNamed:@"taplogoicon.png"];
        centerImage.contentMode=UIViewContentModeScaleAspectFit;
        [viewCheck addSubview:centerImage];
        
        
        
        UILabel *tapLabel = [[UILabel alloc] initWithFrame:CGRectMake(66, 100, 70, 40)];
	    tapLabel.text=[NSString stringWithFormat:@"$%@",tapAmountArray[index]];
        tapLabel.backgroundColor=[UIColor clearColor];
        tapLabel.textAlignment = UITextAlignmentCenter;
        [tapLabel setFont:[UIFont fontWithName:@"Futura-Medium" size:18.0f]];
		[viewCheck addSubview:tapLabel];
        
        
        
        
	}
	else
	{
		//label = (UILabel *)[view viewWithTag:9999];
        // senImg=(UIImageView *)[view viewWithTag:9999];
       // viewCheck=(UIView *)[view viewWithTag:9999];
        //sentlabel=(UILabel *)[view viewWithTag:9999];
	}
	
    //set label
   
	sentlabel.text = [NSString stringWithFormat:@"%i", index];
    
  
    [view update];
	
	return view;
     */
}




-(IBAction)addBack
{
    [self business];
}


int editTag;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:_addContactView];
    _addContactView.hidden=YES;
    [self.view addSubview:_contactEditView];
    _contactEditView.hidden=YES;
    
    NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
    
    [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
    
    
    [_editButton setTitle:@"Edit" forState:UIControlStateNormal];
    editTag=0;
  
    
    carousel.type = iCarouselTypeCoverFlow2;
    
    
    [self sentTaps];
  
}

-(IBAction)pickOne:(id)sender
{
    if (_segment.selectedSegmentIndex==0)
    {
        [self sentTaps];
    }
    else
    {
       [self receivedTaps];
    }
}

-(void)receivedTaps
{
    
    
    tapStatusArray=[[NSMutableArray alloc]init];
    //expireDateArray =[[NSMutableArray alloc]init];
    
    tapIDArray=[[NSMutableArray alloc]init];
    tapAmountArray=[[NSMutableArray alloc]init];
    
    senderImageArr=[[NSMutableArray alloc]init];
    senderFNameArr=[[NSMutableArray alloc]init];
    
    ReceiverImageArr=[[NSMutableArray alloc]init];
    ReceiverFNameArr=[[NSMutableArray alloc]init];
    
    senderLNameArr=[[NSMutableArray alloc]init];
    ReceiverLNameArr=[[NSMutableArray alloc]init];
    
    
    merNameThumpArray=[[NSMutableArray alloc]init];
    merimageThumpArray=[[NSMutableArray alloc]init];
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    
    
    // userID=@"230";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/tap/retrieveReceivedTaps",CONFIG_BASE_URL]];
    
    //NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
    
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        
        
        
        NSArray *temp= [results1 valueForKeyPath:@"Response.tapDetails"];
        
        
        
        
        for(NSDictionary *value in temp)
        {
            
            @try {
                
                [tapStatusArray  addObject:[value valueForKey:@"tap_status"]];
                //[expireDateArray  addObject:[value valueForKey:@"expire_date"]];
                
                [tapIDArray  addObject:[value valueForKey:@"pk_id"]];
                [tapAmountArray  addObject:[value valueForKey:@"tap_amount"]];
                
                
                [ReceiverImageArr  addObject:[value valueForKey:@"receiverImage"]];
                [senderImageArr  addObject:[value valueForKey:@"senderImage"]];
                
                [senderLNameArr  addObject:[value valueForKey:@"senderLastName"]];
                [senderFNameArr  addObject:[value valueForKey:@"senderFirstName"]];
                
                [ReceiverFNameArr  addObject:[value valueForKey:@"receiverFirstName"]];
                [ReceiverLNameArr  addObject:[value valueForKey:@"receiverLastName"]];
                
                [merimageThumpArray  addObject:[value valueForKey:@"merchantImage"]];
                [merNameThumpArray  addObject:[value valueForKey:@"merchantFirstName"]];
 
            }
            @catch (NSException *exception) {
                
            }
            
                    

        }
        
        
        if ([senderFNameArr count]==0) {
            
            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You don't Received Taps from your friends" delegate:self
                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [loginalert show];
            
        }
        
        
        [_tblView reloadData];
          [carousel reloadData];
        
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    
}



-(void)sentTaps
{
    
    
    
    tapStatusArray=[[NSMutableArray alloc]init];
    //expireDateArray=[[NSMutableArray alloc]init];
    
    tapIDArray=[[NSMutableArray alloc]init];
    tapAmountArray=[[NSMutableArray alloc]init];
    
    senderImageArr=[[NSMutableArray alloc]init];
    senderFNameArr=[[NSMutableArray alloc]init];
    
    ReceiverImageArr=[[NSMutableArray alloc]init];
    ReceiverFNameArr=[[NSMutableArray alloc]init];
    
    senderLNameArr=[[NSMutableArray alloc]init];
    ReceiverLNameArr=[[NSMutableArray alloc]init];
    
    merNameThumpArray=[[NSMutableArray alloc]init];
    merimageThumpArray=[[NSMutableArray alloc]init];
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
   
    
    // userID=@"230";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/tap/retrieveSentTaps",CONFIG_BASE_URL]];
    
   // NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
   
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        
        
        
        NSArray *temp= [results1 valueForKeyPath:@"Response.tapDetails"];
        
        
        
        
        for(NSDictionary *value in temp)
        {
            
            @try {
                
                
                [tapStatusArray  addObject:[value valueForKey:@"tap_status"]];
                //[expireDateArray  addObject:[value valueForKey:@"expire_date"]];
                
                [tapIDArray  addObject:[value valueForKey:@"pk_id"]];
                [tapAmountArray  addObject:[value valueForKey:@"tap_amount"]];
                
                
                [ReceiverImageArr  addObject:[value valueForKey:@"receiverImage"]];
                [senderImageArr  addObject:[value valueForKey:@"senderImage"]];
                
                [senderLNameArr  addObject:[value valueForKey:@"senderLastName"]];
                [senderFNameArr  addObject:[value valueForKey:@"senderFirstName"]];
                
                [ReceiverFNameArr  addObject:[value valueForKey:@"receiverFirstName"]];
                [ReceiverLNameArr  addObject:[value valueForKey:@"receiverLastName"]];
                
                [merimageThumpArray  addObject:[value valueForKey:@"merchantImage"]];
                [merNameThumpArray  addObject:[value valueForKey:@"merchantFirstName"]];
            }
            @catch (NSException *exception)
            {
                
            }
          
          
        }
        if ([senderFNameArr count]==0)
        {
            
            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You don't Sent Taps to your friends" delegate:self
                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [loginalert show];
            
        }
        
        
        
        //[_tblView reloadData];
        [carousel reloadData];
        
        
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    
}

/*
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [tapIDArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    
    
    TapCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"TapCell" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (TapCell*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
     cell.amount.text=[NSString stringWithFormat:@"%@",tapAmountArray[indexPath.row]];
    
    
    @try {
        
        cell.from.text=[NSString stringWithFormat:@"%@ %@",senderFNameArr[indexPath.row],senderLNameArr[indexPath.row]];
        
        cell.to.text=[NSString stringWithFormat:@"%@ %@",ReceiverFNameArr[indexPath.row],ReceiverLNameArr[indexPath.row]];
    }
    @catch (NSException *exception) {
        
    }
   
    
    
    
    @try {
        NSString *temp=[senderImageArr objectAtIndex:indexPath.row];
        [cell.senderImg setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }

    @try {
        NSString *temp=[ReceiverImageArr objectAtIndex:indexPath.row];
        [cell.receiverImg setImageWithURL:[NSURL URLWithString:temp]
                       placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }

    
    
    CALayer *imageLayer = cell.senderImg.layer;
    [imageLayer setCornerRadius:25];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    
    
    CALayer *imageLayer1 = cell.receiverImg.layer;
    [imageLayer1 setCornerRadius:25];
    [imageLayer1 setBorderWidth:0];
    [imageLayer1 setMasksToBounds:YES];
    
      return cell;
    
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 65;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Select %d",indexPath.row);
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
     [check setObject:tapIDArray[indexPath.row] forKey:@"LAST_TAPPED_ID"];
    
    if (IS_IPHONE_5)
    {
        TapRoomViewController *signupView=[[TapRoomViewController alloc]initWithNibName:@"TapRoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        TapRoomViewController *signupView=[[TapRoomViewController alloc]initWithNibName:@"TapRoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }

}

*/



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    [textField resignFirstResponder];
    
      
    
    return YES;
    
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
   
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}
#pragma mark - LeveyPopListView delegates
- (void)leveyPopListView:(LeveyPopListView *)popListView didSelectedIndex:(NSInteger)anIndex
{
    NSLog(@"%@",[NSString stringWithFormat:@"You have selected %@", countryArray[anIndex]]);
}
- (void)leveyPopListViewDidCancel
{
    NSLog(@"CANCELLED");
}
////// FACEBOOK END //
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (buttonIndex==0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
        }
        
    }
    else  if (buttonIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
    
}




-(IBAction)friends
{
    if (IS_IPHONE_5)
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)business
{
    if (IS_IPHONE_5)
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)room
{
    if (IS_IPHONE_5)
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)setttings
{
    if (IS_IPHONE_5)
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)more
{
    if (IS_IPHONE_5)
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}




@end

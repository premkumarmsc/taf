//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "SettingsViewController.h"
#import "receiptentCell.h"
#import "UIImageView+WebCache.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
@synthesize collectionView;
receiptentCell *cell;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *contactIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;

UIAlertView *alertExit;
NSString *contactIDName;






int editTag;
-(IBAction)logout
{
    
    alertExit = [[UIAlertView alloc] initWithTitle:@"Logout"
                                           message:@"Do you want to really exit?" delegate:self cancelButtonTitle: @"No"
                                 otherButtonTitles:@"Yes", nil];
    
    //note above delegate property
    
    [alertExit show];
}
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //u need to change 0 to other value(,1,2,3) if u have more buttons.then u can check which button was pressed.
    
    if (alertView==alertExit)
    {
        if (buttonIndex == 0)
        {
            
            //(@"0");
            
            
        }
        else
        {
            // [self fbDidLogout];
            
            NSUserDefaults *checkSta=[NSUserDefaults standardUserDefaults];
            
            [checkSta setObject:@"YES" forKey:@"FIRSTTIME"];
            
            [FBSession.activeSession closeAndClearTokenInformation];
            
            NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
            
            //(@"DEVICE TOKEN 123:%@",[add objectForKey:@"DEVICE_TOKEN"]);
            
            NSString *device_token=[add objectForKey:@"DEVICE_TOKEN"];
            
            if([device_token length]==0)
            {
                device_token=@"";
                
                //(@"DEVICE TOKEN 789:%@",device_token);
            }
            else
            {
                
               
                
                
            }
            
            
            
            
            NSUserDefaults *add1=[NSUserDefaults standardUserDefaults];
            
            [add1 setObject:@"" forKey:@"USERID"];
            
           
            
            
            
            if (IS_IPHONE_5)
            {
                ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            else
            {
                ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController4" bundle:nil];
                [self presentViewController:viewController animated:NO completion:^{
                    //////(@"SUCCESS");
                }
                 ];
            }
            
            
            
        }
    }
    else
    {
        
        
    }
    
}
-(void)editcontectFun:(NSString *)status
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/editUserProfile",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    
    [request_post1234 setPostValue:status forKey:@"fb_share"];
    
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}
-(void)getUserDetails
{
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    NSString *fbID=[checkval objectForKey:@"FB_USERID"];
    NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
    
    // userID=@"230";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserDetails",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
    
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        NSArray *pk_id=[results1 valueForKeyPath:@"Response.user.pk_id"];
        NSArray *fk_facebook_user_id=[results1 valueForKeyPath:@"Response.user.fk_facebook_user_id"];
        NSArray *fk_merchant_package_id=[results1 valueForKeyPath:@"Response.user.fk_merchant_package_id"];
        NSArray *first_name=[results1 valueForKeyPath:@"Response.user.first_name"];
        NSArray *last_name=[results1 valueForKeyPath:@"Response.user.last_name"];
        NSArray *email=[results1 valueForKeyPath:@"Response.user.email"];
        NSArray *password=[results1 valueForKeyPath:@"Response.user.password"];
        NSArray *signup_type=[results1 valueForKeyPath:@"Response.user.signup_type"];
        NSArray *facebook_access_token=[results1 valueForKeyPath:@"Response.user.facebook_access_token"];
        NSArray *image=[results1 valueForKeyPath:@"Response.user.image"];
        NSArray *address1=[results1 valueForKeyPath:@"Response.user.address1"];
        NSArray *address2=[results1 valueForKeyPath:@"Response.user.address2"];
        NSArray *city=[results1 valueForKeyPath:@"Response.user.city"];
        NSArray *state=[results1 valueForKeyPath:@"Response.user.state"];
        NSArray *zipcode=[results1 valueForKeyPath:@"Response.user.zipcode"];
        NSArray *country=[results1 valueForKeyPath:@"Response.user.country"];
        NSArray *phone=[results1 valueForKeyPath:@"Response.user.phone"];
        NSArray *user_security_pin=[results1 valueForKeyPath:@"Response.user.user_security_pin"];
        NSArray *user_type=[results1 valueForKeyPath:@"Response.user.user_type"];
        NSArray *location_latitude=[results1 valueForKeyPath:@"Response.user.location_latitude"];
        NSArray *location_longitude=[results1 valueForKeyPath:@"Response.user.location_longitude"];
        NSArray *created_date=[results1 valueForKeyPath:@"Response.user.created_date"];
        NSArray *created_ipaddress=[results1 valueForKeyPath:@"Response.user.created_ipaddress"];
        NSArray *updated_date=[results1 valueForKeyPath:@"Response.user.updated_date"];
        NSArray *updated_ipaddress=[results1 valueForKeyPath:@"Response.user.updated_ipaddress"];
        NSArray *browser_details=[results1 valueForKeyPath:@"Response.user.browser_details"];
        NSArray *user_status=[results1 valueForKeyPath:@"Response.user.user_status"];
        NSArray *merchant_company_details=[results1 valueForKeyPath:@"Response.user.merchant_company_details"];
        
        
        
        NSArray *payemail=[results1 valueForKeyPath:@"Response.user.paypal_mailid"];
        NSArray *fbshare=[results1 valueForKeyPath:@"Response.user.fb_share"];
        
        
        NSLog(@"EMAIl:%@",email);
        
        
      
        
        
       
        
        
        
        if ([fbshare[0]isEqualToString:@"yes"])
        {
             _facebookSwitch.on=YES;
        }
        else
        {
            
           _facebookSwitch.on=NO;
        }
        
            
        
        
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    
}
- (void)viewDidLoad
{
    
    NSUserDefaults *ch=[NSUserDefaults standardUserDefaults];
    
   
    
    NSString *conStatus=[ch objectForKey:@"CONVERTER"];
     NSString *pinStatus=[ch objectForKey:@"PIN_STATUS"];
     
    
    
    if ([conStatus isEqualToString:@"ON"]) {
         _converterSwitch.on=YES;
    }
    else
    {
        _converterSwitch.on=NO;
    }
    
    
    
   
    if ([pinStatus isEqualToString:@"ACTIVE"]) {
        _pinSwitch.on=YES;
    }
   else
   {
       _pinSwitch.on=NO;
   }
    
    [self getUserDetails];
    
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissModalViewControllerAnimated:YES];
}

-(IBAction)friends
{
    if (IS_IPHONE_5)
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)business
{
    if (IS_IPHONE_5)
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)room
{
    if (IS_IPHONE_5)
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)setttings
{
    if (IS_IPHONE_5)
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)more
{
    if (IS_IPHONE_5)
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}

- (IBAction)facebookflip:(id)sender
{
    if (_facebookSwitch.on)
    {
        [self editcontectFun:@"yes"];
    }
    else
    {
        
        NSLog(@"OFF");
        
        [self editcontectFun:@"no"];
        
    }

}









- (IBAction)Pinflip:(id)sender
{
    if (_pinSwitch.on)
    {
         [self setPasscode:nil];
    }
    else
    {
        
         [self enterPasscode:nil];
       
    }
}

- (IBAction)Converterflip:(id)sender
{
    if (_converterSwitch.on)
    {
        NSLog(@"ON");
        NSUserDefaults *of=[NSUserDefaults standardUserDefaults];
        [of setObject:@"ON" forKey:@"CONVERTER"];
    }
    else
    {
        
        NSLog(@"OFF");
        
        NSUserDefaults *of=[NSUserDefaults standardUserDefaults];
        [of setObject:@"OFF" forKey:@"CONVERTER"];
        
    }
}


- (IBAction)setPasscode:(id)sender {
    PAPasscodeViewController *passcodeViewController = [[PAPasscodeViewController alloc] initForAction:PasscodeActionSet];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        passcodeViewController.backgroundView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStyleGrouped];
    }
    passcodeViewController.delegate = self;
    passcodeViewController.simple = !_simpleSwitch.on;
    [self presentViewController:passcodeViewController animated:YES completion:nil];
}

- (IBAction)enterPasscode:(id)sender {
    PAPasscodeViewController *passcodeViewController = [[PAPasscodeViewController alloc] initForAction:PasscodeActionEnter];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        passcodeViewController.backgroundView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStyleGrouped];
    }
    passcodeViewController.delegate = self;
    passcodeViewController.passcode = _passcodeLabel.text;
    passcodeViewController.alternativePasscode = @"9999";
    passcodeViewController.simple =  !_simpleSwitch.on;
    [self presentViewController:passcodeViewController animated:YES completion:nil];
}

- (IBAction)changePasscode:(id)sender {
    PAPasscodeViewController *passcodeViewController = [[PAPasscodeViewController alloc] initForAction:PasscodeActionChange];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        passcodeViewController.backgroundView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStyleGrouped];
    }
    passcodeViewController.delegate = self;
    passcodeViewController.passcode = _passcodeLabel.text;
    passcodeViewController.simple =  !_simpleSwitch.on;
    [self presentViewController:passcodeViewController animated:YES completion:nil];
}

#pragma mark - PAPasscodeViewControllerDelegate

- (void)PAPasscodeViewControllerDidCancel:(PAPasscodeViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
   
    NSUserDefaults *ch=[NSUserDefaults standardUserDefaults];
    
    
    NSString *pinStatus=[ch objectForKey:@"PIN_STATUS"];
    
    if ([pinStatus isEqualToString:@"ACTIVE"]) {
        _pinSwitch.on=YES;
    }
    else
    {
        _pinSwitch.on=NO;
    }

}

- (void)PAPasscodeViewControllerDidEnterAlternativePasscode:(PAPasscodeViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^() {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Alternative Passcode entered correctly" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }];
}

- (void)PAPasscodeViewControllerDidEnterPasscode:(PAPasscodeViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^()
    {
        
        _pinSwitch.on=NO;
        
        
        NSUserDefaults *status=[NSUserDefaults standardUserDefaults];
        
        
        [status setObject:@"" forKey:@"PIN_NUMBER"];
        [status setObject:@"INACTIVE" forKey:@"PIN_STATUS"];
        
        //[[[UIAlertView alloc] initWithTitle:nil message:@"Passcode entered correctly" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }];
}

- (void)PAPasscodeViewControllerDidSetPasscode:(PAPasscodeViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:^() {
        _passcodeLabel.text = controller.passcode;
        
        NSLog(@"PASS:%@",controller.passcode);
        
        
        NSUserDefaults *status=[NSUserDefaults standardUserDefaults];
        
        
        [status setObject:controller.passcode forKey:@"PIN_NUMBER"];
         [status setObject:@"ACTIVE" forKey:@"PIN_STATUS"];
        
    }];
}

- (void)PAPasscodeViewControllerDidChangePasscode:(PAPasscodeViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^() {
        _passcodeLabel.text = controller.passcode;
    }];
}




@end

//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "SignupViewController.h"

@interface SignupViewController ()

@end

@implementation SignupViewController
@synthesize emailImgView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    emailImgView.image=[UIImage imageNamed:@"si.png"];
    
    

     [_emailTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
	
    [_loginPassTextField addTarget:self action:@selector(textFieldDidChange2:) forControlEvents:UIControlEventEditingChanged];
    
     [_SignupConPassTextField addTarget:self action:@selector(textFieldDidChange1:) forControlEvents:UIControlEventEditingChanged];
       [_signupPassField addTarget:self action:@selector(textFieldDidChange1:) forControlEvents:UIControlEventEditingChanged];
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
   
    
    NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSString *subjectString =_emailTextField.text;
    
    if (textField==_signupPassField)
    {
         [_signupPassField resignFirstResponder];
         //[_SignupConPassTextField becomeFirstResponder];
    }
    else
    {
    
    if (textField==_loginPassTextField)
    {
        [_loginPassTextField resignFirstResponder];
    }
    else
    {
    if((_emailTextField.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
    {
        
      //  NSLog(@"NOT VALID");
        emailImgView.image=[UIImage imageNamed:@"sr.png"];
        
        _loginView.hidden=YES;
        _signupView.hidden=YES;
    }
    
    else
    {
        
       // NSLog(@"VALID");
        
        if ((_emailTextField.text.length==0))
        {
            emailImgView.image=[UIImage imageNamed:@"si.png"];
        }
        else
        {
            
            emailImgView.image=[UIImage imageNamed:@"sg.png"];
                        
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/checkEmailExist",CONFIG_BASE_URL]];
            
            NSLog(@"HELLO:%@",url);
            
            __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
            
                        
            [request_post setValidatesSecureCertificate:NO];
            [request_post setPostValue:_emailTextField.text forKey:@"email"];
          
            
            
            [request_post setTimeOutSeconds:30];
            
            
            [request_post setCompletionBlock:^{
                NSString *responseString = [request_post responseString];
                
                NSMutableData *results1 = [responseString JSONValue];
                
                
                
                
                
                NSString *error_string=[results1 valueForKeyPath:@"Response.email"];
                
                 NSString *email_available=@"";
                
                if ([error_string isEqualToString:@"Email ID Exists"])
                {
                    email_available=@"YES";
                }
                else
                {
                    email_available=@"NO";
                }
               
                
                
                if ([email_available isEqualToString:@"YES"])
                {
                    _loginView.hidden=NO;
                    _signupView.hidden=YES;
                    [_backView addSubview:_loginView];
                    [_loginPassTextField becomeFirstResponder];
                    
                    
                }
                else
                {
                    _loginView.hidden=YES;
                    _signupView.hidden=NO;
                    [_backView addSubview:_signupView];
                    if (textField==_emailTextField)
                    {
                        [_signupPassField becomeFirstResponder];
                    }
                    if (textField==_signupPassField)
                    {
                        [_SignupConPassTextField becomeFirstResponder];
                    }
                    if (textField==_SignupConPassTextField)
                    {
                        
                        if (![_SignupConPassTextField.text isEqualToString:_signupPassField.text])
                        {
                            
                        }
                        else
                            [_SignupConPassTextField resignFirstResponder];
                    }
                }
                
                
                
            }];
            [request_post setFailedBlock:^{
                NSError *error = [request_post error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [request_post startAsynchronous];

            
            
           
            
                        
            
            
            
        }
        
        
      
    }
    }
    }
    
    
    

    
    return YES;
}
//- (void) animateTextField: (UITextField*) textField up: (BOOL) up
//
//{
//	
//    int txtPosition = (textField.frame.origin.y - 200);
//	
//    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
//	
//    const float movementDuration = 0.3f; // tweak as needed
//    
//    int movement = (up ? -movementDistance : movementDistance);
//    
//    [UIView beginAnimations: @"anim" context: nil];
//	
//    [UIView setAnimationBeginsFromCurrentState: YES];
//	
//    [UIView setAnimationDuration: movementDuration];
//	
//    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
//	
//    [UIView commitAnimations];
//	
//}
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//
//{
//    
//    
//    [self animateTextField: textField up: YES];
//    
//}
//
//
//- (void)textFieldDidEndEditing:(UITextField *)textField
//
//{
//	
//    [self animateTextField: textField up: NO];
//   	
//}


-(IBAction)login_button
{
    NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSString *subjectString =_emailTextField.text;
    
    
    if((_emailTextField.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
    {
        
       // NSLog(@"NOT VALID");
        emailImgView.image=[UIImage imageNamed:@"sr.png"];
        
        _loginView.hidden=YES;
        _signupView.hidden=YES;
    }
    
    else
    {
        
      //  NSLog(@"VALID");
        
        if ((_emailTextField.text.length==0))
        {
            emailImgView.image=[UIImage imageNamed:@"si.png"];
        }
        else
        {
            
            emailImgView.image=[UIImage imageNamed:@"sg.png"];
            
            
            NSLog(@"EMAIL:%@",_emailTextField.text);
            NSLog(@"EMAIL:%@",_loginPassTextField.text);
            
            [_emailTextField resignFirstResponder];
            [_loginPassTextField resignFirstResponder];
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/userLogin",CONFIG_BASE_URL]];
            
            NSLog(@"HELLO:%@",url);
            
            __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
            
            
            
            
            
            [request_post setValidatesSecureCertificate:NO];
            [request_post setPostValue:_emailTextField.text forKey:@"email"];
            [request_post setPostValue:_loginPassTextField.text forKey:@"password"];
            
            
            [request_post setTimeOutSeconds:30];
            
            
            [request_post setCompletionBlock:^{
                NSString *responseString = [request_post responseString];
                             
                NSMutableData *results1 = [responseString JSONValue];
                
                
                
                
                
                NSString *error_string=[results1 valueForKeyPath:@"error.description"];
                
                NSLog(@"HEADER DES:%@",error_string);
                
                if ([error_string length]!=0)
                {
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Tab A Friend" message:error_string delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];
                    
                }
                else
                {
                    NSLog(@"RESULTS HELL:%@",responseString);
                    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
                    NSArray *userID=[results1 valueForKeyPath:@"Response.user.pk_id"];
                    [add setObject:@"0" forKey:@"FB_USERID"];
                    [add setObject:userID[0] forKey:@"USERID"];
                    [add setObject:@"" forKey:@"FB_ACCESS_TOKEN"];
                    
                    
                    NSLog(@"userID:%@",userID);
                    
                    
                    if (IS_IPHONE_5)
                    {
                        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
                        [self presentViewController:signupView animated:NO completion:nil];
                    }
                    else
                    {
                        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
                        [self presentViewController:signupView animated:NO completion:nil];
                    }

                    
                }
                
;
                
                
                
                
                
                
            }];
            [request_post setFailedBlock:^{
                NSError *error = [request_post error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [request_post startAsynchronous];

            
        }
        
        
        
    }
}

-(IBAction)merchant_button
{
    if (IS_IPHONE_5)
    {
        MerSignupViewController *signupView=[[MerSignupViewController alloc]initWithNibName:@"MerSignupViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MerSignupViewController *signupView=[[MerSignupViewController alloc]initWithNibName:@"MerSignupViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
  
}


-(IBAction)signup_button
{
    NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSString *subjectString =_emailTextField.text;
    
    
    if((_emailTextField.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
    {
        
       // NSLog(@"NOT VALID");
        emailImgView.image=[UIImage imageNamed:@"sr.png"];
        
        _loginView.hidden=YES;
        _signupView.hidden=YES;
    }
    
    else
    {
        
        //NSLog(@"VALID");
        
        if ((_emailTextField.text.length==0))
        {
            emailImgView.image=[UIImage imageNamed:@"si.png"];
        }
        else
        {
            
            emailImgView.image=[UIImage imageNamed:@"sg.png"];
            
            
            NSLog(@"EMAIL:%@",_emailTextField.text);
            NSLog(@"EMAIL:%@",_signupPassField.text);
             NSLog(@"EMAIL:%@",_SignupConPassTextField.text);
            
            [_emailTextField resignFirstResponder];
            [_SignupConPassTextField resignFirstResponder];
            [_signupPassField resignFirstResponder];
            
            
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/newUserSignup",CONFIG_BASE_URL]];
            
            NSLog(@"HELLO:%@",url);
            
            __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
            
            
            
            
            
            [request_post setValidatesSecureCertificate:NO];
            [request_post setPostValue:_emailTextField.text forKey:@"email"];
              [request_post setPostValue:_SignupConPassTextField.text forKey:@"password"];
            
            
            [request_post setTimeOutSeconds:30];
            
            
            [request_post setCompletionBlock:^{
                NSString *responseString = [request_post responseString];
                NSMutableData *results1 = [responseString JSONValue];
                
                
               
                
                
                NSString *error_string=[results1 valueForKeyPath:@"header.description"];
                
                NSLog(@"HEADER DES:%@",results1);
                
                if ([error_string length]==0)
                {
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Tab A Friend" message:error_string delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];
                    
                }
                else
                {
                    NSLog(@"RESULTS HELL:%@",responseString);
                    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
                    NSString *userID=[results1 valueForKeyPath:@"Response.userID"];
                    [add setObject:@"" forKey:@"FB_USERID"];
                    [add setObject:userID forKey:@"USERID"];
                    [add setObject:@"" forKey:@"FB_ACCESS_TOKEN"];
                    
                    
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Tab A Friend" message:error_string delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];

                    
                    
                    NSLog(@"userID:%@",userID);
                    
                    if (IS_IPHONE_5)
                    {
                        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
                        [self presentViewController:signupView animated:NO completion:nil];
                    }
                    else
                    {
                        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
                        [self presentViewController:signupView animated:NO completion:nil];
                    }

                }
                
               
               
                
                
                
                
                
            }];
            [request_post setFailedBlock:^{
                NSError *error = [request_post error];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                    message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                
                
                
            }];
            
            [request_post startAsynchronous];
            
            
            
        }
        
        
        
    }

}
- (void)textFieldDidChange:(UITextField*)sender {
    
    
    
    if (sender==_emailTextField)
    {
        NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
        NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        NSString *subjectString =sender.text;
        
        
        if((sender.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
        {
            
           // NSLog(@"NOT VALID");
            emailImgView.image=[UIImage imageNamed:@"sr.png"];
            
            _loginView.hidden=YES;
            _signupView.hidden=YES;
        }
        
        else
        {
            
           // NSLog(@"VALID");
            
            if ((sender.text.length==0))
            {
                emailImgView.image=[UIImage imageNamed:@"si.png"];
            }
            else
            {
                
                emailImgView.image=[UIImage imageNamed:@"sg.png"];
                
            }
        }
    }
    else if (sender==_SignupConPassTextField)
    {
        
       // NSLog(@"ENTER");
        
        
        if ([_SignupConPassTextField.text isEqualToString:_signupPassField.text])
        {
            [_SignupConPassTextField resignFirstResponder];
            
            _passImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
            _conImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
        }
        else
        {
            _passImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
            _conImgView.image=[UIImage imageNamed:@"passwordicon-red.png"];
        }
        
    }
    
    
    
    
}
- (void)textFieldDidChange1:(UITextField*)sender {
    
    
    
    
   // NSLog(@"ENTER");
    
    if ([_signupPassField.text length]<6)
    {
        _passImgView.image=[UIImage imageNamed:@"passwordicon-red.png"];
        
    }
    else
    {
        if ([_SignupConPassTextField.text isEqualToString:_signupPassField.text])
        {
            [_SignupConPassTextField resignFirstResponder];
            
            _passImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
            _conImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
        }
        else
        {
            _passImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
            _conImgView.image=[UIImage imageNamed:@"passwordicon-red.png"];
        }
    }
    
    
    
    
    
}

- (void)textFieldDidChange2:(UITextField*)sender {
   
    
   
        
       // NSLog(@"ENTER");
    
    if ([_loginPassTextField.text length]<6)
    {
        _loginpassImgView.image=[UIImage imageNamed:@"passwordicon-red.png"];
        
    }
    else
    {
        _loginpassImgView.image=[UIImage imageNamed:@"passwordicon-green.png"];
    }
    
   
    
    
}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
     return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back_button
{
    
    if (IS_IPHONE_5)
    {
        ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    else
    {
        ViewController *viewController=[[ViewController alloc]initWithNibName:@"ViewController4" bundle:nil];
        [self presentViewController:viewController animated:NO completion:^{
            //////(@"SUCCESS");
        }
         ];
    }
    

}

@end

//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "SubCategoryViewController.h"
#import "CatCell.h"
#import "UIImageView+WebCache.h"
#import "SubcatCell.h"

@interface SubCategoryViewController ()

@end

@implementation SubCategoryViewController
@synthesize collectionView;
CatCell *cell;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *catIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;


NSString *contactIDName;



-(IBAction)addBack
{
    [self business];
}


int editTag;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:_addContactView];
    _addContactView.hidden=YES;
    [self.view addSubview:_contactEditView];
    _contactEditView.hidden=YES;
    
    NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
    
    [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
    
    
    [_editButton setTitle:@"Edit" forState:UIControlStateNormal];
    editTag=0;
  
    
    
    
    
    
    imageThumpArray=[[NSMutableArray alloc]init];
    FnameArray=[[NSMutableArray alloc]init];
    LnameArray=[[NSMutableArray alloc]init];
    checkmark_array=[[NSMutableArray alloc]init];
    catIDArray=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    address_entered_arr=[[NSMutableArray alloc]init];
    
  
    
    
    //[self getUpdation];
    
    
    [self getCountry];
   // [self getUserDetails];
    
    
   // checkmarkArray=[[NSMutableArray alloc]init];
    
 
    
    
   //  [self.collectionView registerNib:[UINib nibWithNibName:@"CatCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
	// Do any additional setup after loading the view, typically from a nib.
}







-(void)getCountry
{
    
    
    
    
    imageThumpArray=[[NSMutableArray alloc]init];
    FnameArray=[[NSMutableArray alloc]init];
    LnameArray=[[NSMutableArray alloc]init];
    checkmark_array=[[NSMutableArray alloc]init];
    catIDArray=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    address_entered_arr=[[NSMutableArray alloc]init];
    
    
    editAddress1Arr=[[NSMutableArray alloc]init];
    editAddress2Arr=[[NSMutableArray alloc]init];
    editCityArr=[[NSMutableArray alloc]init];
    editCountryArr=[[NSMutableArray alloc]init];
    editEmailArr=[[NSMutableArray alloc]init];
    editPhoneArr=[[NSMutableArray alloc]init];
    editStateArr=[[NSMutableArray alloc]init];
    editZipArr=[[NSMutableArray alloc]init];
    
    
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    NSString *fbID=[checkval objectForKey:@"FB_USERID"];
    NSString *catID=[checkval objectForKey:@"SELECT_CATID"];
    NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
    
    // userID=@"230";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/merchant/retrieveBusinessByCategory",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
    [request_post setPostValue:catID forKey:@"categoryID"];
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        
        NSArray *temp= [results1 valueForKeyPath:@"Response.merchant"];
        
        
        
        
        for(NSDictionary *value in temp)
        {
            [imageThumpArray  addObject:[value valueForKey:@"image"]];
            
            
            [FnameArray  addObject:[value valueForKey:@"first_name"]];
            [LnameArray  addObject:[value valueForKey:@"last_name"]];
            
            
            
            //  [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
            
            
            
            [editZipArr  addObject:[value valueForKey:@"zipcode"]];
            [editStateArr  addObject:[value valueForKey:@"state"]];
            [editPhoneArr  addObject:[value valueForKey:@"phone"]];
            [editEmailArr  addObject:[value valueForKey:@"email"]];
            
            [editCountryArr  addObject:[value valueForKey:@"country"]];
            [editCityArr  addObject:[value valueForKey:@"city"]];
            [editAddress2Arr  addObject:[value valueForKey:@"address2"]];
            [editAddress1Arr  addObject:[value valueForKey:@"address1"]];
            
            
            [catIDArray  addObject:[value valueForKey:@"pk_id"]];
            
            
            
        }
        
        [_tblView reloadData];
        
        
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [catIDArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    
    
    SubcatCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"SubcatCell" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (SubcatCell*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
   cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
  
    @try {
        NSString *temp=[imageThumpArray objectAtIndex:indexPath.row];
        [cell.img setImageWithURL:[NSURL URLWithString:temp]
                        placeholderImage:[UIImage imageNamed:@"logo-black2.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }
    
    
    cell.title.text=[NSString stringWithFormat:@"%@ %@",FnameArray[indexPath.row],LnameArray[indexPath.row]];
    
    
    cell.category.text=[NSString stringWithFormat:@"%@,%@\n%@,%@\n%@,%@\n",editAddress1Arr[indexPath.row],editAddress2Arr[indexPath.row],editCityArr[indexPath.row],editStateArr[indexPath.row],editCountryArr[indexPath.row],editZipArr[indexPath.row]];
    
    return cell;
    
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 61;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Select %d",indexPath.row);
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    @try {
        [check setObject:imageThumpArray[indexPath.row] forKey:@"BUSINESS_IMG"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"BUSINESS_IMG"];
    }
    
    
    @try {
        [check setObject:FnameArray[indexPath.row] forKey:@"BUSINESS_FNAME"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"BUSINESS_FNAME"];
    }
    
    @try {
        [check setObject:LnameArray[indexPath.row] forKey:@"BUSINESS_LNAME"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"BUSINESS_LNAME"];
    }
    
    
    
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editAddress1Arr[indexPath.row]] forKey:@"BUSINESS_AD1"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"BUSINESS_AD1"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editAddress2Arr[indexPath.row]] forKey:@"BUSINESS_AD2"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"BUSINESS_AD2"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editCityArr[indexPath.row]] forKey:@"BUSINESS_CITY"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"BUSINESS_CITY"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editCountryArr[indexPath.row]] forKey:@"BUSINESS_COUNTRY"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"BUSINESS_COUNTRY"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editZipArr[indexPath.row]] forKey:@"BUSINESS_ZIP"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"BUSINESS_ZIP"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editStateArr[indexPath.row]] forKey:@"BUSINESS_STATE"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"BUSINESS_STATE"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",catIDArray[indexPath.row]] forKey:@"BUSINESS_ID"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"BUSINESS_ID"];
    }
    
    @try {
        [check setObject:[NSString stringWithFormat:@"%@",editCountryArr[indexPath.row]] forKey:@"BUSINESS_COUNTRY"];
    }
    @catch (NSException *exception) {
        [check setObject:@"" forKey:@"BUSINESS_COUNTRY"];
    }
    
    
    
    
    if (IS_IPHONE_5)
    {
        BusinessFriendsViewController *signupView=[[BusinessFriendsViewController alloc]initWithNibName:@"BusinessFriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        BusinessFriendsViewController *signupView=[[BusinessFriendsViewController alloc]initWithNibName:@"BusinessFriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }

}


-(IBAction)searchBtn
{
   
    [_searchTextField resignFirstResponder];
    
    if ([_searchTextField.text isEqualToString:@""])
    {
        [self getCountry];
    }
    else
    {
         NSLog(@"SEAR:%@",_searchTextField.text);
        
        [self searchResults];
    }
}

-(void)searchResults
{
    
    
    
    
    imageThumpArray=[[NSMutableArray alloc]init];
    FnameArray=[[NSMutableArray alloc]init];
    LnameArray=[[NSMutableArray alloc]init];
    checkmark_array=[[NSMutableArray alloc]init];
    catIDArray=[[NSMutableArray alloc]init];
    checked_status_arr=[[NSMutableArray alloc]init];
    address_entered_arr=[[NSMutableArray alloc]init];
    
    
    editAddress1Arr=[[NSMutableArray alloc]init];
    editAddress2Arr=[[NSMutableArray alloc]init];
    editCityArr=[[NSMutableArray alloc]init];
    editCountryArr=[[NSMutableArray alloc]init];
    editEmailArr=[[NSMutableArray alloc]init];
    editPhoneArr=[[NSMutableArray alloc]init];
    editStateArr=[[NSMutableArray alloc]init];
    editZipArr=[[NSMutableArray alloc]init];
    
    
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    NSString *fbID=[checkval objectForKey:@"FB_USERID"];
   
    NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
    
    // userID=@"230";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/merchant/searchBusiness",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
    [request_post setPostValue:_searchTextField.text forKey:@"searchCriteriaText"];
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        
        NSArray *temp= [results1 valueForKeyPath:@"Response.business"];
        
        
        
        
        for(NSDictionary *value in temp)
        {
            [imageThumpArray  addObject:[value valueForKey:@"image"]];
            
            
            [FnameArray  addObject:[value valueForKey:@"first_name"]];
            [LnameArray  addObject:[value valueForKey:@"last_name"]];
            
            
            
            //  [address_entered_arr  addObject:[value valueForKey:@"address_entered"]];
            
            
            
            [editZipArr  addObject:[value valueForKey:@"zipcode"]];
            [editStateArr  addObject:[value valueForKey:@"state"]];
            [editPhoneArr  addObject:[value valueForKey:@"phone"]];
            [editEmailArr  addObject:[value valueForKey:@"email"]];
            
            [editCountryArr  addObject:[value valueForKey:@"country"]];
            [editCityArr  addObject:[value valueForKey:@"city"]];
            [editAddress2Arr  addObject:[value valueForKey:@"address2"]];
            [editAddress1Arr  addObject:[value valueForKey:@"address1"]];
            
            
            [catIDArray  addObject:[value valueForKey:@"pk_id"]];
            
            
            
        }
        
        [_tblView reloadData];
        
        
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    [textField resignFirstResponder];
    
      
    
    return YES;
    
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
   
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}
#pragma mark - LeveyPopListView delegates
- (void)leveyPopListView:(LeveyPopListView *)popListView didSelectedIndex:(NSInteger)anIndex
{
    NSLog(@"%@",[NSString stringWithFormat:@"You have selected %@", countryArray[anIndex]]);
}
- (void)leveyPopListViewDidCancel
{
    NSLog(@"CANCELLED");
}
////// FACEBOOK END //
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (buttonIndex==0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
        }
        
    }
    else  if (buttonIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
    
}




-(IBAction)friends
{
    if (IS_IPHONE_5)
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)business
{
    if (IS_IPHONE_5)
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)room
{
    if (IS_IPHONE_5)
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)setttings
{
    if (IS_IPHONE_5)
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)more
{
    if (IS_IPHONE_5)
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}




@end

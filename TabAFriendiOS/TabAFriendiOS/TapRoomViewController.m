//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "TapRoomViewController.h"
#import "receiptentCell.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>

@interface TapRoomViewController ()

@end

@implementation TapRoomViewController
@synthesize collectionView;
receiptentCell *cell;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *contactIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;


NSString *contactIDName;






int editTag;
-(IBAction)qrClick
{
    NSLog(@"Clicked");
    
   // self.frame = CGRectMake(0.0f, 0.0f, 200.0f, 150.0f);
    [UIView beginAnimations:@"Zoom" context:NULL];
    [UIView setAnimationDuration:0.5];
   // self.frame = CGRectMake(0.0f, 0.0f, 1024.0f, 768.0f);
    [UIView commitAnimations];
    _qrView.hidden=NO;
    
    
  
}
-(IBAction)qrBack
{
    _qrView.hidden=YES;
}
-(IBAction)paypalClick
{
    if (IS_IPHONE_5)
    {
        PaypalViewController *signupView=[[PaypalViewController alloc]initWithNibName:@"PaypalViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        PaypalViewController *signupView=[[PaypalViewController alloc]initWithNibName:@"PaypalViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
   
}
- (void)viewDidLoad
{
   
    [self.view addSubview:_qrView];
    _qrView.hidden=YES;
    
    
    [self getTapDetails];
}

-(void)getTapDetails
{
    
        
        
       
        
        
        
        NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
        
        NSString *userID=[checkval objectForKey:@"USERID"];
         NSString *tapID=[checkval objectForKey:@"LAST_TAPPED_ID"];
        
        // userID=@"230";
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/tap/retrieveTaps",CONFIG_BASE_URL]];
        
        NSLog(@"HELLO:%@",url);
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:userID forKey:@"userID"];
        [request_post setPostValue:tapID forKey:@"tapID"];
        
        
        [request_post setTimeOutSeconds:30];
        
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            
            
            
            
            
            
            
            
            NSMutableData *results1 = [responseString JSONValue];
            
            
            
            NSLog(@"RESULTS HELL:%@",results1);
            
            
            
            
            NSArray *temp= [results1 valueForKeyPath:@"Response.tapDetails"];
            
            
            
            
            for(NSDictionary *value in temp)
            {
                
                
                
                NSString *tapAmountArr=[value valueForKey:@"tap_amount"];
                 NSString *fromFName=[value valueForKey:@"senderFirstName"];
                 NSString *fromLName=[value valueForKey:@"senderLastName"];
                 NSString *fromImage=[value valueForKey:@"senderImage"];
                 NSString *fromID=[value valueForKey:@"fk_sender_id"];
                
                NSString *recFName=[value valueForKey:@"receiverFirstName"];
                NSString *recLName=[value valueForKey:@"receiverLastName"];
                NSString *recImage=[value valueForKey:@"receiverImage"];
                NSString *recID=[value valueForKey:@"fk_receiver_id"];
                
                
                NSString *merFName=[value valueForKey:@"merchantFirstName"];
                NSString *merLName=[value valueForKey:@"merchantLastName"];
                NSString *merImage=[value valueForKey:@"merchantImage"];
                NSString *merID=[value valueForKey:@"fk_merchant_id"];
                NSString *merAd1=[value valueForKey:@"merchantAddress1"];
                NSString *merAd2=[value valueForKey:@"merchantAddress2"];
                NSString *meeCity=[value valueForKey:@"merchantCity"];
                NSString *merState=[value valueForKey:@"merchantState"];
               
                 NSString *merZip=[value valueForKey:@"merchantZipcode"];
                
                
                 NSString *expireDate=[value valueForKey:@"expire_date"];
                 NSString *merLAt=[value valueForKey:@"merchantLat"];
                 NSString *merLong=[value valueForKey:@"merchantLong"];
                 NSString *shortURL=[value valueForKey:@"merchantZipcode"];
                 NSString *qrImage=[value valueForKey:@"qr_code"];
                 NSString *status=[value valueForKey:@"tap_status"];
                
                
                NSString *totalAmount=[value valueForKey:@"total_amount"];
                NSString *stripeCommission=[value valueForKey:@"tap_create_strip_charge"];
                NSString *tapCommission=[value valueForKey:@"tap_commsion"];
                
                NSString *transanctionCharge;
                
                
                
                    
                    _detailFeeDisplayView.layer.cornerRadius=8.0;
                    _detailFeeDisplayView.layer.masksToBounds = YES;
                    
                    if([status isEqualToString:@"converted_to_cash_success"])
                    {
                        transanctionCharge=[value valueForKey:@"convert_cash_strip_charge"];
                        _transactionFee.hidden=NO;
                        _transactionFeeLabel.hidden=NO;
                        
                        
                        _transactionFee.text=[NSString stringWithFormat:@"$%.2f",[transanctionCharge floatValue]];
                    }
                    else
                    {
                        _transactionFee.hidden=YES;
                        _transactionFeeLabel.hidden=YES;
                    }
                    
                    _totalAmountFee.text=[NSString stringWithFormat:@"$%.2f",[totalAmount floatValue]];
                    _stripeCommissionFee.text=[NSString stringWithFormat:@"$%.2f",[stripeCommission floatValue]];
                    _tapCommissionFee.text=[NSString stringWithFormat:@"$%.2f",[tapCommission floatValue]];
                    _tapAmountFee.text=[NSString stringWithFormat:@"$%.2f",[tapAmountArr floatValue]];
                               
                
                NSUserDefaults *ch=[NSUserDefaults standardUserDefaults];
                
                
                
                NSString *conStatus=[ch objectForKey:@"CONVERTER"];
                 NSString *userID=[checkval objectForKey:@"USERID"];
                
                
                if ([conStatus isEqualToString:@"ON"])
                {
                    if ([recID intValue]==[userID intValue])
                    {
                         _paypalButton.hidden=NO;
                    }
                    else
                        
                    {
                         _paypalButton.hidden=YES;
                        
                    }
                    
                   
                }
                else
                {
                    _paypalButton.hidden=YES;
                }

                
                
                
                
                NSLog(@"TAP_AMOUNT:%@",tapAmountArr);
                              
//                _AmountLabel.text=[NSString stringWithFormat:@"%@",tapAmountArr];
                _AmountLabel.text=[NSString stringWithFormat:@"%.2f",[tapAmountArr floatValue]];
                _fromName.text=[NSString stringWithFormat:@"%@ %@",fromFName,fromLName];
                @try {
                   
                    [_fromImgView setImageWithURL:[NSURL URLWithString:fromImage]
                                    placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                }
                
                _toName.text=[NSString stringWithFormat:@"%@ %@",recFName,recLName];
                @try
                {
                    
                    [_toImgView setImageWithURL:[NSURL URLWithString:recImage]
                                 placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                }
                
                _MerchentName.text=[NSString stringWithFormat:@"%@ %@",merFName,merLName];
                @try {
                    
                    [_merchentImgView setImageWithURL:[NSURL URLWithString:merImage]
                                 placeholderImage:[UIImage imageNamed:@"logo-black1.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                }
                
                 _MerchentAddress.text=[NSString stringWithFormat:@"%@,%@\n%@,%@\n%@",merAd1,merAd2,meeCity,merState,merZip];
                
                
                //qrImage=@"https://chart.googleapis.com/chart?chs=450x450&cht=qr&chl=http://www.sng.me/50&choe=UTF-8";
                
                @try {
                    
                    [_QRImgView setImageWithURL:[NSURL URLWithString:qrImage]
                                     placeholderImage:[UIImage imageNamed:@"chart.png"]];
                    
                    
                    [_QRImgView1 setImageWithURL:[NSURL URLWithString:qrImage]
                               placeholderImage:[UIImage imageNamed:@"chart.png"]];
                    
                    
                    
                }
                @catch (NSException *exception) {
                    //////////////(@"CHJECK");
                    
                    _QRImgView.image=[UIImage imageNamed:@"chart.png"];
                     _QRImgView1.image=[UIImage imageNamed:@"chart.png"];
                }
                
                
                
                NSUserDefaults *addLat=[NSUserDefaults standardUserDefaults];
                                
                NSString *locLat1  = [NSString stringWithFormat:@"%@", merLAt];
                NSString * locLong1 = [NSString stringWithFormat:@"%@", merLong];
                
                [addLat setObject:locLat1 forKey:@"TO_LAT"];
                [addLat setObject:locLong1 forKey:@"TO_LONG"];
                
                
                
                
                
                
                
                NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
                
                [dateFormat1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                 
                
                NSDate *someDate = [dateFormat1 dateFromString:expireDate];
                
                
                
                
                if ([status isEqualToString:@"valid"])
                {
                    if ([expireDate isEqualToString:@"0000-00-00 00:00:00"])
                    {
                        _ExpiredLabel.text=@"";
                       
                        _exBackground.hidden=YES;
                        
                        
                        _qrButton.hidden=YES;
                        
                        _QRStatusImgView.image=[UIImage imageNamed:@"expired-icon.png"];
                    }
                    else
                    {
                        if ([someDate timeIntervalSinceNow] < 0.0) {
                            // Date has passed
                            _ExpiredLabel.text=@"";
                            _exBackground.hidden=YES;
                            _qrButton.hidden=YES;
                            _QRStatusImgView.image=[UIImage imageNamed:@"expired-icon.png"];
                        }
                        else
                        {
                             NSDate *now = [NSDate date];
                           
                            NSCalendar *calendar = [NSCalendar currentCalendar];
                            
                            NSDate *fromDate;
                            NSDate *toDate;
                            
                            _qrButton.hidden=NO;
                            [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                                         interval:NULL forDate:now];
                            [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                                         interval:NULL forDate:someDate];
                            
                            NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                                                       fromDate:fromDate toDate:toDate options:0];
                            
                            NSLog(@"DAYYYY:%ld",(long)[difference day]);
                            
                            if ((long)[difference day]==0) {
                                 _ExpiredLabel.text=[NSString stringWithFormat:@"EXPIRES IN TODAY"];
                            }
                            else
                            {
                            
                            _ExpiredLabel.text=[NSString stringWithFormat:@"EXPIRES IN %ld DAY(S)",(long)[difference day]];
                            }
                        }
                    }
                }
                else
                {
                    if ([status isEqualToString:@"expired"]||[expireDate isEqualToString:@"0000-00-00 00:00:00"])
                    {
                        _ExpiredLabel.text=@"";
                        _exBackground.hidden=YES;
                        _qrButton.hidden=YES;
                        _QRStatusImgView.image=[UIImage imageNamed:@"expired-icon.png"];
                    }
                   
                
                     if ([status isEqualToString:@"used"])
                     {
                        _QRStatusImgView.image=[UIImage imageNamed:@"used.png"];
                         _exBackground.hidden=YES;
                          _ExpiredLabel.text=@"";
                         _qrButton.hidden=YES;
                     }
                    
                    
                   
                    if ([status rangeOfString:@"converted"].location == NSNotFound) {
                        NSLog(@"string does not contain bla");
                    } else {
                        
                        _QRStatusImgView.image=[UIImage imageNamed:@"converted_orange.png"];
                        _exBackground.hidden=YES;
                        _ExpiredLabel.text=@"";
                        _paypalButton.hidden=YES;
                        _qrButton.hidden=YES;
                    }
                    
                   
                }
        
                
            }
            
            //[_tblView reloadData];
            
            
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];
 
}

-(IBAction)addBack
{
    [self room];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    [textField resignFirstResponder];
    
      
    
    return YES;
    
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
	
}
-(IBAction)Direction
{
    if (IS_IPHONE_5)
    {
        LocationViewController *signupView=[[LocationViewController alloc]initWithNibName:@"LocationViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        LocationViewController *signupView=[[LocationViewController alloc]initWithNibName:@"LocationViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
   
    
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	
    [self animateTextField: textField up: NO];
   	
}
#pragma mark - LeveyPopListView delegates
- (void)leveyPopListView:(LeveyPopListView *)popListView didSelectedIndex:(NSInteger)anIndex
{
    NSLog(@"%@",[NSString stringWithFormat:@"You have selected %@", countryArray[anIndex]]);
}
- (void)leveyPopListViewDidCancel
{
    NSLog(@"CANCELLED");
}
////// FACEBOOK END //
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (buttonIndex==0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
        }
        
    }
    else  if (buttonIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    
    
}


-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    NSData *imagedata = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage" ],1);
    UIImage *image = [[UIImage alloc] initWithData:imagedata];
    
    
    //  image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(240, 240)];
    
    
    float ori_height=image.size.height;
    float ori_width=image.size.width;
    
    NSLog(@"H:%f|W:%f",ori_height,ori_width);
    
    
    float height_ratio1=ori_height/4;
    float width_ratio1=ori_width/4;
    
    NSLog(@"HJ:%f|WJ:%f",height_ratio1,width_ratio1);
    
    
    
    image = [self imageWithImageSimple:image scaledToSize:CGSizeMake(width_ratio1, height_ratio1)];
    
    
    if (editTag==0)
    {
        
        [_imageButton setImage:image forState:UIControlStateNormal];
        [_imageButton setTitle:nil forState:UIControlStateNormal];
    }
    else
    {
        [_editimageButton setImage:image forState:UIControlStateNormal];
        [_editimageButton setTitle:nil forState:UIControlStateNormal];
    }
    
   
   
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postProImgfile",@"http://api.socialnetgateapp.com/v1"]];
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    //[request_post setFile:@"/Users/apple/Desktop/Free-Grammar-Ebook-Level-2.pdf" forKey:@"filename"];
    
    //UIImage * ourImage = [UIImage imageNamed:@"rajini-bday2.jpg"];
    NSData * ourImageData = UIImageJPEGRepresentation(image, 100);
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:ss zzz"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    //////(@"DATE STRING:%@",dateString);
    
    NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
    NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
    NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSString *imageName=[NSString stringWithFormat:@"%@.jpg",newString3];
    
    
    
    [request_post addData:ourImageData withFileName:imageName andContentType:@"image/jpeg" forKey:@"file"];
    
    [request_post setTimeOutSeconds:60];
    
    
    [request_post setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request_post responseString];
        
        
        //////(@"RES:%@",responseString);
        
        NSMutableData *results11 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",results11);
        
        
        NSString *temp= [results11 valueForKeyPath:@"photos.profile_img_url"];
        
        
        NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
        
        [imgtemp setObject:temp forKey:@"USER_IMAGE"];
        
        
        
        
        if ([temp length]==0) {
            
            NSString *temp= [results11 valueForKeyPath:@"error.text"];
            
            
            NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
            
            [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
            
            [_imageButton setImage:nil forState:UIControlStateNormal];
           
            
        }
        
        // Use when fetching binary data
        // NSData *responseData = [request_post responseData];
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        
        
        
        
    }];
    [request_post startAsynchronous];
    
    
    [picker dismissModalViewControllerAnimated:YES];
    
    
    
    
}
-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissModalViewControllerAnimated:YES];
}

-(IBAction)friends
{
    if (IS_IPHONE_5)
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)business
{
    if (IS_IPHONE_5)
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    
}
-(IBAction)room
{
    if (IS_IPHONE_5)
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)setttings
{
    if (IS_IPHONE_5)
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)more
{
    if (IS_IPHONE_5)
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}




- (IBAction)detailBack:(id)sender
{
    _detailFeeView.hidden=YES;
}
- (IBAction)goFeeDetailView:(id)sender
{
    _detailFeeView.hidden=NO;
}
@end

//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "TapViewController.h"
#import "receiptentCell.h"
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"
#import "STPCard.h"
@interface TapViewController ()

@end

@implementation TapViewController
@synthesize collectionView;
receiptentCell *cell;

NSMutableArray *checkmarkArray;
NSMutableArray *countryArray;
NSString *countrystr;
NSString *editcountrystr;
NSString *contactIDSelected;

NSMutableArray *imageThumpArray;
NSMutableArray *FnameArray;
NSMutableArray *LnameArray;
NSMutableArray *countryArray;
NSMutableArray *checkmark_array;
NSString *countrystr;
NSMutableArray* checked_status_arr;
UIActivityIndicatorView *Activity;
UIAlertView *progressAlert;
UIAlertView *firstalert;
NSMutableArray* address_entered_arr;

NSMutableArray *contactFname;
NSMutableArray *contactMobile_numbers;
NSMutableArray *contacttype_array;
NSMutableArray *contactLastNames;
NSMutableArray *selectContactArray1;

NSMutableArray *contactIDArray;

NSMutableArray *contactSourceArray;

NSMutableArray *editAddress1Arr;
NSMutableArray *editAddress2Arr;
NSMutableArray *editCityArr;
NSMutableArray *editStateArr;
NSMutableArray *editCountryArr;
NSMutableArray *editZipArr;
NSMutableArray *editPhoneArr;
NSMutableArray *editEmailArr;
STPCard *stripeCard;

NSString *contactIDName;

UIAlertView *cardAlertView;


int cardTag;

-(IBAction)Back
{
    [self dismissViewControllerAnimated:NO completion:Nil];
}


int editTag;

-(void)getUpdation
{
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    
    NSString *nameStr=[NSString stringWithFormat:@"%@ %@",[check valueForKey:@"BUSINESS_FNAME"],[check valueForKey:@"BUSINESS_LNAME"]];
    
    _catNameLabel.text=nameStr;
    
    
    NSString *imgStr=[check valueForKey:@"BUSINESS_IMG"];
    
    @try {
        NSString *temp=imgStr;
        [_catImageView setImageWithURL:[NSURL URLWithString:temp]
                     placeholderImage:[UIImage imageNamed:@"logo-black2.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }
    
    
    
    NSString *addrStr=[NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@",[check valueForKey:@"BUSINESS_AD1"],[check valueForKey:@"BUSINESS_AD2"],[check valueForKey:@"BUSINESS_CITY"],[check valueForKey:@"BUSINESS_STATE"],[check valueForKey:@"BUSINESS_COUNTRY"],[check valueForKey:@"BUSINESS_ZIP"]];
    
    
    _catAddressLabel.text=addrStr;
    
    NSString *nameStr1=[NSString stringWithFormat:@"%@ %@",[check valueForKey:@"CONTCAT_FNAME"],[check valueForKey:@"CONTCAT_LNAME"]];
    
    _toNameLabel.text=nameStr1;
    
    
    NSString *imgStr1=[check valueForKey:@"CONTCAT_IMG"];
    
    @try {
        NSString *temp=imgStr1;
        [_toImageView setImageWithURL:[NSURL URLWithString:temp]
                     placeholderImage:[UIImage imageNamed:@"logo-black.png"]];
        
        
        
    }
    @catch (NSException *exception) {
        //////////////(@"CHJECK");
    }
    
    
    
    //NSString *addrStr1=[NSString stringWithFormat:@"%@ %@\n%@ %@\n%@ %@",[check valueForKey:@"CONTCAT_AD1"],[check valueForKey:@"CONTCAT_AD2"],[check valueForKey:@"CONTCAT_CITY"],[check valueForKey:@"CONTCAT_STATE"],[check valueForKey:@"CONTCAT_COUNTRY"],[check valueForKey:@"CONTCAT_ZIP"]];
    
     NSString *addrStr1=[NSString stringWithFormat:@"Email: %@\nMobile: %@",[check valueForKey:@"CONTCAT_EMAIL"],[check valueForKey:@"CONTCAT_PHONE"]];
    
    _toAddressLabel.text=addrStr1;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
      [_cvvField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
     _buyButton.hidden=YES;
    
    _tapAmount.text=@"10";
    
    cardTag=0;
    
    [_tapAmount becomeFirstResponder];
    [self getUpdation];
    [self getUserDetails];
    
   
}

-(void)getUserDetails
{
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[checkval objectForKey:@"USERID"];
    NSString *fbID=[checkval objectForKey:@"FB_USERID"];
    NSString *accessToken=[checkval objectForKey:@"FB_ACCESS_TOKEN"];
    
    // userID=@"230";
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserDetails",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:userID forKey:@"userID"];
    
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS HELL:%@",results1);
        
        NSArray *pk_id=[results1 valueForKeyPath:@"Response.user.pk_id"];
        NSArray *fk_facebook_user_id=[results1 valueForKeyPath:@"Response.user.fk_facebook_user_id"];
        NSArray *fk_merchant_package_id=[results1 valueForKeyPath:@"Response.user.fk_merchant_package_id"];
        NSArray *first_name=[results1 valueForKeyPath:@"Response.user.first_name"];
        NSArray *last_name=[results1 valueForKeyPath:@"Response.user.last_name"];
        NSArray *email=[results1 valueForKeyPath:@"Response.user.email"];
        NSArray *password=[results1 valueForKeyPath:@"Response.user.password"];
        NSArray *signup_type=[results1 valueForKeyPath:@"Response.user.signup_type"];
        NSArray *facebook_access_token=[results1 valueForKeyPath:@"Response.user.facebook_access_token"];
        NSArray *image=[results1 valueForKeyPath:@"Response.user.image"];
        NSArray *address1=[results1 valueForKeyPath:@"Response.user.address1"];
        NSArray *address2=[results1 valueForKeyPath:@"Response.user.address2"];
        NSArray *city=[results1 valueForKeyPath:@"Response.user.city"];
        NSArray *state=[results1 valueForKeyPath:@"Response.user.state"];
        NSArray *zipcode=[results1 valueForKeyPath:@"Response.user.zipcode"];
        NSArray *country=[results1 valueForKeyPath:@"Response.user.country"];
        NSArray *phone=[results1 valueForKeyPath:@"Response.user.phone"];
        NSArray *user_security_pin=[results1 valueForKeyPath:@"Response.user.user_security_pin"];
        NSArray *user_type=[results1 valueForKeyPath:@"Response.user.user_type"];
        NSArray *location_latitude=[results1 valueForKeyPath:@"Response.user.location_latitude"];
        NSArray *location_longitude=[results1 valueForKeyPath:@"Response.user.location_longitude"];
        NSArray *created_date=[results1 valueForKeyPath:@"Response.user.created_date"];
        NSArray *created_ipaddress=[results1 valueForKeyPath:@"Response.user.created_ipaddress"];
        NSArray *updated_date=[results1 valueForKeyPath:@"Response.user.updated_date"];
        NSArray *updated_ipaddress=[results1 valueForKeyPath:@"Response.user.updated_ipaddress"];
        NSArray *browser_details=[results1 valueForKeyPath:@"Response.user.browser_details"];
        NSArray *user_status=[results1 valueForKeyPath:@"Response.user.user_status"];
        NSArray *merchant_company_details=[results1 valueForKeyPath:@"Response.user.merchant_company_details"];
        
        
        
        NSArray *payemail=[results1 valueForKeyPath:@"Response.user.paypal_mailid"];
        NSArray *fbshare=[results1 valueForKeyPath:@"Response.user.fb_share"];
        
        
        
        NSArray *accNameArr=[results1 valueForKeyPath:@"Response.user.bank_acc_holder_name"];
        NSArray *accNumArr=[results1 valueForKeyPath:@"Response.user.bank_acc_number"];
        NSArray *bankNameArr=[results1 valueForKeyPath:@"Response.user.bank_name"];
        NSArray *bankCodeArr=[results1 valueForKeyPath:@"Response.user.bank_swift_code"];
        
        
        NSArray *cardNameArr=[results1 valueForKeyPath:@"Response.user.card_holder_name"];
        NSArray *cardNumArr=[results1 valueForKeyPath:@"Response.user.card_number"];
        NSArray *cardExpArr=[results1 valueForKeyPath:@"Response.user.card_expiry_date"];
        
        
        
        NSUserDefaults *checkCard=[NSUserDefaults standardUserDefaults];
        
        [checkCard setObject:cardNameArr[0] forKey:@"SAVED_CC_NAME"];
        [checkCard setObject:cardNumArr[0] forKey:@"SAVED_CC_NUMBER"];
        [checkCard setObject:cardExpArr[0] forKey:@"SAVED_CC_DATE"];
        
        
        if (![cardNameArr[0] isEqualToString:@""])
        {
            NSString *savedCardNumber=cardNumArr[0];
            
            savedCardNumber = [savedCardNumber stringByReplacingOccurrencesOfString:@"C"
                                                                         withString:@""];
            
            NSString *trimmedString=[savedCardNumber substringFromIndex:MAX((int)[savedCardNumber length]-4, 0)];
            
            NSLog(@"Trim:%@",savedCardNumber);
            
            _cvvLabel.text=[NSString stringWithFormat:@"**** **** **** %@",trimmedString];
        }
        
     
        
        
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
    
}


-(IBAction)clickTextField
{
    self.stripeView.hidden=YES;
    _cvvView.hidden=YES;
     _buyButton.hidden=YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];_cvvView.hidden=NO;
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidChange:(UITextField*)sender {
    
    
    
    if (sender==_cvvField)
    {
        NSString *lenStr=_cvvField.text;
        
        int len=[lenStr length];
        
        if (len==3)
        {
            [_cvvField resignFirstResponder];
            _buyButton.hidden=NO;
        }
        else
        {
             _buyButton.hidden=YES;
        }
        
        
    }
        
    
    
    
}


- (void)performStripeOperation {
    
        //2
    /*
     [Stripe createTokenWithCard:self.stripeCard
     publishableKey:STRIPE_TEST_PUBLIC_KEY
     success:^(STPToken* token) {
     [self postStripeToken:token.tokenId];
     } error:^(NSError* error) {
     [self handleStripeError:error];
     }];
     */
    [Stripe createTokenWithCard:stripeCard
                 publishableKey:STRIPE_KEY
                     completion:^(STPToken* token, NSError* error) {
                         if(error)
                             [self handleError:error];
                         else
                             [self handleToken:token];
                     }];
}

- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    
    /*
    stripeCard = [[STPCard alloc] init];
    stripeCard.name = @"PREMKUMAR";
    stripeCard.number = @"4242424242424242";
    stripeCard.cvc = @"444";
    stripeCard.expMonth = 11;
    stripeCard.expYear = 15;
    
    //2
   
        [self performStripeOperation];
*/
    
    
    
       
   
    
    
    
    if ([_tapAmount.text isEqualToString:@""])
    {
        
    }
   else
   {
       //[self tapFuntion];
       
       
       
       
       
       NSString *tapAmount=_tapAmount.text;
       
       int tapAmountInt=[tapAmount intValue];
       
       if (tapAmountInt<10)
       {
           UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Tap A Friend"
                                                             message:@"Minimum Tap Amount is $10"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
           [message show];

      
        }
       else
       {
           
           
           
           
           NSUserDefaults *checkCard=[NSUserDefaults standardUserDefaults];
           NSString *savedCardName=[checkCard objectForKey:@"SAVED_CC_NAME"];
            NSString *savedCardNumber=[checkCard objectForKey:@"SAVED_CC_NUMBER"];
            NSString *savedCardDate=[checkCard objectForKey:@"SAVED_CC_DATE"];
           
           
           if ([savedCardName isEqualToString:@""])
           {
               
               cardTag=0;
               
               if (IS_IPHONE_5)
               {
                   self.stripeView = [[STPView alloc] initWithFrame:CGRectMake(15,280,290,55)
                                                             andKey:STRIPE_KEY];
                   self.stripeView.delegate = self;
                  
                   [self.view addSubview:self.stripeView];
                   self.stripeView.hidden=NO;
               }
               else
               {
                   self.stripeView = [[STPView alloc] initWithFrame:CGRectMake(15,195,290,55)
                                                             andKey:STRIPE_KEY];
                   self.stripeView.delegate = self;
                   [self.view addSubview:self.stripeView];
                   self.stripeView.hidden=NO;
               }

           }
           else
           {
               NSLog(@"Saved Card Available");
               
               cardAlertView = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                      message:@"Do you want to use your saved card?" delegate:self cancelButtonTitle: @"No"
                                            otherButtonTitles:@"Yes", nil];
               
               //note above delegate property
               
               [cardAlertView show];
               
               
               
               
           }
           
       }
    

   }
    
    
    
    return YES;
    
    
}



-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //u need to change 0 to other value(,1,2,3) if u have more buttons.then u can check which button was pressed.
    
    if (alertView==cardAlertView)
    {
        if (buttonIndex == 0)
        {
            
            //(@"0");
            
            cardTag=0;
            
            if (IS_IPHONE_5)
            {
                self.stripeView = [[STPView alloc] initWithFrame:CGRectMake(15,280,290,55)
                                                          andKey:STRIPE_KEY];
                self.stripeView.delegate = self;
                [self.view addSubview:self.stripeView];
                self.stripeView.hidden=NO;
            }
            else
            {
                self.stripeView = [[STPView alloc] initWithFrame:CGRectMake(15,195,290,55)
                                                          andKey:STRIPE_KEY];
                self.stripeView.delegate = self;
                [self.view addSubview:self.stripeView];
                self.stripeView.hidden=NO;
            }

            
            
        }
        else
        {
            // [self fbDidLogout];
            
            
            cardTag=1;
            
            if (IS_IPHONE_5)
            {
                
                
                
                _cvvView.frame=CGRectMake(20,280,280,52);
                [self.view addSubview:_cvvView];
                _cvvView.hidden=NO;
                [_cvvField becomeFirstResponder];
            }
            else
            {
                
                _cvvView.frame=CGRectMake(20,195,280,52);
                [self.view addSubview:_cvvView];
                _cvvView.hidden=NO;
                [_cvvField becomeFirstResponder];
                
                
            }
      
            
            
        }
    }
    else
    {
        
        
    }
    
}


- (void)stripeView:(STPView *)view withCard:(PKCard *)card isValid:(BOOL)valid
{
   
    
    if (valid)
    {
        _buyButton.hidden=NO;
    }
    else
    {
       _buyButton.hidden=YES;
    }
    
    
    
}



-(IBAction)save:(id)sender
{
    // Call 'createToken' when the save button is tapped
    
    NSUserDefaults *ch=[NSUserDefaults standardUserDefaults];
    
    NSString *pinNumber=[ch objectForKey:@"PIN_NUMBER"];
    NSString *pinStatus=[ch objectForKey:@"PIN_STATUS"];
    
    if ([pinStatus isEqualToString:@"ACTIVE"])
    {
        [self enterPasscode:nil];
    }
    else
    {
        
               
        NSUserDefaults *checkCard=[NSUserDefaults standardUserDefaults];
        NSString *savedCardName=[checkCard objectForKey:@"SAVED_CC_NAME"];
        NSString *savedCardNumber=[checkCard objectForKey:@"SAVED_CC_NUMBER"];
        NSString *savedCardDate=[checkCard objectForKey:@"SAVED_CC_DATE"];
        
        
        if (cardTag==0)
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            
            [self.stripeView createToken:^(STPToken *token, NSError *error)
             {
                 
                 
                 
                 if (error) {
                     // Handle error
                     [self handleError:error];
                     
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                 } else {
                     // Send off token to your server
                     [self handleToken:token];
                 }
             }];
            
        }
        
        else
        {
            
                      
            NSUserDefaults *checkCard=[NSUserDefaults standardUserDefaults];
            NSString *savedCardName=[checkCard objectForKey:@"SAVED_CC_NAME"];
            NSString *savedCardNumber=[checkCard objectForKey:@"SAVED_CC_NUMBER"];
            NSString *savedCardDate=[checkCard objectForKey:@"SAVED_CC_DATE"];

            
            NSArray *lines = [savedCardDate componentsSeparatedByString: @"/"];
            
            
            
            NSLog(@"CARD_NAME:%@",savedCardName);
            NSLog(@"CARD_NAME:%@",savedCardNumber);
            NSLog(@"CARD_NAME:%@",savedCardDate);
            
            
            savedCardNumber = [savedCardNumber stringByReplacingOccurrencesOfString:@"C"
                                                 withString:@""];
            
            
         [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        stripeCard = [[STPCard alloc] init];
        stripeCard.name = savedCardName;
        stripeCard.number = savedCardNumber;
        stripeCard.cvc = _cvvField.text;
            
        stripeCard.expMonth = [lines[0] intValue];
        stripeCard.expYear = [lines[1] intValue];
            
            
            
            NSLog(@"CARD_NAME:%@",stripeCard.name);
             NSLog(@"CARD_NAME:%@",stripeCard.number);
             NSLog(@"CARD_NAME:%@",stripeCard.cvc);
             NSLog(@"CARD_NAME:%d",stripeCard.expMonth);
          NSLog(@"CARD_NAME:%d",stripeCard.expYear);
            
            [Stripe createTokenWithCard:stripeCard
                         publishableKey:STRIPE_KEY
                             completion:^(STPToken* token, NSError* error) {
                                 if(error){
                                     [self handleError:error];
                                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                                 }
                                 else
                                     [self handleToken:token];
                             }];
            
            
        }
        
       
    }

    
    
   }


- (void)handleError:(NSError *)error
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                      message:[error localizedDescription]
                                                     delegate:nil
                                            cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                            otherButtonTitles:nil];
    [message show];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)handleToken:(STPToken *)token
{
    NSLog(@"Received token %@", token.tokenId);
    
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/tap/stripeValidation",CONFIG_BASE_URL]];
    
    
    
    __block  ASIFormDataRequest *requestmethod = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *user=[check objectForKey:@"USERID"];
    
    
    [check setObject:token.tokenId forKey:@"LAST_TOKEN"];
    
    
    ////(@"USER:%@",user);
    
    
    
    [requestmethod setPostValue:user forKey:@"userID"];
    [requestmethod setPostValue:token.tokenId forKey:@"stripe_token"];
    [requestmethod setPostValue:_tapAmount.text forKey:@"stripe_amount"];
    [requestmethod setPostValue:@"usd" forKey:@"stripe_currency"];
   
    
    [requestmethod setTimeOutSeconds:30];
    
    
    [requestmethod setCompletionBlock:^{
        NSString *responseString23 = [requestmethod responseString];
        
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"TEMPARY:%@",responseString23);
        
        
        NSString *message=[results11 valueForKeyPath:@"Response.stripe"];
        NSString *chargeID=[results11 valueForKeyPath:@"Response.charge_id"];
        
        
        
        if ([message isEqualToString:@"Success"])
        {
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
                       [check setObject:chargeID forKey:@"LAST_CHARGEID"];
            
           [self addPaymentInfo];
            
           
        }
        else
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Payment failed!"
                                                                message:[NSString stringWithFormat:@"Check your Card Informations"] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];
            
            
                     
        }
        
        
        
        
        // [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    [requestmethod setFailedBlock:^{
        NSError *error = [requestmethod error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [requestmethod startAsynchronous];
    
    
    
    //[self addPaymentInfo];
    
    /*
     [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (IS_IPHONE_5)
    {
        TapRoomViewController *signupView=[[TapRoomViewController alloc]initWithNibName:@"TapRoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        TapRoomViewController *signupView=[[TapRoomViewController alloc]initWithNibName:@"TapRoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
     */

    
}
-(void)addPaymentInfo
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/tap/addPaymentInfo",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
    NSString *touser=[check objectForKey:@"CONTCAT_ID"];
    NSString *merchentid=[check objectForKey:@"BUSINESS_ID"];
    
    
        
  
    
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:_tapAmount.text forKey:@"payment_amount"];
    [request_post1234 setPostValue:@"success" forKey:@"payment_status"];
        
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN FINAL SSS:%@",results11);
        
        NSString *recID=[results11 valueForKeyPath:@"Response.recordID"];
        
    
        NSLog(@"HELLO:%@",recID);
     
        
        [self tapFuntion];
        
        
        
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
}
-(void)tapFuntion
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/tap/tapAFriend",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
    NSString *touser=[check objectForKey:@"CONTCAT_ID"];
    NSString *merchentid=[check objectForKey:@"BUSINESS_ID"];
    
      NSString *fk_receiver_friend_tbl_id=[check objectForKey:@"CONTCAT_FBID"];
    
    
    NSLog(@"TO USER:%@",touser);
    
    
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://www.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    
    [dateFormat1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSDate *now = [NSDate date];
    int daysToAdd = 30;
    NSDate *newDate1 = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
    
    NSDate *todaydate = newDate1;
    
    [dateFormat1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]] ;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormat stringFromDate:todaydate];
    
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:touser forKey:@"to_userID"];
    [request_post1234 setPostValue:@"" forKey:@"fk_receiver_friend_tbl_id"];
    [request_post1234 setPostValue:merchentid forKey:@"merchantID"];
    [request_post1234 setPostValue:_tapAmount.text forKey:@"tap_amount"];
    [request_post1234 setPostValue:dateString forKey:@"expire_date"];
    [request_post1234 setPostValue:@"" forKey:@"qr_code_image"];
    [request_post1234 setPostValue:@"valid" forKey:@"tap_status"];
    [request_post1234 setPostValue:@"YES" forKey:@"is_push_notif_sent"];
    [request_post1234 setPostValue:@"YES" forKey:@"is_email_sent"];
    [request_post1234 setPostValue:@"YES" forKey:@"is_text_sent"];
  
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN FINAL eddf:%@",responseString23);
        
        
        NSString *tapID=[results11 valueForKeyPath:@"Response.tapID"];
        
        
        NSLog(@"TAP ID:%@",tapID);
        
        [check setObject:tapID forKey:@"LAST_TAPPED_ID"];
        
        
        
                


        
        
        [self tapTansaction];
        
        
       
             
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
}


-(void)tapTansaction
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/tap/tapTransactionDetail",CONFIG_BASE_URL]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
   
    NSString *user=[check objectForKey:@"USERID"];
    NSString *fbuserid=[check objectForKey:@"FB_USERID"];
    
    NSString *touser=[check objectForKey:@"CONTCAT_ID"];
    NSString *merchentid=[check objectForKey:@"BUSINESS_ID"];
    
    NSString *fk_receiver_friend_tbl_id=[check objectForKey:@"CONTCAT_FBID"];
       NSString *tapID=[check objectForKey:@"LAST_TAPPED_ID"];
      NSString *chargeID=[check objectForKey:@"LAST_CHARGEID"];
     NSString *tokenID=[check objectForKey:@"LAST_TOKEN"];
    
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    
    [dateFormat1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSDate *now = [NSDate date];
    int daysToAdd = 30;
   
    
    [dateFormat1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]] ;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormat stringFromDate:now];
    
    
    
    [request_post1234 setPostValue:user forKey:@"userID"];
    [request_post1234 setPostValue:tapID forKey:@"tapID"];
    [request_post1234 setPostValue:touser forKey:@"to_userID"];
    [request_post1234 setPostValue:@"0" forKey:@"fk_to_user_friend_tbl_id"];
    [request_post1234 setPostValue:_tapAmount.text forKey:@"amount"];
    [request_post1234 setPostValue:dateString forKey:@"transaction_date"];
    
    
    [request_post1234 setPostValue:@"completed" forKey:@"transaction_status"];
    
    [request_post1234 setPostValue:tokenID forKey:@"transaction_token"];
    [request_post1234 setPostValue:chargeID forKey:@"transaction_id"];
    [request_post1234 setPostValue:@"CreditCard" forKey:@"transaction_type"];
    
    
    [request_post1234 setPostValue:@"tap" forKey:@"action"];
    [request_post1234 setPostValue:@"usd" forKey:@"currency"];
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN FINAL fdefd:%@",results11);
        
        
      
        
        
              
        
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        
        NSString *alertString=[NSString stringWithFormat:@"You have successfully tapped your friend %@ for $%@ at %@",_toNameLabel.text,_tapAmount.text,_catNameLabel.text];
        
        UIAlertView *aler=[[UIAlertView alloc]initWithTitle:@"Congratulations!" message:alertString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [aler show];
        
        if (IS_IPHONE_5)
        {
            TapRoomViewController *signupView=[[TapRoomViewController alloc]initWithNibName:@"TapRoomViewController" bundle:nil];
            [self presentViewController:signupView animated:NO completion:nil];
        }
        else
        {
            TapRoomViewController *signupView=[[TapRoomViewController alloc]initWithNibName:@"TapRoomViewController4" bundle:nil];
            [self presentViewController:signupView animated:NO completion:nil];
        }

        
        /*
         if (IS_IPHONE_5)
         {
         TapRoomViewController *signupView=[[TapRoomViewController alloc]initWithNibName:@"TapRoomViewController" bundle:nil];
         [self presentViewController:signupView animated:NO completion:nil];
         }
         else
         {
         TapRoomViewController *signupView=[[TapRoomViewController alloc]initWithNibName:@"TapRoomViewController4" bundle:nil];
         [self presentViewController:signupView animated:NO completion:nil];
         }
         
         */
        
        
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
}


-(IBAction)hideiPhone
{
    self.stripeView.hidden=YES;
    _cvvView.hidden=YES;
    [_tapAmount becomeFirstResponder];
    
}



- (IBAction)setPasscode:(id)sender {
    PAPasscodeViewController *passcodeViewController = [[PAPasscodeViewController alloc] initForAction:PasscodeActionSet];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        passcodeViewController.backgroundView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStyleGrouped];
    }
    passcodeViewController.delegate = self;
    passcodeViewController.simple = !_simpleSwitch.on;
    [self presentViewController:passcodeViewController animated:YES completion:nil];
}

- (IBAction)enterPasscode:(id)sender {
    PAPasscodeViewController *passcodeViewController = [[PAPasscodeViewController alloc] initForAction:PasscodeActionEnter];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        passcodeViewController.backgroundView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStyleGrouped];
    }
    passcodeViewController.delegate = self;
    passcodeViewController.passcode = _passcodeLabel.text;
    passcodeViewController.alternativePasscode = @"9999";
    passcodeViewController.simple =  !_simpleSwitch.on;
    [self presentViewController:passcodeViewController animated:YES completion:nil];
}

- (IBAction)changePasscode:(id)sender {
    PAPasscodeViewController *passcodeViewController = [[PAPasscodeViewController alloc] initForAction:PasscodeActionChange];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        passcodeViewController.backgroundView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStyleGrouped];
    }
    passcodeViewController.delegate = self;
    passcodeViewController.passcode = _passcodeLabel.text;
    passcodeViewController.simple =  !_simpleSwitch.on;
    [self presentViewController:passcodeViewController animated:YES completion:nil];
}

#pragma mark - PAPasscodeViewControllerDelegate

- (void)PAPasscodeViewControllerDidCancel:(PAPasscodeViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
   
    
}

- (void)PAPasscodeViewControllerDidEnterAlternativePasscode:(PAPasscodeViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^() {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Alternative Passcode entered correctly" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }];
}

- (void)PAPasscodeViewControllerDidEnterPasscode:(PAPasscodeViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^()
     {
         
         NSUserDefaults *checkCard=[NSUserDefaults standardUserDefaults];
         NSString *savedCardName=[checkCard objectForKey:@"SAVED_CC_NAME"];
         NSString *savedCardNumber=[checkCard objectForKey:@"SAVED_CC_NUMBER"];
         NSString *savedCardDate=[checkCard objectForKey:@"SAVED_CC_DATE"];
         
         
         if (cardTag==0)
         {
             [MBProgressHUD showHUDAddedTo:self.view animated:YES];
             
             
             [self.stripeView createToken:^(STPToken *token, NSError *error)
              {
                  
                  
                  
                  if (error) {
                      // Handle error
                      [self handleError:error];
                      
                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                  } else {
                      // Send off token to your server
                      [self handleToken:token];
                  }
              }];
             
         }
         
         else
         {
             
             
             NSUserDefaults *checkCard=[NSUserDefaults standardUserDefaults];
             NSString *savedCardName=[checkCard objectForKey:@"SAVED_CC_NAME"];
             NSString *savedCardNumber=[checkCard objectForKey:@"SAVED_CC_NUMBER"];
             NSString *savedCardDate=[checkCard objectForKey:@"SAVED_CC_DATE"];
             
             
             NSArray *lines = [savedCardDate componentsSeparatedByString: @"/"];
             
             
             
             NSLog(@"CARD_NAME:%@",savedCardName);
             NSLog(@"CARD_NAME:%@",savedCardNumber);
             NSLog(@"CARD_NAME:%@",savedCardDate);
             
             
             savedCardNumber = [savedCardNumber stringByReplacingOccurrencesOfString:@"C"
                                                                          withString:@""];
             
             
             [MBProgressHUD showHUDAddedTo:self.view animated:YES];
             stripeCard = [[STPCard alloc] init];
             stripeCard.name = savedCardName;
             stripeCard.number = savedCardNumber;
             stripeCard.cvc = _cvvField.text;
             
             stripeCard.expMonth = [lines[0] intValue];
             stripeCard.expYear = [lines[1] intValue];
             
             
             
             NSLog(@"CARD_NAME:%@",stripeCard.name);
             NSLog(@"CARD_NAME:%@",stripeCard.number);
             NSLog(@"CARD_NAME:%@",stripeCard.cvc);
             NSLog(@"CARD_NAME:%d",stripeCard.expMonth);
             NSLog(@"CARD_NAME:%d",stripeCard.expYear);
             
             [Stripe createTokenWithCard:stripeCard
                          publishableKey:STRIPE_KEY
                              completion:^(STPToken* token, NSError* error) {
                                  if(error){
                                      [self handleError:error];
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  }
                                  else
                                      [self handleToken:token];
                              }];
             
             
         }
         
     
         
         //[[[UIAlertView alloc] initWithTitle:nil message:@"Passcode entered correctly" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
     }];
}

- (void)PAPasscodeViewControllerDidSetPasscode:(PAPasscodeViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:^() {
        _passcodeLabel.text = controller.passcode;
        
        NSLog(@"PASS:%@",controller.passcode);
        
        
        NSUserDefaults *status=[NSUserDefaults standardUserDefaults];
        
        
        [status setObject:controller.passcode forKey:@"PIN_NUMBER"];
        [status setObject:@"ACTIVE" forKey:@"PIN_STATUS"];
        
    }];
}

- (void)PAPasscodeViewControllerDidChangePasscode:(PAPasscodeViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^() {
        _passcodeLabel.text = controller.passcode;
    }];
}







#pragma mark - LeveyPopListView delegates
- (void)leveyPopListView:(LeveyPopListView *)popListView didSelectedIndex:(NSInteger)anIndex
{
    NSLog(@"%@",[NSString stringWithFormat:@"You have selected %@", countryArray[anIndex]]);
}
- (void)leveyPopListViewDidCancel
{
    NSLog(@"CANCELLED");
}

-(IBAction)friends
{
    if (IS_IPHONE_5)
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }

}
-(IBAction)business
{
    if (IS_IPHONE_5)
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        CategoryViewController *signupView=[[CategoryViewController alloc]initWithNibName:@"CategoryViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }

}
-(IBAction)room
{
    if (IS_IPHONE_5)
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        RoomViewController *signupView=[[RoomViewController alloc]initWithNibName:@"RoomViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)setttings
{
    if (IS_IPHONE_5)
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        SettingsViewController *signupView=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}
-(IBAction)more
{
    if (IS_IPHONE_5)
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
        MoreViewController *signupView=[[MoreViewController alloc]initWithNibName:@"MoreViewController4" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up

{
	
    if (!IS_IPHONE_5) {
        
    
    
    int txtPosition = (textField.frame.origin.y - 200);
	
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
	
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
	
    [UIView setAnimationBeginsFromCurrentState: YES];
	
    [UIView setAnimationDuration: movementDuration];
	
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
	
    [UIView commitAnimations];
    }}
- (void)textFieldDidBeginEditing:(UITextField *)textField

{
     if (!IS_IPHONE_5) {
    
    [self animateTextField: textField up: YES];
     }
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField

{
	 if (!IS_IPHONE_5)
     {
    [self animateTextField: textField up: NO];
     }
   	
}


@end

//
//  ViewController.h
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FbGraph.h"
#import <FacebookSDK/FacebookSDK.h>
#import <AddressBook/AddressBook.h>
#import <Social/Social.h>
#import "ZBarSDK.h"
#import "ZBarReaderView.h"

@interface ViewController : UIViewController<FBLoginViewDelegate,FBFriendPickerDelegate,ZBarReaderDelegate>
{
      FbGraph *fbGraph;
}
-(IBAction)facebookConnect:(id)sender;
-(IBAction)signupConnect:(id)sender;
-(IBAction)closeButton:(id)sender;
-(IBAction)scanButton:(id)sender;
@property(nonatomic,retain)IBOutlet UIView *scan_view;
@property(nonatomic,retain)IBOutlet UIView *scan_main_view;
@property(nonatomic,retain)IBOutlet ZBarReaderViewController *reader;
@end

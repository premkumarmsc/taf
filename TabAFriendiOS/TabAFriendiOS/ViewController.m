//
//  ViewController.m
//  TabAFriendiOS
//
//  Created by ephronsystems on 9/23/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@end

@implementation ViewController
@synthesize reader;
FBLoginView *loginview;

/*
- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"TestNotification"])
    {
        NSLog (@"Successfully received the test notification!");
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Hello" message:notification.object delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
 */


-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    

        // ADD: get the decode results
        id<NSFastEnumeration> results =
        [info objectForKey: ZBarReaderControllerResults];
        ////(@"info");
        ZBarSymbol *symbol = nil;
        for(symbol in results)
            break;
        
        ////(@"datas %@",symbol.data);
        
        NSString *result=[NSString stringWithFormat:@"%@",symbol.data];
        
        NSArray *arrString = [result componentsSeparatedByString:@" "];
        
        NSString *finalString=@"";
        
        for(int i=0; i<arrString.count;i++){
            if([[arrString objectAtIndex:i] rangeOfString:@"http"].location != NSNotFound)
                ////(@"ARRA:%@", [arrString objectAtIndex:i]);
                
                finalString=[arrString objectAtIndex:i];
        }
        
        
        
        
        if ([finalString rangeOfString:@"http"].location == NSNotFound)
        {
            ////(@"string does not contain bla");
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"This code not contains valid URL" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else
        {
            ////(@"string contains bla!");
            
            _scan_main_view.hidden=YES;
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalString]];
            
            [reader.view removeFromSuperview];
        }
        
        
        // resultImage.image =
        [info objectForKey: UIImagePickerControllerOriginalImage];
        
        
        //
        

    
    
}




-(IBAction)scanButton:(id)sender
{
     _scan_main_view.hidden=NO;
    
    
   
    
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    
    
    reader.wantsFullScreenLayout = NO;
    
    reader.showsZBarControls = NO;
    
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
	
    [reader.view setFrame:CGRectMake(0, 0, 320,540)];
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
	
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
				   config: ZBAR_CFG_ENABLE
					   to: 0];
    
    // hideview.frame=CGRectMake(0, 420, 320, 40);
	
    // present and release the controller
    
    [self.scan_view addSubview:reader.view];
    
}
-(IBAction)closeButton:(id)sender
{
   _scan_main_view.hidden=YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
  
    
    /*
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
    */
    
    
   loginview = [[FBLoginView alloc] init];
    loginview.readPermissions = @[@"email", @"user_likes",@"user_hometown",
                                  @"user_location",
                                  @"user_about_me",
                                  @"friends_birthday",
                                  @"friends_photos",
                                  @"friends_work_history",
                                  @"friends_location",
                                  @"user_subscriptions",
                                  @"read_stream",
                                  @"user_birthday",
                                  @"friends_about_me",
                                  @"friends_likes",
                                  @"user_likes",
                                  @"user_photos"
                                  ];
    
    //loginview.publishPermissions=@[@"publish_stream",@"publish_actions"];
    
    
    if (IS_IPHONE_5)
    {
        loginview.frame = CGRectMake(48, 420, 271, 37);
    }
    else
        
    {
        loginview.frame = CGRectMake(48, 350, 271, 37);
    }
    
    
    
    
    for (id obj in loginview.subviews)
    {
        if ([obj isKindOfClass:[UIButton class]])
        {
            UIButton * loginButton =  obj;
            UIImage *loginImage = [UIImage imageNamed:@"fb-icon1.png"];
            [loginButton setBackgroundImage:loginImage forState:UIControlStateNormal];
            [loginButton setBackgroundImage:nil forState:UIControlStateSelected];
            [loginButton setBackgroundImage:nil forState:UIControlStateHighlighted];
            [loginButton sizeToFit];
        }
        if ([obj isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  obj;
            loginLabel.text = @"";
            loginLabel.textAlignment = UITextAlignmentCenter;
            loginLabel.frame = CGRectMake(0, 0, 271, 37);
        }
    }
    
    loginview.delegate = self;
    
    [self.view addSubview:loginview];
    
    [self.view addSubview:_scan_main_view];
    _scan_main_view.hidden=YES;
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - FBLoginViewDelegate

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    // first get the buttons set for login mode
    
    loginView.hidden=YES;
    
    NSLog(@"ENTER GHGHGH");
    
    
    
    NSString *val=[NSString stringWithFormat:@"%@",[[[FBSession activeSession] accessTokenData] accessToken]];
    
    
    NSLog(@"ACCESS:%@",val);
    
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    [checkval setObject:val forKey:@"ACCESS_TOKEN"];
    
   
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/authenticateFaceBookUser",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    
    
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:val forKey:@"facebook_access_token"];
    
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
        NSLog(@"RESULTS 1:%@",responseString);
        
        NSString *fbUserID=[results1 valueForKeyPath:@"Response.facebook_user_id"];
        NSString *userID=[results1 valueForKeyPath:@"Response.userID"];
        NSString *facebookToken=[results1 valueForKeyPath:@"Response.facebook_access_token"];
        
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        
        [add setObject:fbUserID forKey:@"FB_USERID"];
        [add setObject:userID forKey:@"USERID"];
        [add setObject:facebookToken forKey:@"FB_ACCESS_TOKEN"];
        
        
        NSLog(@"fbUserID:%@",fbUserID);
        NSLog(@"userID:%@",userID);
        NSLog(@"facebookToken:%@",facebookToken);
        
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/fetchFBContacts",CONFIG_BASE_URL]];
        
        NSLog(@"HELLO:%@",url);
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        
        
        
        
        [request_post setValidatesSecureCertificate:NO];
        [request_post setPostValue:userID forKey:@"userID"];
        
        
        
        [request_post setTimeOutSeconds:30];
        
       
        
        [request_post setCompletionBlock:^{
            NSString *responseString = [request_post responseString];
            
            NSMutableData *results1 = [responseString JSONValue];
            
            
              NSLog(@"fetchFBContacts:%@",responseString);
            
            if (IS_IPHONE_5)
            {
                FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
                [self presentViewController:signupView animated:NO completion:nil];
            }
            else
            {
                FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
                [self presentViewController:signupView animated:NO completion:nil];
            }

               loginView.hidden=NO;
            
            
            //return nil;
            
            
            
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];
        
        
              
        
    }];
    [request_post setFailedBlock:^{
        NSError *error = [request_post error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post startAsynchronous];
   
    
    
    
    
    // self.shareOnFacebook.enabled = YES;
}



- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView
{
    
    NSLog(@"HELLO HJHJJHJ" );
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *userID=[NSString stringWithFormat:@"%@",[checkval objectForKey:@"USERID"]];
   
    NSLog(@"USER:%@",userID);
    
       
    
    //BOOL canShareAnyhow = [FBNativeDialogs canPresentShareDialogWithSession:nil];
    // self.shareOnFacebook.enabled = canShareAnyhow;
    
    //self.loggedInUser = nil;
}

- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error
{
    NSString *alertMessage, *alertTitle;
    
    if (error.fberrorShouldNotifyUser) {
        // If the SDK has a message for the user, surface it. This conveniently
        // handles cases like password change or iOS6 app slider state.
        alertTitle = @"Something Went Wrong";
        alertMessage = error.fberrorUserMessage;
    } else if (error.fberrorCategory == FBErrorCategoryAuthenticationReopenSession) {
        // It is important to handle session closures as mentioned. You can inspect
        // the error for more context but this sample generically notifies the user.
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
    } else if (error.fberrorCategory == FBErrorCategoryUserCancelled) {
        // The user has cancelled a login. You can inspect the error
        // for more context. For this sample, we will simply ignore it.
        NSLog(@"user cancelled login");
        [FBSession.activeSession close];
        
    } else {
        // For simplicity, this sample treats other errors blindly, but you should
        // refer to https://developers.facebook.com/docs/technical-guides/iossdk/errors/ for more information.
        alertTitle  = @"Unknown Error";
        alertMessage = @"Error. Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage)
    {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}



#pragma mark -

// Convenience method to perform some action that requires the "publish_actions" permissions.
- (void) performPublishAction:(void (^)(void)) action
{
    
    NSLog(@"ENTER PERMISSIONS");
    
    
    // we defer request for permission to post to the moment of post, then we check for the permission
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        // if we don't already have the permission, then we request it now
        
        [FBSession.activeSession requestNewPublishPermissions:@[@"publish_actions",@"publish_stream"]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                            completionHandler:^(FBSession *session, NSError *error) {
                                                if (!error) {
                                                    action();
                                                }
                                                //For this example, ignore errors (such as if user cancels).
                                            }];
    } else {
        action();
    }
    
}


-(void)LoginToFacebook
{
    
    NSLog(@"LOGIn");
    
    if (!FBSession.activeSession.isOpen)
    {
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession.activeSession openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error) {
            switch (state) {
                case FBSessionStateClosedLoginFailed:
                {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                        message:error.localizedDescription
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
                    break;
                default:
                    break;
            }
        }];
    }
    
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user
{
    NSLog(@"%@", [NSString stringWithFormat:@"Hello %@!", user.first_name] );
    
    
}


// Pick Friends button handler
- (IBAction)pickFriendsList:(UIButton *)sender
{
    
    
    [FBSession.activeSession closeAndClearTokenInformation];
    
    /*
     NSLog(@"ACCESS:%@",[[[FBSession activeSession] accessTokenData] accessToken]);
     
     [self LoginToFacebook];
     
     */
    
}


- (void)showAlert:(NSString *)message result:(id)result error:(NSError *)error
{
    
    NSString *alertMsg;
    NSString *alertTitle;
    if (error)
    {
        alertTitle = @"Error";
        if (error.fberrorShouldNotifyUser ||
            error.fberrorCategory == FBErrorCategoryPermissions ||
            error.fberrorCategory == FBErrorCategoryAuthenticationReopenSession)
        {
            alertMsg = error.fberrorUserMessage;
        }
        
        else
        {
            alertMsg = @"Operation failed due to a connection problem, retry later.";
        }
    }
    else
    {
        NSDictionary *resultDict = (NSDictionary *)result;
        alertMsg = [NSString stringWithFormat:@"Successfully posted '%@'.\nPost ID: %@",
                    message, [resultDict valueForKey:@"id"]];
        alertTitle = @"Success";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle
                                                        message:alertMsg
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}



-(IBAction)signupConnect:(id)sender
{
    if (IS_IPHONE_5)
    {
        SignupViewController *signupView=[[SignupViewController alloc]initWithNibName:@"SignupViewController" bundle:nil];
        [self presentViewController:signupView animated:NO completion:nil];
    }
    else
    {
    SignupViewController *signupView=[[SignupViewController alloc]initWithNibName:@"SignupViewController4" bundle:nil];
    [self presentViewController:signupView animated:NO completion:nil];
    }
}

-(IBAction)facebookConnect:(id)sender
{
    
    NSString *client_id = FB_KEY;
    
    
    self->fbGraph = [[FbGraph alloc] initWithFbClientID:client_id];
    
    [fbGraph authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback) andExtendedPermissions:@"email"];
}

-(void)fbGraphCallback
{
    
    
    
    FbGraphResponse *fb_graph_response = [fbGraph doGraphGet:@"me" withGetVars:nil];
    
    
    
    
    // AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    //if (appDelegate.session.isOpen) {
    SBJSON *parser = [[SBJSON alloc] init];
    
    //NSData *response = fb_graph_response.htmlResponse;
    
    
    
    NSString *json_string = fb_graph_response.htmlResponse;
    
    
    
    NSDictionary *statuses = [parser objectWithString:json_string error:nil];
    
    NSLog(@"STATUS:%@",json_string);
    
    NSUserDefaults *checkval=[NSUserDefaults standardUserDefaults];
    
    NSString *val=[checkval objectForKey:@"ACCESS_TOKEN"];
 
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/authenticateFaceBookUser",CONFIG_BASE_URL]];
    
    NSLog(@"HELLO:%@",url);
    
    __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
    
    
    
    
    
    [request_post setValidatesSecureCertificate:NO];
    [request_post setPostValue:val forKey:@"facebook_access_token"];
   
    
    
    [request_post setTimeOutSeconds:30];
    
    
    [request_post setCompletionBlock:^{
        NSString *responseString = [request_post responseString];
        
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        
       NSLog(@"RESULTS HELL:%@",responseString);
        
        NSString *fbUserID=[results1 valueForKeyPath:@"Response.facebook_user_id"];
        NSString *userID=[results1 valueForKeyPath:@"Response.userID"];
        NSString *facebookToken=[results1 valueForKeyPath:@"Response.facebook_access_token"];
               
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        
        [add setObject:fbUserID forKey:@"FB_USERID"];
        [add setObject:userID forKey:@"USERID"];
        [add setObject:facebookToken forKey:@"FB_ACCESS_TOKEN"];
        
   
        NSLog(@"fbUserID:%@",fbUserID);
        NSLog(@"userID:%@",userID);
        NSLog(@"facebookToken:%@",facebookToken);
        
            
        if (IS_IPHONE_5)
        {
            FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
            [self presentViewController:signupView animated:NO completion:nil];
        }
        else
        {
            FriendsViewController *signupView=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController4" bundle:nil];
            [self presentViewController:signupView animated:NO completion:nil];
        }
            
            
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                                message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            
            
            
        }];
        
        [request_post startAsynchronous];

    
    
    
    
}


@end

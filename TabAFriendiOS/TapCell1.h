//
//  UpdatesTableViewCell.h
//  UpdatesListView
//
//  Created by Tope on 10/11/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TapCell1 : UITableViewCell
@property(nonatomic,retain )IBOutlet UIImageView *img;
@property(nonatomic,retain )IBOutlet UILabel *title;
@property(nonatomic,retain )IBOutlet UILabel *category;
@property(nonatomic,retain )IBOutlet UILabel *startfrom;

@property(nonatomic,retain )IBOutlet UILabel *distance;

@property (nonatomic,retain)IBOutlet UILabel *progress;
@property(nonatomic,retain )IBOutlet UIButton *deletebtn;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *av;



@property(nonatomic,retain )IBOutlet UIImageView *senderImg;
@property(nonatomic,retain )IBOutlet UIImageView *receiverImg;

@property(nonatomic,retain )IBOutlet UILabel *from;
@property(nonatomic,retain )IBOutlet UILabel *to;
@property(nonatomic,retain )IBOutlet UILabel *amount;
@property(nonatomic,retain )IBOutlet UILabel *status;
@property(nonatomic,retain )IBOutlet UILabel *expireDate;

@property(nonatomic,retain )IBOutlet UIImageView *statusImg;

@end
